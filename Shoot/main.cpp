/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include <d3d9.h>
#include <d3dx9.h>
#include <time.h>

#define PROGRAM_NAME "Distress FC"

#include "managers/logManager.h"
#include "managers/optionManager.h"
#include "managers/deviceManager.h"
#include "managers/cameraManager.h"
#include "managers/keyboardManager.h"
#include "managers/mouseManager.h"
#include "managers/joypadManager.h"
#include "managers/controlManager.h"
#include "managers/fontManager.h"
#include "managers/projectileManager.h"
#include "managers/bonusManager.h"
#include "managers/interfaceManager.h"
#include "managers/particleManager.h"
#include "managers/messageManager.h"
#include "managers/meshManager.h"
#include "managers/soundManager.h"
#include "managers/gameManager.h"
#include "managers/lightManager.h"
#include "managers/environmentManager.h"
#include "misc/timer.h"

#pragma comment(lib,"dinput8.lib")
#pragma comment(lib,"dxguid.lib")
#pragma comment(lib,"d3d9.lib")
#pragma comment(lib,"d3dx9.lib")
#pragma comment(lib,"comctl32.lib")
#pragma comment(lib,"dxerr.lib")

LogManager * logM;
OptionManager * optM;
DeviceManager * deviceM;
CameraManager * cameraM;
KeyboardManager * keyM;
MouseManager * mouseM;
JoypadManager * joypadM;
ControlManager * controlM;
FontManager * fontM;
ProjectileManager * projectileM;
ParticleManager * particleM;
BonusManager * bonusM;
MessageManager * messageM;
InterfaceManager * interfaceM;
MeshManager * meshM;
GameManager * gameM;
LightManager * lightM;
SoundManager * soundM;
EnvironmentManager * environmentM;

LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam,LPARAM lParam);
bool checkDevice();
void refresh(long time);
void draw();

/*bool checkDevice()
{
	bool ret = true;
	HRESULT deviceState = deviceM->getDevice()->TestCooperativeLevel();
	logM->getStream() << deviceState << "\n";
	logM->write();

    switch (deviceState)
    {
        case D3DERR_DEVICELOST :
            Sleep(100);
            ret = false;
    
        case D3DERR_DEVICENOTRESET :
            mouseM->releaseResources();
            deviceM->reset();
            mouseM->restoreResources();
            ret = false;
    }
	return ret;
}*/

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine,int nCmdShow)
{
    HWND		hWnd;
    WNDCLASSEX	wc;

	srand((unsigned int)time(NULL));

	logM = LogManager::getInstance();
	optM = OptionManager::getInstance();
	deviceM = DeviceManager::getInstance();
	cameraM = CameraManager::getInstance();
	keyM = KeyboardManager::getInstance();
	mouseM = MouseManager::getInstance();
	joypadM = JoypadManager::getInstance();
	controlM = ControlManager::getInstance();
	fontM = FontManager::getInstance();
	projectileM = ProjectileManager::getInstance();
	meshM = MeshManager::getInstance();
	gameM = GameManager::getInstance();
	lightM = LightManager::getInstance();
	bonusM = BonusManager::getInstance();
	interfaceM = InterfaceManager::getInstance();
	particleM = ParticleManager::getInstance();
	messageM = MessageManager::getInstance();
	soundM = SoundManager::getInstance();
	environmentM = EnvironmentManager::getInstance();

    ZeroMemory(&wc, sizeof(WNDCLASSEX));

    wc.cbSize			= sizeof(WNDCLASSEX);
    wc.style			= CS_HREDRAW | CS_VREDRAW;
    wc.lpfnWndProc		= (WNDPROC) WindowProc;
    wc.hInstance		= hInstance;
    wc.hCursor			= LoadCursor(NULL, IDC_ARROW);
    wc.hbrBackground	= (HBRUSH) COLOR_WINDOW;
    wc.lpszClassName	= PROGRAM_NAME;

    RegisterClassEx(&wc);

	if(optM->isFullScreen())
		hWnd = CreateWindowEx(NULL,PROGRAM_NAME,PROGRAM_NAME,WS_EX_TOPMOST | WS_POPUP, 0,  0, optM->getWidth(), optM->getHeight(), NULL, NULL, hInstance,NULL);	
	else
		hWnd = CreateWindowEx(NULL,PROGRAM_NAME,PROGRAM_NAME,WS_OVERLAPPED | WS_SYSMENU | WS_MINIMIZEBOX | WS_VISIBLE,100,100,optM->getWidth(),optM->getHeight(), NULL,NULL, hInstance, NULL);	

    ShowWindow(hWnd, nCmdShow);

	deviceM->initialize(hInstance,hWnd);
	cameraM->initialize();
	soundM->initialize();
	keyM->initialize();
	mouseM->initialize();
	joypadM->initialize();
	controlM->initialize();
	fontM->initialize();
	projectileM->initialize();
	meshM->initialize();
	gameM->initialize();
	lightM->initialize();
	interfaceM->initialize();
	particleM->initialize();
	messageM->initialize();
	environmentM->initialize();

	cameraM->generateMatrixes(D3DXVECTOR3(0.0f, 90.0f, -30.0f),D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	bonusM->initialize();

	HighResolutionTimer hrt;
	float time;
	double previousTime = hrt.Elapsed();

	bool exit = false;

    MSG msg;
    while(!exit)
    {
		time = (float)((hrt.Elapsed() - previousTime) * 1000);
		previousTime = hrt.Elapsed();

        if(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            if (msg.message == WM_QUIT)
                break;

            TranslateMessage(&msg);
            DispatchMessage(&msg);
        } 
		gameM->refresh(time);
		exit = gameM->getGameState() == GAME_STATE_EXIT;
		draw();

		Sleep(10);
    }

	EnemyManager::getInstance()->kill();
	ShaderManager::getInstance()->kill();
	TextureManager::getInstance()->kill();
	environmentM->kill();
	messageM->kill();
	particleM->kill();
	projectileM->kill();
	interfaceM->kill();
	bonusM->kill();
	lightM->kill();
	meshM->kill();
	fontM->kill();
	keyM->kill();
	mouseM->kill();
	joypadM->kill();
	controlM->kill();
	soundM->kill();
	gameM->kill();
	cameraM->kill();
	optM->kill();
	deviceM->kill();
	LogManager::getInstance()->kill();
	
    return (int)msg.wParam;
}

LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch(message)
    {
        case WM_DESTROY:
			PostQuitMessage(0);
			return 0;
		break;

		case WM_SETCURSOR:
			SetCursor(NULL);
			return TRUE;
			break;
    }
    return DefWindowProc (hWnd, message, wParam, lParam);
}

/**
 * Draw the frame
 */
void draw()
{
	deviceM->getDevice()->Clear(0, NULL, D3DCLEAR_TARGET , D3DCOLOR_XRGB(0, 0, 0), 1.0f, 0);
	deviceM->getDevice()->Clear(0, NULL, D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(0, 0, 0), 1.0f, 0);
	deviceM->getDevice()->BeginScene();

	deviceM->getDevice()->SetTransform(D3DTS_VIEW, &cameraM->getViewMatrix());
	deviceM->getDevice()->SetTransform(D3DTS_PROJECTION, &cameraM->getProjMatrix());

	gameM->draw();

	deviceM->getDevice()->EndScene(); 
	deviceM->getDevice()->Present(NULL, NULL, NULL, NULL);
}