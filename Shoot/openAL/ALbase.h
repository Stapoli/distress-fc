#ifndef _ALBASE_
#define _ALBASE_

// standard OpenAL include
#include <al.h>
#include <alc.h>
#include <efx.h>
#include <efx-util.h>

// standard OpenAL library
#pragma comment(lib,"OpenAL32.lib")
#pragma comment(lib,"alut.lib")
#pragma comment(lib,"EFX-Util.lib")
#pragma comment(lib,"Winmm.lib")

extern ALenum	OpenALerror;
const int ALnull = 0xffffffff;
#define AL_SAFECALL(x) 	{ alGetError(); x; if ((OpenALerror=alGetError()) != AL_NO_ERROR) fprintf(stderr,"OpenAL error (%s: %s at %d) : %s\n", __FILE__, __FUNCTION__, __LINE__, alGetString(OpenALerror)); }
ALvoid DisplayALError(ALbyte *szText, ALint errorcode);
float ALBufferDuration(ALenum BufferFormat, ALsizei BufferSize, ALsizei BufferFrequency);

#endif