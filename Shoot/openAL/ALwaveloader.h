#ifndef _ALWAVELOADER_
#define _ALWAVELOADER_


#include <al.h>
#include "CWaves.h"

class WaveLoader {
private:
	CWaves *pWaveLoader;
public:
	WaveLoader();
	~WaveLoader();
	WAVEID Open(const char *fname, ALenum *pFormat, ALvoid **ppData, ALsizei *pSize, ALsizei *pFreq );
	void Close(WAVEID WaveID);
	unsigned int Info(WAVEID WaveID);
};

#endif
