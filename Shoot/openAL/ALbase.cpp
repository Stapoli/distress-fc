#include <stdio.h>
#include "ALbase.h"

// constant used in AL_SAFECALL
ALenum	OpenALerror;

// Displaying error
ALvoid DisplayALError(ALbyte *szText, ALint errorcode) { 
	printf("%s%s", szText, alGetString(errorcode)); 
}

// buffer duration in ms
float ALBufferDuration(ALenum BufferFormat, ALsizei BufferSize, ALsizei BufferFrequency) {
	ALsizei		BytePerSample;
	switch(BufferFormat) {
	case AL_FORMAT_MONO8: 
		BytePerSample=1; 
		break;
	case AL_FORMAT_MONO16:
	case AL_FORMAT_STEREO8:
		BytePerSample=2; 
		break;
	case AL_FORMAT_STEREO16:
		BytePerSample=4; 
		break;
	}	
	return float(BufferSize) / float( BufferFrequency * BytePerSample );
}

