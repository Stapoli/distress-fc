#include <al.h>
#include "CWaves.h"
#include "ALwaveloader.h"

WaveLoader::WaveLoader() { 
	pWaveLoader = new CWaves(); 
}

WaveLoader::~WaveLoader() { 
	delete pWaveLoader; 
}

WAVEID WaveLoader::Open(const char *fname, ALenum *pFormat, ALvoid **ppData, ALsizei *pSize, ALsizei *pFreq ) {
	// chargement du fichier
	WAVEID			WaveID;
	pWaveLoader->LoadWaveFile(fname,&WaveID);
	// récupération des données nécessaires pour OpenAL
	pWaveLoader->GetWaveData(WaveID, ppData);
	GetWaveInfo(pWaveLoader,WaveID,pFormat,pSize,pFreq);			
	// pour post-destruction
	return WaveID;
}

void WaveLoader::Close(WAVEID WaveID) {
	pWaveLoader->DeleteWaveFile(WaveID);
}

unsigned int WaveLoader::Info(WAVEID WaveID) {
	const char	speaker[18][4]={"FL","FR","FC","lf","BL","BR","FLC","FRC","BC",
		"SL","SR","TC","TFL", "TFC", "TFR", "TBL", "TBC", "TBR" };
	WAVEFORMATEX			wft;
	WAVEFORMATEXTENSIBLE	wftx;
	WAVEFILETYPE			wftype;
	unsigned long			size;
	pWaveLoader->GetWaveType(WaveID, &wftype);
	if (wftype == WF_EX) 
		 pWaveLoader->GetWaveFormatExHeader(WaveID, &wft);
	else {
		pWaveLoader->GetWaveFormatExtensibleHeader(WaveID, &wftx);
		wft = wftx.Format;
	}
	pWaveLoader->GetWaveSize(WaveID, &size);
	printf("EX: nChan=%d, Freq=%d, Bits/samp=%d, Byte/Samp=%d Align=%d\n",
		wft.nChannels, wft.nSamplesPerSec, wft.wBitsPerSample, 
		wft.nAvgBytesPerSec, wft.nBlockAlign);
	printf("SZ+EZ: DataSz=%d, Time=%4.2fs\n",size,size/(float)wft.nAvgBytesPerSec);
	if (wftype == WF_EXT) {
		printf("EXT: ValidBit/sam=%d ChannelMask=",wftx.Samples.wValidBitsPerSample);
		for(int i=0;i<18;i++) {
			if (wftx.dwChannelMask & 0x0001) printf("%s ",speaker[i]);
			wftx.dwChannelMask = wftx.dwChannelMask >> 1;
		}
		printf("\n");
	}
	return (size*1000)/wft.nAvgBytesPerSec;
}
