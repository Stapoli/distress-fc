#ifndef _TIMER
#define _TIMER

/**
* HighResolutionTimer class
* Handle time
*/
class HighResolutionTimer 
{
protected:
	double tstart, tstop, tcorr, tfreq, tlast;

public:
	double Get(double *pt=NULL) 
	{
		LARGE_INTEGER timer;
		QueryPerformanceCounter(&timer);
		double	t = (double)timer.QuadPart / tfreq;
		if (pt!=NULL) *pt = t;
		return t;
	};

	HighResolutionTimer() 
	{
		LARGE_INTEGER timer;
		tcorr = 0.0;
		QueryPerformanceFrequency(&timer); 
		tfreq = double(timer.QuadPart); 
		tstart = Get(); 
		tstop  = Get(); 
		tcorr  = tstop-tstart;
		tlast = Get();
	};

	double Elapsed() 
	{
		double	tcur = Get();
		return tcur - tlast - tcorr;
	};

	double Delta() 
	{
		double	tsave = tlast;
		tlast = Get();
		return tlast - tsave - tcorr;
	};
};

#endif
