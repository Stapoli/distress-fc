/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include "../menus/mainMenu.h"

/**
* Constructor
*/
MainMenu::MainMenu() : Menu(3,ID_MAINMENU,-1)
{
	this->m_actionList[0] = ID_PLAYGAME;
	this->m_actionList[1] = ID_OPTIONMENU;
	this->m_actionList[2] = ID_EXITGAME;

	int centerY = this->m_optionM->getHeight() / 2;

	this->m_textList[0] = new FontMenu("PLAY_MAINMENU","Courrier","JOUER",35,0,FW_NORMAL,false,FONTMANAGER_ALIGN_CENTER,0,centerY - 60,this->m_optionM->getWidth(),centerY - 20,0xFFFFFFFF,0xFFFF0000);
	this->m_textList[1] = new FontMenu("OPTION_MAINMENU","Courrier","OPTIONS",35,0,FW_NORMAL,false,FONTMANAGER_ALIGN_CENTER,0,centerY - 20,this->m_optionM->getWidth(),centerY + 20,0xFFFFFFFF,0xFFFF0000);
	this->m_textList[2] = new FontMenu("EXIT_MAINMENU","Courrier","QUITTER",35,0,FW_NORMAL,false,FONTMANAGER_ALIGN_CENTER,0,centerY + 20,this->m_optionM->getWidth(),centerY + 60,0xFFFFFFFF,0xFFFF0000);

	this->m_textList[0]->setActive(true);

	this->m_sprite = new Sprite("imgs/menu.png",D3DXVECTOR2(0,0),D3DXVECTOR2(0,0));
	this->m_sprite->scaleSprite(this->m_optionM->getWidth() / 1024.0f,this->m_optionM->getHeight() / 1024.0f);
}

/**
* Destructor
*/
MainMenu::~MainMenu()
{
	Menu::~Menu();
}
