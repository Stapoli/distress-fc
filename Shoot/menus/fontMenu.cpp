/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include "../menus/fontMenu.h"

/**
* Constructor
* @param fontDescName The font descriptor name
* @param fontName The font name
* @param text The text
* @param fontHeight The font heigh in pixel
* @param fontWidth The font width in pixel
* @param fontWeight The font weight
* @param italic Change the italic state of the font
* @param alignment Set the alignment
* @param startX The start x coordinate
* @param startY The start y coordinate
* @param endX The end x coordinate
* @param endY The end y coordinate
* @param defaultColor The font default color
* @param selectionColor The font color used when selected
*/
FontMenu::FontMenu(std::string fontDescName, std::string fontName, std::string text, int fontHeight, int fontWidth, int fontWeight, bool italic, int alignment, int startX, int startY, int endX, int endY, D3DCOLOR defaultColor, D3DCOLOR selectionColor)
{
	this->m_fontDescName = fontDescName;
	this->m_text = text;
	this->m_alignment = alignment;
	this->m_startX = startX;
	this->m_startY = startY;
	this->m_endX = endX;
	this->m_endY = endY;
	this->m_defaultColor = defaultColor;
	this->m_selectionColor = selectionColor;
	this->m_activeColor = defaultColor;

	this->m_fontM = FontManager::getInstance();
	this->m_fontM->addFont(fontDescName,fontName,fontHeight,fontWidth,fontWeight,italic);
}

/**
* Destructor
*/
FontMenu::~FontMenu(){}

/**
* Check if the font is selected
* @param x X coordinate of the cursor
* @param y Y coordinate of the cursor
* @return true if the font is selected
*/
bool FontMenu::isSelected(int x, int y)
{
	return x >= this->m_startX && x <= this->m_endX && y >= this->m_startY && y <= this->m_endY;
}

/**
* Change the text
* @param text The new text
*/
void FontMenu::setText(std::string text)
{
	this->m_text = text;
}

/**
* Change de active state
* @param value Active state value
*/
void FontMenu::setActive(bool value)
{
	if(value)
		this->m_activeColor = this->m_selectionColor;
	else
		this->m_activeColor = this->m_defaultColor;
}

/**
* Draw the font
*/
void FontMenu::draw()
{
	this->m_fontM->drawText(this->m_fontDescName,this->m_text,this->m_alignment,this->m_startX,this->m_startY,this->m_endX, this->m_endY,this->m_activeColor);
}
