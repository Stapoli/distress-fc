/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#ifndef __FONTMENU_H
#define __FONTMENU_H

#include "../managers/fontManager.h"

/**
* FontMenu class
* Handle the fonts in the menu
*/
class FontMenu
{
private:
	std::string m_fontDescName;
	std::string m_text;
	int m_alignment;
	int m_startX;
	int m_startY;
	int m_endX;
	int m_endY;
	D3DCOLOR m_defaultColor;
	D3DCOLOR m_selectionColor;
	D3DCOLOR m_activeColor;
	FontManager * m_fontM;

public:
	FontMenu(std::string fontDescName, std::string fontName, std::string text, int fontHeight, int fontWidth, int fontWeight, bool italic, int alignment, int startX, int startY, int endX, int endY, D3DCOLOR defaultColor, D3DCOLOR selectionColor);
	~FontMenu();
	bool isSelected(int x, int y);
	void setText(std::string text);
	void setActive(bool value);
	void draw();
};

#endif
