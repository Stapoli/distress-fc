/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include "../menus/menu.h"

/**
* Constructor
* @param actionSize The number of actions
* @param codeMenu The code of the menu
* @param previousMenu The previous menu code
*/
Menu::Menu(int actionSize, int codeMenu, int previousMenu)
{
	this->m_optionM = OptionManager::getInstance();
	this->m_controlM = ControlManager::getInstance();
	this->m_mouseM = MouseManager::getInstance();

	this->m_codeMenu = codeMenu;
	this->m_previousMenu = previousMenu;
	this->m_actionSize = actionSize;

	this->m_actionList = new int[this->m_actionSize];
	this->m_textList = new FontMenu*[this->m_actionSize];
	this->m_movedTimer = 0;
	this->m_validationCode = this->m_codeMenu;
	this->m_actionChoosen = 0;
}

/**
* Destructor
*/
Menu::~Menu(){}

/**
* Get the menu code
* @return The menu code
*/
int Menu::getCodeMenu()
{
	return this->m_codeMenu;
}

/**
* Get the selected action
* @return The selected action
*/
int Menu::getSelectedMenu()
{
	return this->m_validationCode;
}

/**
* Refresh the menu
* @param time Elapsed time
*/
void Menu::refresh(float time)
{
	this->m_movedTimer += time;
	bool mouseOnText = false;

	if(this->m_mouseM->hasMoved())
	{
		for(int i = 0 ; i < this->m_actionSize && !mouseOnText ; i++)
		{
			if(this->m_textList[i]->isSelected(this->m_mouseM->getX(), this->m_mouseM->getY()))
			{
				mouseOnText = true;
				this->m_actionChoosen = i;
			}
		}

		for(int i = 0 ; i < this->m_actionSize ; i++)
			this->m_textList[i]->setActive(false);
		this->m_textList[this->m_actionChoosen]->setActive(true);
	}

	if(!mouseOnText)
	{
		if(this->m_controlM->isKeyDown(ACTION_KEY_UP_MENU) && this->m_movedTimer >= TIMEDELAYMENU)
		{
			this->m_actionChoosen--;
			if(this->m_actionChoosen < 0)
				this->m_actionChoosen = this->m_actionSize - 1;

			for(int i = 0 ; i < this->m_actionSize ; i++)
				this->m_textList[i]->setActive(false);
			this->m_textList[this->m_actionChoosen]->setActive(true);

			this->m_movedTimer = 0;
		}
		else
		{
			if(this->m_controlM->isKeyDown(ACTION_KEY_DOWN_MENU) && this->m_movedTimer >= TIMEDELAYMENU)
			{
				this->m_actionChoosen++;
				if(this->m_actionChoosen >= this->m_actionSize)
					this->m_actionChoosen = 0;

				for(int i = 0 ; i < this->m_actionSize ; i++)
					this->m_textList[i]->setActive(false);
				this->m_textList[this->m_actionChoosen]->setActive(true);

				this->m_movedTimer = 0;
			}
		}
	}

	if(this->m_controlM->isKeyDown(ACTION_KEY_ENTER_MENU) || this->m_mouseM->isButtonDown(MOUSE_BUTTON_ENTER_MENU))
		this->m_validationCode = this->m_actionList[this->m_actionChoosen];

	if((this->m_controlM->isKeyDown(ACTION_KEY_ESCAPE_MENU) || this->m_mouseM->isButtonDown(MOUSE_BUTTON_RETURN_MENU)) && this->m_previousMenu > -1)
		this->m_validationCode = this->m_previousMenu;
}

/**
* Draw the menu
*/
void Menu::draw()
{
	this->m_sprite->draw();
	for(int i = 0 ; i < this->m_actionSize ; i++)
		this->m_textList[i]->draw();
}
