/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include "../../elements/3D/bonus.h"

/**
* Constructor
*/
Bonus::Bonus()
{
	this->m_cameraM = CameraManager::getInstance();
	this->m_mesh = NULL;
	this->m_alive = false;

	//Get screen radius corresponding to world radius
	D3DXVECTOR3 tmpRadius = this->m_cameraM->coordinateToScreen(D3DXVECTOR3(BONUS_COLLISION_RADIUS,0,0));
	D3DXVECTOR3 tmpCenter = this->m_cameraM->coordinateToScreen(D3DXVECTOR3(0,0,0));
	tmpCenter -= tmpRadius;
	this->m_collisionRadius = D3DXVec3Length(&tmpCenter);
}

/**
* Destructor
*/
Bonus::~Bonus()
{
	delete this->m_mesh;
}

/**
* Return the world position
* @return The world position
*/
D3DXVECTOR3 Bonus::getPosition()
{
	return this->m_position;
}

/**
* Return the screen position
* @return The screen position
*/
D3DXVECTOR2 Bonus::getScreenPosition()
{
	return this->m_screenPosition;
}

/**
* Test if the bonus is alive
* @return The bonus state
*/
bool Bonus::isAlive()
{
	return this->m_alive;
}

/**
* Get the bonus radius
* @return The bonus radius
*/
float Bonus::getRadius()
{
	return this->m_collisionRadius;
}

/**
* Get the bonus type
* @return The bonus type
*/
int Bonus::getType()
{
	return this->m_type;
}

/**
* Change the bonus state
* @param b bonus state
*/
void Bonus::setAlive(bool b)
{
	this->m_alive = b;
}

/**
* Set the bonus
* @param position The bonus position
* @param type The bonus type
*/
void Bonus::setBonus(D3DXVECTOR3 position, int type)
{
	this->m_position = position;
	this->m_position.y--;
    this->m_rotation = D3DXVECTOR3((float)(rand()%360),(float)(rand()%360),(float)(rand()%360));
	this->m_direction.x = (rand()%31 - 10) / 10.0f;
	this->m_direction.z = (rand()%31 - 10) / 10.0f;
	this->m_type = type;
	this->m_life = 0;
	std::string modelName;

	switch(this->m_type)
	{
	case BONUS_POWERUP:
		modelName = "elements/bonus/powerup.x";
		break;

	case BONUS_BOMB:
		modelName = "elements/bonus/bomb.x";
		break;

	case BONUS_LIFE:
		modelName = "elements/bonus/life.x";
		break;

	case BONUS_SCORE:
		modelName = "elements/bonus/score.x";
		break;
	}

	if(this->m_mesh != NULL)
		delete this->m_mesh;
	this->m_mesh = new Model(modelName);
	this->m_mesh->setPosition(this->m_position);
	this->m_alive = true;
}

/**
* Refresh the bonus
* @param time Elapsed time
*/
void Bonus::refresh(float time)
{
	this->m_life += time;
	if(this->m_life < BONUS_MAX_LIFE)
	{
		this->m_position.x -= BONUS_MOVEMENT_SPEED * time * this->m_direction.x / 1000.0f;
		this->m_position.z -= BONUS_MOVEMENT_SPEED * time * this->m_direction.z / 1000.0f;

		if(this->m_position.x >= WINDOW_BORDER_X - BONUS_BORDER_X_MODIFIER)
		{
			this->m_position.x = WINDOW_BORDER_X - BONUS_BORDER_X_MODIFIER;
			this->m_direction.x *= -1;
		}

		if(this->m_position.x <= -WINDOW_BORDER_X + BONUS_BORDER_X_MODIFIER)
		{
			this->m_position.x = -WINDOW_BORDER_X + BONUS_BORDER_X_MODIFIER;
			this->m_direction.x *= -1;
		}

		if(this->m_position.z >= WINDOW_BORDER_Z_FAR)
		{
			this->m_position.z = WINDOW_BORDER_Z_FAR;
			this->m_direction.z *= -1;
		}

		if(this->m_position.z <= WINDOW_BORDER_Z_NEAR)
		{
			this->m_position.z = WINDOW_BORDER_Z_NEAR;
			this->m_direction.z *= -1;
		}

		for(int i = 0 ; i < 3 ; i++)
		{
			this->m_rotation[i] += BONUS_ROTATION_SPEED * time / 1000.0f;
			if(this->m_rotation[i] >= 360)
				this->m_rotation[i] -= 360;
		}
		this->m_mesh->setPosition(this->m_position);
		this->m_mesh->setRotation(this->m_rotation);
		this->m_screenPosition = this->m_cameraM->coordinateToScreen(this->m_position);
	}
	else
		this->m_alive = false;
}

/**
* Draw the bonus
*/
void Bonus::draw()
{
	this->m_mesh->draw(MODEL_DRAW_ENLIGHTENED);
}