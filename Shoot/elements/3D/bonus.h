/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#ifndef __BONUS_H
#define __BONUS_H

#include <string>
#include "../../managers/cameraManager.h"
#include "../../elements/3D/model.h"

#define BONUS_MOVEMENT_SPEED 3.0f
#define BONUS_ROTATION_SPEED 100.0f
#define BONUS_COLLISION_RADIUS 1.2f

#define BONUS_SCORE_VALUE 50
#define BONUS_BOMB_VALUE 150
#define BONUS_CONTINUE_VALUE 300
#define BONUS_POWERUP_VALUE 200

#define BONUS_BORDER_X_MODIFIER 6.0f
#define BONUS_MAX_LIFE 10000

enum bonusType
{
	BONUS_NOTHING,
	BONUS_POWERUP,
	BONUS_BOMB,
	BONUS_LIFE,
	BONUS_SCORE
};

/**
* Bonus class
* Handle the bonus
*/
class Bonus
{
private:
	bool m_alive;
	int m_type;
	float m_life;
	float m_collisionRadius;
	D3DXVECTOR3 m_direction;
	D3DXVECTOR3 m_position;
	D3DXVECTOR3 m_rotation;
	D3DXVECTOR2 m_screenPosition;
	Model * m_mesh;
	CameraManager * m_cameraM;

public:
	Bonus();
	~Bonus();
	D3DXVECTOR3 getPosition();
	D3DXVECTOR2 getScreenPosition();
	bool isAlive();
	float getRadius();
	int getType();
	void setAlive(bool b);
	void setBonus(D3DXVECTOR3 position, int type);
	void refresh(float time);
	void draw();
};

#endif
