/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include "../../elements/3D/model.h"

/**
* Constructor
* @param filename The 3d model filename
*/
Model::Model(std::string filename)
{
	this->m_deviceM = DeviceManager::getInstance();
	this->m_cameraM = CameraManager::getInstance();
	this->m_lightM = LightManager::getInstance();
	this->m_meshM = MeshManager::getInstance(); 
	
	this->m_meshPath = filename;
	this->m_meshM->loadMesh(filename);

	setPosition(D3DXVECTOR3(0.0f,0.0f,0.0f));
	setScale(D3DXVECTOR3(1.0f,1.0f,1.0f));
	setRotation(D3DXVECTOR3(0.0f,0.0f,0.0f));

	this->m_lightningShader = ShaderManager::getInstance()->loadShader(SHADER_TYPE_NULL);
	this->m_hitShader = ShaderManager::getInstance()->loadShader(SHADER_TYPE_HIT);
}

/**
* Destructor
*/
Model::~Model(){}

/**
* Set the model scale
* @param scale The model scale
*/
void Model::setScale(D3DXVECTOR3 scale)
{
	D3DXMatrixScaling(&this->m_scaleMatrix, scale.x, scale.y, scale.z);
}

/**
* Set the model position
* @param position The model position
*/
void Model::setPosition(D3DXVECTOR3 position)
{
	D3DXMatrixTranslation(&this->m_positionMatrix,position.x, position.y, position.z);
}

/**
* Set the model rotation
* @param rotation The model rotation
*/
void Model::setRotation(D3DXVECTOR3 rotation)
{
	D3DXMATRIX rotationMatrixX, rotationMatrixY, rotationMatrixZ;

	D3DXMatrixRotationX(&rotationMatrixX,D3DXToRadian(rotation.x));
	D3DXMatrixRotationY(&rotationMatrixY,D3DXToRadian(rotation.y));
	D3DXMatrixRotationZ(&rotationMatrixZ,D3DXToRadian(rotation.z));

	this->m_rotationMatrix = rotationMatrixX * rotationMatrixY * rotationMatrixZ;
}

/**
* Draw the model
* @param modelDraw The draw type
*/
void Model::draw(int modelDraw)
{
	this->m_worldMatrix = this->m_rotationMatrix * this->m_scaleMatrix * this->m_positionMatrix;
	D3DXMATRIX worldViewProjection = this->m_worldMatrix * this->m_cameraM->getViewProjMatrix();
	D3DXVECTOR4 camEye = D3DXVECTOR4(this->m_cameraM->getEye(),1);

	switch(modelDraw)
	{
	case MODEL_DRAW_ENLIGHTENED:
		this->m_lightningShader->getConstantTableVS()->SetMatrix(this->m_deviceM->getDevice(), this->m_lightningShader->getVertexHandle(NULLSHADER_HANDLE_VERTEX_WORLD), &this->m_worldMatrix);
		this->m_lightningShader->getConstantTableVS()->SetMatrix(this->m_deviceM->getDevice(), this->m_lightningShader->getVertexHandle(NULLSHADER_HANDLE_VERTEX_WORLDVIEWPROJ), &worldViewProjection);
		this->m_lightningShader->getConstantTablePS()->SetVector(this->m_deviceM->getDevice(), this->m_lightningShader->getVertexHandle(NULLSHADER_HANDLE_VERTEX_CAMERAEYE), &camEye);

		this->m_lightningShader->getConstantTablePS()->SetFloatArray(this->m_deviceM->getDevice(), this->m_lightningShader->getPixelHandle(NULLSHADER_HANDLE_PIXEL_LIGHTRADIUS), this->m_lightM->getRadius(),LIGHT_RESERVATION_SIZE);
		this->m_lightningShader->getConstantTablePS()->SetVectorArray(this->m_deviceM->getDevice(),this->m_lightningShader->getPixelHandle(NULLSHADER_HANDLE_PIXEL_LIGHTPOSITION),this->m_lightM->getPositions(),LIGHT_RESERVATION_SIZE);
		this->m_lightningShader->getConstantTablePS()->SetVectorArray(this->m_deviceM->getDevice(),this->m_lightningShader->getPixelHandle(NULLSHADER_HANDLE_PIXEL_LIGHTCOLOR),this->m_lightM->getColors(),LIGHT_RESERVATION_SIZE);
		this->m_lightningShader->getConstantTablePS()->SetIntArray(this->m_deviceM->getDevice(),this->m_lightningShader->getPixelHandle(NULLSHADER_HANDLE_PIXEL_LIGHTENABLED),this->m_lightM->getActivated(),LIGHT_RESERVATION_SIZE);

		this->m_deviceM->getDevice()->SetVertexDeclaration(this->m_lightningShader->getVertexShaderDeclaration());
		this->m_deviceM->getDevice()->SetVertexShader(this->m_lightningShader->getVertexShader());
		this->m_deviceM->getDevice()->SetPixelShader(this->m_lightningShader->getPixelShader());
		break;

	case MODEL_DRAW_HIT:
		this->m_hitShader->getConstantTableVS()->SetMatrix(this->m_deviceM->getDevice(), this->m_hitShader->getVertexHandle(HITSHADER_HANDLE_VERTEX_WORLD), &this->m_worldMatrix);
		this->m_hitShader->getConstantTableVS()->SetMatrix(this->m_deviceM->getDevice(), this->m_hitShader->getVertexHandle(HITSHADER_HANDLE_VERTEX_WORLDVIEWPROJ), &worldViewProjection);

		this->m_deviceM->getDevice()->SetVertexDeclaration(this->m_hitShader->getVertexShaderDeclaration());
		this->m_deviceM->getDevice()->SetVertexShader(this->m_hitShader->getVertexShader());
		this->m_deviceM->getDevice()->SetPixelShader(this->m_hitShader->getPixelShader());
		break;
	}

	this->m_meshM->draw(this->m_meshPath);

	this->m_deviceM->getDevice()->SetVertexShader(NULL);
	this->m_deviceM->getDevice()->SetPixelShader(NULL);
}
