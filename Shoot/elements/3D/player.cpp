/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include "../../elements/3D/player.h"

/**
* Constructor
*/
Player::Player()
{
	this->m_optionM = OptionManager::getInstance();
	this->m_controlM = ControlManager::getInstance();
	this->m_projectileM = ProjectileManager::getInstance();
	this->m_cameraM = CameraManager::getInstance();
	this->m_lightM = LightManager::getInstance();
	this->m_interfaceM = InterfaceManager::getInstance();
	this->m_particleM = ParticleManager::getInstance();
	this->m_bonusM = BonusManager::getInstance();
	this->m_messageM = MessageManager::getInstance();
	this->m_deviceM = DeviceManager::getInstance();
	this->m_position = D3DXVECTOR3(0,40,WINDOW_BORDER_Z_NEAR - PLAYER_BORDER_Z_OFFSCREEN_MODIFIER);
	this->m_spaceShip = new Model("elements/spaceships/player.x");
	this->m_spaceShip->setPosition(this->m_position);
	this->m_spaceShip->setScale(D3DXVECTOR3(0.3f, 0.3f, 0.3f));
	this->m_fireDelay = SPACESHIP_FIRE_DELAY;
	this->m_respawnTimer = 0;
	this->m_state = PLAYER_STATE_START;
	this->m_continues = PLAYER_NUMBER_OF_CONTINUES;
	this->m_powerUpState = PLAYER_POWERUP_BIG;
	this->m_score = 0;

	this->m_fireSound = SoundManager::getInstance()->addSound("sounds/laser.wav");
	this->m_explosionSound = SoundManager::getInstance()->addSound("sounds/explosion.wav");

	//Get screen radius corresponding to world radius
	D3DXVECTOR2 tmpRadius = this->m_cameraM->coordinateToScreen(D3DXVECTOR3(SPACESHIP_FIRE_COLLISION_RADIUS, 0, 0));
	D3DXVECTOR2 tmpCenter = this->m_cameraM->coordinateToScreen(D3DXVECTOR3(0, 0, 0));
	tmpCenter -= tmpRadius;
	this->m_cockpitRadius = D3DXVec2Length(&tmpCenter);

	tmpRadius = this->m_cameraM->coordinateToScreen(D3DXVECTOR3(SPACESHIP_OBJECT_COLLISION_RADIUS, 0, 0));
	tmpCenter = this->m_cameraM->coordinateToScreen(D3DXVECTOR3(0, 0, 0));
	tmpCenter -= tmpRadius;
	this->m_objectRadius = D3DXVec2Length(&tmpCenter);

	this->m_lightId = LIGHT_RESERVATION_PLAYER;
	this->m_lightM->setRadius(this->m_lightId, 40);
	this->m_lightM->setColor(this->m_lightId, D3DXVECTOR3(1, 1, 1));
	this->m_lightM->setEnabled(this->m_lightId, true);

	this->m_continuesSprites = new Sprite**[PLAYER_NUMBER_OF_CONTINUES + 1];
	for(int i = 0 ; i < PLAYER_NUMBER_OF_CONTINUES + 1 ; i++)
	{
		this->m_continuesSprites[i] = new Sprite*[PLAYER_PARTICLES_PER_SPRITE_CONTINUE];
		for(int j = 0 ; j < PLAYER_PARTICLES_PER_SPRITE_CONTINUE ; j++)
		{
			this->m_continuesSprites[i][j] = new Sprite("imgs/particles/continues.png",D3DXVECTOR2(0,0),D3DXVECTOR2(16 * this->m_optionM->getResizeFactor(),16 * this->m_optionM->getResizeFactor()));
			this->m_continuesSprites[i][j]->setColor(D3DXCOLOR(1, 1, 1, (j + 1) / (float)PLAYER_PARTICLES_PER_SPRITE_CONTINUE));
		}
	}
	this->m_continuesSpriteRotation = 0;
	this->m_interfaceM->setScore(this->m_score);
}

/**
* Destructor
*/
Player::~Player()
{
	delete this->m_spaceShip;
	for(int i = 0 ; i < this->m_continues + 1; i++)
	{
		for(int j = 0 ; j < PLAYER_PARTICLES_PER_SPRITE_CONTINUE ; j++)
			delete this->m_continuesSprites[i][j];
		delete this->m_continuesSprites[i];
	}
	delete this->m_continuesSprites;
}

/**
* Test if the game is over
* @return The game state
*/
bool Player::isGameOver()
{
	return (this->m_state == PLAYER_STATE_GAMEOVER);
}

/**
* Test if the player is alive
* @return The player state
*/
bool Player::isAlive()
{
	return (this->m_state == PLAYER_STATE_ALIVE);
}

/**
* Test the collision between the player and an object
* @param position The object position
* @param additionalRadius The object radius
* @return The collision test
*/
bool Player::isCollision(D3DXVECTOR2 position, float additionalRadius)
{
	bool ret = false;
	D3DXVECTOR2 tmp = this->m_screenPosition - position;
	if((D3DXVec2Length(&tmp)) <= this->m_cockpitRadius + additionalRadius)
	{
		ret = true;

		this->m_particleM->addParticle(PARTICLE_EXPLOSION, this->m_position,0,9);
		for(int i = 0 ; i < (this->m_powerUpState / 2) + 1 ; i++)
			this->m_bonusM->addBonus(this->m_position, BONUS_POWERUP);

		this->m_powerUpState = PLAYER_POWERUP_BIG;
		this->m_state = PLAYER_STATE_DEAD;
		alSourcePlay(this->m_explosionSound);
	}
	return ret;
}

/**
* Test the collision between the player and a bonus
* @param position The object position
* @param additionalRadius The object radius
* @param bonusType The bonus type
* @return The collision test
*/
bool Player::isCollisionBonus(D3DXVECTOR2 position, float additionalRadius, int bonusType)
{
	bool ret = false;
	D3DXVECTOR2 tmp = this->m_screenPosition - position;
	if((D3DXVec2Length(&tmp)) <= this->m_objectRadius + additionalRadius)
	{
		switch(bonusType)
		{
		case BONUS_POWERUP:
			this->m_powerUpState++;
			this->m_score += BONUS_POWERUP_VALUE;
			this->m_interfaceM->setScore(this->m_score);
			break;

		case BONUS_SCORE:
			this->m_score += BONUS_SCORE_VALUE;
			this->m_interfaceM->setScore(this->m_score);
			break;

		case BONUS_LIFE:
			if(this->m_continues < PLAYER_NUMBER_OF_CONTINUES)
				this->m_continues++;
		}
		ret = true;
	}
	return ret;
}

/**
* Refresh the player
* @param time Elapsed time
*/
void Player::refresh(float time)
{
	this->m_continuesSpriteRotation -= PLAYER_PARTICLES_CONTINUE_SPEED * time / 1000.0f;
	if(this->m_continuesSpriteRotation < 0)
		this->m_continuesSpriteRotation += 360.0f;

	if(this->m_state == PLAYER_STATE_ALIVE || this->m_state == PLAYER_STATE_RESPAWNING)
	{
		this->m_fireDelay += time;
		
		if(this->m_state == PLAYER_STATE_RESPAWNING)
		{
			this->m_respawnTimer += time;
			if(this->m_respawnTimer >= PLAYER_RESPAWN_TIME + PLAYER_IMMUNITY_TIME)
			{
				this->m_respawnTimer = 0;
				this->m_state = PLAYER_STATE_ALIVE;
			}
		}

		if(this->m_controlM->isKeyDown(ACTION_KEY_UP_ARROW))
			this->m_position.z += SPACESHIP_SPEED * time / 1000.0f;

		if(this->m_controlM->isKeyDown(ACTION_KEY_DOWN_ARROW))
			this->m_position.z -= SPACESHIP_SPEED * time / 1000.0f;
			
		if(this->m_controlM->isKeyDown(ACTION_KEY_LEFT_ARROW))
			this->m_position.x -= SPACESHIP_SPEED * time / 1000.0f;
				
		if(this->m_controlM->isKeyDown(ACTION_KEY_RIGHT_ARROW))
			this->m_position.x += SPACESHIP_SPEED * time / 1000.0f;

		//Border check
		if(this->m_position.x < -WINDOW_BORDER_X + PLAYER_BORDER_X_MODIFIER)
			this->m_position.x = -WINDOW_BORDER_X + PLAYER_BORDER_X_MODIFIER;
		if(this->m_position.x > WINDOW_BORDER_X - PLAYER_BORDER_X_MODIFIER)
			this->m_position.x = WINDOW_BORDER_X - PLAYER_BORDER_X_MODIFIER;
		if(this->m_position.z < WINDOW_BORDER_Z_NEAR + PLAYER_BORDER_Z_MODIFIER)
			this->m_position.z = WINDOW_BORDER_Z_NEAR + PLAYER_BORDER_Z_MODIFIER;
		if(this->m_position.z > WINDOW_BORDER_Z_FAR - PLAYER_BORDER_Z_MODIFIER)
			this->m_position.z = WINDOW_BORDER_Z_FAR - PLAYER_BORDER_Z_MODIFIER;

		if(this->m_controlM->isKeyDown(ACTION_KEY_FIRE1) && this->m_fireDelay >= SPACESHIP_FIRE_DELAY)
		{
			if(this->m_powerUpState >= PLAYER_POWERUP_BIG)
				this->m_projectileM->addProjectile(PROJECTILE_TYPE_PLAYER_BIG,PROJECTILE_SOURCE_PLAYER,D3DXVECTOR3(this->m_position.x,this->m_position.y,this->m_position.z + 1),0);

			if(this->m_powerUpState >= PLAYER_POWERUP_DOUBLE)
			{
				this->m_projectileM->addProjectile(PROJECTILE_TYPE_PLAYER_SMALL,PROJECTILE_SOURCE_PLAYER,D3DXVECTOR3(this->m_position.x - 1,this->m_position.y,this->m_position.z),0);
				this->m_projectileM->addProjectile(PROJECTILE_TYPE_PLAYER_SMALL,PROJECTILE_SOURCE_PLAYER,D3DXVECTOR3(this->m_position.x + 1,this->m_position.y,this->m_position.z),0);
			}

			if(this->m_powerUpState >= PLAYER_POWERUP_CURVE)
			{
				this->m_projectileM->addProjectile(PROJECTILE_TYPE_PLAYER_CURVE_LEFT,PROJECTILE_SOURCE_PLAYER,D3DXVECTOR3(this->m_position.x - 2,this->m_position.y,this->m_position.z),0);
				this->m_projectileM->addProjectile(PROJECTILE_TYPE_PLAYER_CURVE_RIGHT,PROJECTILE_SOURCE_PLAYER,D3DXVECTOR3(this->m_position.x + 2,this->m_position.y,this->m_position.z),0);
			}

			if(this->m_powerUpState >= PLAYER_POWERUP_DIAGONAL)
			{
				this->m_projectileM->addProjectile(PROJECTILE_TYPE_PLAYER_BIG_SLOW,PROJECTILE_SOURCE_PLAYER,D3DXVECTOR3(this->m_position.x - 3,this->m_position.y,this->m_position.z),315);
				this->m_projectileM->addProjectile(PROJECTILE_TYPE_PLAYER_BIG_SLOW,PROJECTILE_SOURCE_PLAYER,D3DXVECTOR3(this->m_position.x + 3,this->m_position.y,this->m_position.z),45);
			}

			if(this->m_powerUpState >= PLAYER_POWERUP_LATERAL)
			{
				this->m_projectileM->addProjectile(PROJECTILE_TYPE_PLAYER_SMALL,PROJECTILE_SOURCE_PLAYER,D3DXVECTOR3(this->m_position.x - 1,this->m_position.y,this->m_position.z),270);
				this->m_projectileM->addProjectile(PROJECTILE_TYPE_PLAYER_SMALL,PROJECTILE_SOURCE_PLAYER,D3DXVECTOR3(this->m_position.x + 1,this->m_position.y,this->m_position.z),90);
				this->m_projectileM->addProjectile(PROJECTILE_TYPE_PLAYER_SMALL_SLOW,PROJECTILE_SOURCE_PLAYER,D3DXVECTOR3(this->m_position.x,this->m_position.y,this->m_position.z - 2),180);
			}

			if(this->m_powerUpState >= PLAYER_POWERUP_CURVE2)
			{
				this->m_projectileM->addProjectile(PROJECTILE_TYPE_PLAYER_CURVE_LEFT,PROJECTILE_SOURCE_PLAYER,D3DXVECTOR3(this->m_position.x - 3,this->m_position.y,this->m_position.z),0);
				this->m_projectileM->addProjectile(PROJECTILE_TYPE_PLAYER_CURVE_RIGHT,PROJECTILE_SOURCE_PLAYER,D3DXVECTOR3(this->m_position.x + 3,this->m_position.y,this->m_position.z),0);
				this->m_projectileM->addProjectile(PROJECTILE_TYPE_PLAYER_CURVE_LEFT,PROJECTILE_SOURCE_PLAYER,D3DXVECTOR3(this->m_position.x - 4,this->m_position.y,this->m_position.z),0);
				this->m_projectileM->addProjectile(PROJECTILE_TYPE_PLAYER_CURVE_RIGHT,PROJECTILE_SOURCE_PLAYER,D3DXVECTOR3(this->m_position.x + 4,this->m_position.y,this->m_position.z),0);
			}

			alSourcePlay(this->m_fireSound);
			this->m_fireDelay = 0;
		}

		this->m_spaceShip->setPosition(this->m_position);
		this->m_screenPosition = this->m_cameraM->coordinateToScreen(this->m_position);
		this->m_screenPositionContinues = this->m_cameraM->coordinateToScreen(D3DXVECTOR3(this->m_position.x, this->m_position.y, this->m_position.z + PLAYER_PARTICLES_CONTINUE_CENTER_Z_MODIFIER));
		this->m_lightM->setPosition(this->m_lightId, D3DXVECTOR3(this->m_position.x,this->m_position.y + 10.0f,this->m_position.z));

		for(int i = 0 ; i < PLAYER_NUMBER_OF_CONTINUES + 1 ; i++)
			for(int j = 0 ; j < PLAYER_PARTICLES_PER_SPRITE_CONTINUE ; j++)
				this->m_continuesSprites[i][j]->setPosition(D3DXVECTOR2(this->m_screenPositionContinues.x + this->m_objectRadius * sin(((360 / (PLAYER_NUMBER_OF_CONTINUES + 1)) * i + this->m_continuesSpriteRotation + j * PLAYER_PARTICLES_CONTINUE_INTERVAL) / 180.0f * PI), this->m_screenPositionContinues.y + this->m_objectRadius * cos(((360 / (PLAYER_NUMBER_OF_CONTINUES + 1)) * i + this->m_continuesSpriteRotation + j * PLAYER_PARTICLES_CONTINUE_INTERVAL) / 180.0f * PI)));

	}
	else
	{
		if(this->m_state == PLAYER_STATE_START)
		{
			this->m_position.z += SPACESHIP_SPEED / 2.0f * time / 1000.0f;
			if(this->m_position.z >= WINDOW_BORDER_Z_NEAR + PLAYER_INTRO_Z_MODIFIER)
			{
				this->m_position.z = WINDOW_BORDER_Z_NEAR + PLAYER_INTRO_Z_MODIFIER;
				this->m_state = PLAYER_STATE_ALIVE;
				this->m_messageM->addMessage(MESSAGE_TYPE_IN_GAME_BEGIN,4000);
			}
			this->m_spaceShip->setPosition(this->m_position);
			this->m_screenPosition = this->m_cameraM->coordinateToScreen(this->m_position);
			this->m_screenPositionContinues = this->m_cameraM->coordinateToScreen(D3DXVECTOR3(this->m_position.x, this->m_position.y, this->m_position.z + PLAYER_PARTICLES_CONTINUE_CENTER_Z_MODIFIER));
			this->m_lightM->setPosition(this->m_lightId, D3DXVECTOR3(this->m_position.x,this->m_position.y + 10.0f,this->m_position.z));

			for(int i = 0 ; i < PLAYER_NUMBER_OF_CONTINUES + 1 ; i++)
			for(int j = 0 ; j < PLAYER_PARTICLES_PER_SPRITE_CONTINUE ; j++)
				this->m_continuesSprites[i][j]->setPosition(D3DXVECTOR2(this->m_screenPositionContinues.x + this->m_objectRadius * sin(((360 / (PLAYER_NUMBER_OF_CONTINUES + 1)) * i + this->m_continuesSpriteRotation + j * PLAYER_PARTICLES_CONTINUE_INTERVAL) / 180.0f * PI), this->m_screenPositionContinues.y + this->m_objectRadius * cos(((360 / (PLAYER_NUMBER_OF_CONTINUES + 1)) * i + this->m_continuesSpriteRotation + j * PLAYER_PARTICLES_CONTINUE_INTERVAL) / 180.0f * PI)));
		}
		else
		{
			this->m_respawnTimer += time;
			if(this->m_state == PLAYER_STATE_DEAD && this->m_respawnTimer >= PLAYER_RESPAWN_TIME)
			{
				if(this->m_continues > 0)
				{
					this->m_continues--;
					this->m_state = PLAYER_STATE_RESPAWNING;
				}
				else
				{
					this->m_state = PLAYER_STATE_GAMEOVER;
					this->m_messageM->addMessage(MESSAGE_TYPE_IN_GAME_GAME_OVER,3000);
				}
			}
		}
	}
}

/**
* Draw the player
*/
void Player::draw()
{
	if(this->m_state == PLAYER_STATE_ALIVE || this->m_state == PLAYER_STATE_START || (this->m_state == PLAYER_STATE_RESPAWNING && (int)(this->m_respawnTimer / PLAYER_RESPAWN_INTERVAL) % 2 == 0))
	{
		this->m_spaceShip->draw(MODEL_DRAW_ENLIGHTENED);
		for(int i = 0 ; i < this->m_continues + 1; i++)
			for(int j = 0 ; j < PLAYER_PARTICLES_PER_SPRITE_CONTINUE ; j++)
				this->m_continuesSprites[i][j]->draw();
	}
}