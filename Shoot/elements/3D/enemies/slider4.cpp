/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include "../../../elements/3D/enemies/slider4.h"

/**
* Constructor
* @param position The world position
* @param orientation The orientation
* @param sleep The starting sleep
* @param waveId The wave id
* @param drop The object drop on death
*/
Slider4::Slider4(D3DXVECTOR3 position, int orientation, int sleep, int waveId, int drop) : Enemy(position, 3, 2, waveId, drop, 8, 2.0f, 1)
{
	this->m_projectileType = PROJECTILE_TYPE_ENEMY1;
	this->m_fireDelay = 70;
	this->m_speed = 30.0f;
	this->m_rotationSpeed = 120.0f;
	this->m_spaceShip = new Model("elements/spaceships/slider.x");
	this->m_spaceShip->setScale(D3DXVECTOR3(0.6f, 0.6f, 0.6f));
	this->m_spaceShip->setPosition(this->m_position);
	this->m_type = ENEMY_TYPE_FIXED;

	switch(orientation)
	{
	case ENEMY_ORIENTATION_LEFT:
		this->m_patterns[0][0] = Pattern(PATTERN_ACTION_SLEEP,(float)sleep);
		this->m_patterns[1][0] = Pattern(PATTERN_ACTION_TRANSLATE_MINUS_Z,50);
		this->m_patterns[1][1] = Pattern(PATTERN_ACTION_MINUS_ROTATE,180);
		this->m_patterns[2][0] = Pattern(PATTERN_ACTION_TRANSLATE_X,60);
		this->m_patterns[2][1] = Pattern(PATTERN_ACTION_FIRE,1,false);
		break;

	case ENEMY_ORIENTATION_RIGHT:
		this->m_patterns[0][0] = Pattern(PATTERN_ACTION_SLEEP,(float)sleep);
		this->m_patterns[1][0] = Pattern(PATTERN_ACTION_TRANSLATE_MINUS_Z,50);
		this->m_patterns[1][1] = Pattern(PATTERN_ACTION_MINUS_ROTATE,180);
		this->m_patterns[2][0] = Pattern(PATTERN_ACTION_TRANSLATE_MINUS_X,60);
		this->m_patterns[2][1] = Pattern(PATTERN_ACTION_FIRE,1,false);
		break;
	}
}

/**
* Destructor
*/
Slider4::~Slider4(){}