/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include "../../../elements/3D/enemies/enemy.h"

/**
* Constructor
* @param position The world position
* @param patternSize The size of the pattern
* @param actionsPerPattern The number of actions per pattern
* @param waveId The wave id
* @param drop The object drop on death
* @param score The score given on death
* @param collisionRadius The collision Radius
* @param fireEntityNumber The number of FireEntity
*/
Enemy::Enemy(D3DXVECTOR3 position, int patternSize, int actionsPerPattern, int waveId, int drop, int score, float collisionRadius, int fireEntityNumber)
{
	this->m_projectileM = ProjectileManager::getInstance();
	this->m_cameraM = CameraManager::getInstance();
	this->m_deviceM = DeviceManager::getInstance();
	this->m_bonusM = BonusManager::getInstance();
	this->m_particleM = ParticleManager::getInstance();
	this->m_explosionSound = SoundManager::getInstance()->addSound("sounds/explosion.wav");
	this->m_fireSound = SoundManager::getInstance()->addSound("sounds/laser.wav");

	this->m_state = ENEMY_STATE_ALIVE;
	this->m_energy = 100;
	this->m_maxEnergy = this->m_energy;
	this->m_patternIndex = 0;
	this->m_patternSize = patternSize;
	this->m_actionsPerPattern = actionsPerPattern;
	this->m_waveId = waveId;
	this->m_drop = drop;
	this->m_score = score;
	this->m_position = position;
	this->m_rotation = D3DXVECTOR3(0,180,0);
	this->m_fireTimer = 0;
	this->m_sleepTimer = 0;
	this->m_smokeTimer = 0;
	this->m_hitTimer = 0;
	this->m_hit = false;
	this->m_drawState = MODEL_DRAW_ENLIGHTENED;
	this->m_fireEntitiesNumber = fireEntityNumber;

	this->m_turret = NULL;

	this->m_patterns = new Pattern*[this->m_patternSize];
	for(int i = 0 ; i < this->m_patternSize ; i++)
		this->m_patterns[i] = new Pattern[this->m_actionsPerPattern];

	this->m_fireEntities = new FireEntity[this->m_fireEntitiesNumber];

	//Get screen radius corresponding to world radius
	D3DXVECTOR2 tmpRadius = this->m_cameraM->coordinateToScreen(D3DXVECTOR3(collisionRadius,0,0));
	D3DXVECTOR2 tmpCenter = this->m_cameraM->coordinateToScreen(D3DXVECTOR3(0,0,0));
	tmpCenter -= tmpRadius;
	this->m_collisionRadius = D3DXVec2Length(&tmpCenter);
}

/**
* Destructor
*/
Enemy::~Enemy()
{
	for(int i = 0 ; i < this->m_patternSize ; i++)
		delete this->m_patterns[i];
	this->m_patterns;
	delete this->m_fireEntities;
}

/**
* Get the enemy state
* @return The enemy state
*/
int Enemy::getState()
{
	return this->m_state;
}

/**
* Return the wave id
* @return Wave id
*/
int Enemy::getWaveId()
{
	return this->m_waveId;
}

/**
* Test a collision between the enemy and an object
* @param position The object position
* @param additionalRadius Radius of the object
* @return The collision state
*/
bool Enemy::isCollision(D3DXVECTOR2 position, float additionalRadius)
{
	D3DXVECTOR2 tmp = this->m_screenPosition - position;
	return ((D3DXVec2Length(&tmp)) <= this->m_collisionRadius + additionalRadius) && !this->m_hit;
}

/**
* Damage the enemy
* @param damage The damage done
*/
void Enemy::takeDamage(int damage)
{
	this->m_energy -= damage;
	this->m_hit = true;
	this->m_drawState = MODEL_DRAW_HIT;

	if(this->m_energy <= 0)
	{
		this->m_energy = 0;
		this->m_state = ENEMY_STATE_DEAD;
		if(this->m_drop > BONUS_NOTHING)
			this->m_bonusM->addBonus(this->m_position,this->m_drop);
		for(int i = 0 ; i < this->m_score ; i++)
			this->m_bonusM->addBonus(this->m_position,BONUS_SCORE);
		this->m_particleM->addParticle(PARTICLE_EXPLOSION,this->m_position,0,6);
		alSourcePlay(this->m_explosionSound);
	}
}

/**
* Refresh the enemy
* @param time Elapsed time
*/
void Enemy::refresh(float time)
{
	if(this->m_patternIndex < this->m_patternSize && this->m_position.z >= WINDOW_BORDER_Z_NEAR - ENEMY_BORDER_Z_OFFSCREEN_MODIFIER)
	{
		this->m_fireTimer += time;

		if((this->m_energy / this->m_maxEnergy) <= ENEMY_SMOKE_PERCENTAGE)
		{
			this->m_smokeTimer += time;
			if(this->m_smokeTimer >= ENEMY_SMOKE_TIME)
			{
				this->m_smokeTimer = 0;
				this->m_particleM->addParticle(PARTICLE_SMOKE,this->m_position, 0, 1);
			}
		}

		if(this->m_hit)
		{
			this->m_hitTimer += time;
			if(this->m_hitTimer >= ENEMY_HIT_DELAY / 2.0f)
				this->m_drawState = MODEL_DRAW_ENLIGHTENED;
			if(this->m_hitTimer >= ENEMY_HIT_DELAY)
			{
				this->m_hitTimer = 0;
				this->m_hit = false;
			}
		}

		bool currentPatternOver = true;
		float currentMovement;
		for(int i = 0 ; i < this->m_actionsPerPattern && currentPatternOver ; i++)
			if(!this->m_patterns[this->m_patternIndex][i].m_over && this->m_patterns[this->m_patternIndex][i].m_blocking)
				currentPatternOver = false;

		if(currentPatternOver)
			this->m_patternIndex++;
	
		if(this->m_patternIndex < this->m_patternSize)
		{
			for(int i = 0 ; i < this->m_actionsPerPattern ; i++)
			{
				if(!this->m_patterns[this->m_patternIndex][i].m_over)
				{
					switch(this->m_patterns[this->m_patternIndex][i].m_action)
					{
					case PATTERN_ACTION_TRANSLATE_X:
						currentMovement = this->m_speed * time / 1000.0f;

						if(this->m_patterns[this->m_patternIndex][i].m_unit < currentMovement)
							currentMovement = this->m_patterns[this->m_patternIndex][i].m_unit;
						this->m_position.x += currentMovement;
						
						break;

					case PATTERN_ACTION_TRANSLATE_Z:
						currentMovement = this->m_speed * time / 1000.0f;

						if(this->m_patterns[this->m_patternIndex][i].m_unit < currentMovement)
							currentMovement = this->m_patterns[this->m_patternIndex][i].m_unit;
						this->m_position.z += currentMovement;
						break;

					case PATTERN_ACTION_TRANSLATE_MINUS_X:
						currentMovement = this->m_speed * time / 1000.0f;

						if(this->m_patterns[this->m_patternIndex][i].m_unit < currentMovement)
							currentMovement = this->m_patterns[this->m_patternIndex][i].m_unit;
						this->m_position.x -= currentMovement;
						break;

					case PATTERN_ACTION_TRANSLATE_MINUS_Z:
						currentMovement = this->m_speed * time / 1000.0f;

						if(this->m_patterns[this->m_patternIndex][i].m_unit < currentMovement)
							currentMovement = this->m_patterns[this->m_patternIndex][i].m_unit;
						this->m_position.z -= currentMovement;
						break;

					case PATTERN_ACTION_TRANSLATE_X_MINUS_Z:
						currentMovement = this->m_speed * time / 1000.0f;

						if(this->m_patterns[this->m_patternIndex][i].m_unit < currentMovement)
							currentMovement = this->m_patterns[this->m_patternIndex][i].m_unit;
						this->m_position.x += currentMovement;
						this->m_position.z -= currentMovement;
						break;

					case PATTERN_ACTION_TRANSLATE_MINUS_X_Z:
						currentMovement = this->m_speed * time / 1000.0f;

						if(this->m_patterns[this->m_patternIndex][i].m_unit < currentMovement)
							currentMovement = this->m_patterns[this->m_patternIndex][i].m_unit;
						this->m_position.x -= currentMovement;
						this->m_position.z += currentMovement;
						break;

					case PATTERN_ACTION_TRANSLATE_X_Z:
						currentMovement = this->m_speed * time / 1000.0f;

						if(this->m_patterns[this->m_patternIndex][i].m_unit < currentMovement)
							currentMovement = this->m_patterns[this->m_patternIndex][i].m_unit;
						this->m_position.x += currentMovement;
						this->m_position.z += currentMovement;
						break;

					case PATTERN_ACTION_TRANSLATE_MINUS_X_MINUS_Z:
						currentMovement = this->m_speed * time / 1000.0f;

						if(this->m_patterns[this->m_patternIndex][i].m_unit < currentMovement)
							currentMovement = this->m_patterns[this->m_patternIndex][i].m_unit;
						this->m_position.x -= currentMovement;
						this->m_position.z -= currentMovement;
						break;

					case PATTERN_ACTION_MOVE_FORWARD_NORMAL:
						currentMovement = this->m_speed * time / 1000.0f;

						if(this->m_patterns[this->m_patternIndex][i].m_unit < currentMovement)
							currentMovement = this->m_patterns[this->m_patternIndex][i].m_unit;

						this->m_position.x += sin(this->m_rotation.y / 180.0f * PI) * currentMovement;
						this->m_position.z += cos(this->m_rotation.y / 180.0f * PI) * currentMovement;
						break;

					case PATTERN_ACTION_MOVE_BACKWARD_NORMAL:
						currentMovement = this->m_speed * time / 1000.0f;

						if(this->m_patterns[this->m_patternIndex][i].m_unit < currentMovement)
							currentMovement = this->m_patterns[this->m_patternIndex][i].m_unit;

						this->m_position.x -= sin(this->m_rotation.y / 180.0f * PI) * currentMovement;
						this->m_position.z -= cos(this->m_rotation.y / 180.0f * PI) * currentMovement;
						break;

					case PATTERN_ACTION_MOVE_FORWARD_SLOW:
						currentMovement = (this->m_speed * time / 1000.0f) / 2.0f;

						if(this->m_patterns[this->m_patternIndex][i].m_unit < currentMovement)
							currentMovement = this->m_patterns[this->m_patternIndex][i].m_unit;

						this->m_position.x += sin(this->m_rotation.y / 180.0f * PI) * currentMovement;
						this->m_position.z += cos(this->m_rotation.y / 180.0f * PI) * currentMovement;
						break;

					case PATTERN_ACTION_MOVE_BACKWARD_SLOW:
						currentMovement = (this->m_speed * time / 1000.0f) / 2.0f;

						if(this->m_patterns[this->m_patternIndex][i].m_unit < currentMovement)
							currentMovement = this->m_patterns[this->m_patternIndex][i].m_unit;

						this->m_position.x -= sin(this->m_rotation.y / 180.0f * PI) * currentMovement;
						this->m_position.z -= cos(this->m_rotation.y / 180.0f * PI) * currentMovement;
						break;

					case PATTERN_ACTION_MOVE_FORWARD_BOOST:
						currentMovement = this->m_speed * time / 1000.0f * 3.0f;

						if(this->m_patterns[this->m_patternIndex][i].m_unit < currentMovement)
							currentMovement = this->m_patterns[this->m_patternIndex][i].m_unit;

						this->m_position.x += sin(this->m_rotation.y / 180.0f * PI) * currentMovement;
						this->m_position.z += cos(this->m_rotation.y / 180.0f * PI) * currentMovement;
						break;

					case PATTERN_ACTION_MOVE_BACKWARD_BOOST:
						currentMovement = this->m_speed * time / 1000.0f * 3.0f;

						if(this->m_patterns[this->m_patternIndex][i].m_unit < currentMovement)
							currentMovement = this->m_patterns[this->m_patternIndex][i].m_unit;

						this->m_position.x -= sin(this->m_rotation.y / 180.0f * PI) * currentMovement;
						this->m_position.z -= cos(this->m_rotation.y / 180.0f * PI) * currentMovement;
						break;

					case PATTERN_ACTION_ROTATE:
						currentMovement = this->m_rotationSpeed * time / 1000.0f;

						if(this->m_patterns[this->m_patternIndex][i].m_unit < currentMovement)
							currentMovement = this->m_patterns[this->m_patternIndex][i].m_unit;
						this->m_rotation.y += currentMovement;
						if(this->m_type == ENEMY_TYPE_FIXED)
						{
							for(int j = 0 ; j < this->m_fireEntitiesNumber ; j++)
							{
								if(this->m_fireEntities[j].m_enabled)
								{
									this->m_fireEntities[j].m_rotation += currentMovement;
									this->m_fireEntities[j].refresh();
								}
							}
						}
						break;

					case PATTERN_ACTION_MINUS_ROTATE:
						currentMovement = this->m_rotationSpeed * time / 1000.0f;

						if(this->m_patterns[this->m_patternIndex][i].m_unit < currentMovement)
							currentMovement = this->m_patterns[this->m_patternIndex][i].m_unit;
						this->m_rotation.y -= currentMovement;
						if(this->m_type == ENEMY_TYPE_FIXED)
						{
							for(int j = 0 ; j < this->m_fireEntitiesNumber ; j++)
							{
								if(this->m_fireEntities[j].m_enabled)
								{
									this->m_fireEntities[j].m_rotation -= currentMovement;
									this->m_fireEntities[j].refresh();
								}
							}
						}
						break;

					case PATTERN_ACTION_ROTATE_AIM:
						currentMovement = this->m_rotationSpeed * time / 1000.0f;

						if(this->m_patterns[this->m_patternIndex][i].m_unit < currentMovement)
							currentMovement = this->m_patterns[this->m_patternIndex][i].m_unit;

						if(this->m_type == ENEMY_TYPE_FIXED)
						{
							for(int j = 0 ; j < this->m_fireEntitiesNumber ; j++)
							{
								if(this->m_fireEntities[j].m_enabled)
								{
									this->m_fireEntities[j].m_rotation += currentMovement;
									this->m_fireEntities[j].refresh();
								}
							}
						}

						if(this->m_type == ENEMY_TYPE_FIXED)
							this->m_rotation.y += currentMovement;
						break;

					case PATTERN_ACTION_MINUS_ROTATE_AIM:
						currentMovement = this->m_rotationSpeed * time / 1000.0f;

						if(this->m_patterns[this->m_patternIndex][i].m_unit < currentMovement)
							currentMovement = this->m_patterns[this->m_patternIndex][i].m_unit;

						if(this->m_type == ENEMY_TYPE_FIXED)
						{
							for(int j = 0 ; j < this->m_fireEntitiesNumber ; j++)
							{
								if(this->m_fireEntities[j].m_enabled)
								{
									this->m_fireEntities[j].m_rotation -= currentMovement;
									this->m_fireEntities[j].refresh();
								}
							}
						}

						if(this->m_type == ENEMY_TYPE_FIXED)
							this->m_rotation.y -= currentMovement;
						break;

					case PATTERN_ACTION_FIRE:
						currentMovement = 0;
						if(this->m_fireTimer >= this->m_fireDelay)
						{
							for(int j = 0 ; j < this->m_fireEntitiesNumber ; j++)
								if(this->m_fireEntities[j].m_enabled)
									this->m_projectileM->addProjectile(this->m_fireEntities[j].m_projectileType,PROJECTILE_SOURCE_ENEMY,this->m_position + this->m_fireEntities[j].m_finalPosition,this->m_fireEntities[j].m_rotation);
							alSourcePlay(this->m_fireSound);
							this->m_fireTimer = 0;
						}
						break;

					case PATTERN_ACTION_LOOP:
						currentMovement = 0;
						if(this->m_patterns[this->m_patternIndex][i].m_unit >= 0 && this->m_patterns[this->m_patternIndex][i].m_unit < this->m_patternSize)
						{
							for(int j = (int)this->m_patterns[this->m_patternIndex][i].m_unit ; j < this->m_patternSize ; j++)
								for(int k = 0 ; k < this->m_actionsPerPattern ; k++)
									this->m_patterns[j][k].reset();
							this->m_patternIndex = (int)this->m_patterns[this->m_patternIndex][i].m_unit;
						}
						break;

					case PATTERN_ACTION_SLEEP:
						currentMovement = (float)time;
						this->m_sleepTimer += time;
						if(this->m_patterns[this->m_patternIndex][i].m_unit <= currentMovement)
							this->m_sleepTimer = 0;
						break;

					case PATTERN_ACTION_NOTHING:
						if(!this->m_patterns[this->m_patternIndex][i].m_over)
							this->m_patterns[this->m_patternIndex][i].m_over = true;
						break;
					}
					this->m_patterns[this->m_patternIndex][i].m_unit -= currentMovement;
					if(this->m_patterns[this->m_patternIndex][i].m_unit <= 0)
						this->m_patterns[this->m_patternIndex][i].m_over = true;
				}
			}
			this->m_spaceShip->setPosition(this->m_position);
			this->m_spaceShip->setRotation(this->m_rotation);
		}
		this->m_screenPosition = this->m_cameraM->coordinateToScreen(this->m_position);
	}
	else
		this->m_state = ENEMY_STATE_DEAD;
}

/**
* Draw the enemy
*/
void Enemy::draw()
{
	this->m_spaceShip->draw(this->m_drawState);
}
