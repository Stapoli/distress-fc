/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#ifndef __ENEMY_H
#define __ENEMY_H

#include <string>
#include <d3d9.h>
#include <d3dx9.h>
#include "../../../managers/projectileManager.h"
#include "../../../managers/cameraManager.h"
#include "../../../managers/deviceManager.h"
#include "../../../managers/meshManager.h"
#include "../../../managers/bonusManager.h"
#include "../../../managers/particleManager.h"
#include "../../../managers/soundManager.h"
#include "../../../elements/3D/model.h"
#include "../../../elements/pattern.h"
#include "../../../elements/fireEntity.h"

#define ENEMY_SMOKE_TIME 25
#define ENEMY_SMOKE_PERCENTAGE 0.3f
#define ENEMY_HIT_DELAY 100.0f
#define ENEMY_BORDER_Z_OFFSCREEN_MODIFIER 1.0f

enum enemyState
{
	ENEMY_STATE_ALIVE,
	ENEMY_STATE_DEAD
};

enum enemyType
{
	ENEMY_TYPE_FIXED,
	ENEMY_TYPE_DYNAMIC
};

enum enemyOrientation
{
	ENEMY_ORIENTATION_LEFT,
	ENEMY_ORIENTATION_RIGHT
};

/**
* Enemy class
* Generic enemy class
*/
class Enemy
{
protected:
	bool m_hit;
	int m_drawState;
	int m_state;
	int m_energy;
	int m_maxEnergy;
	int m_projectileType;
	int m_patternIndex;
	int m_patternSize;
	int m_actionsPerPattern;
	int m_type;
	int m_waveId;
	int m_drop;
	int m_score;
	int m_fireEntitiesNumber;
	float m_fireDelay;
	float m_fireTimer;
	float m_sleepTimer;
	float m_smokeTimer;
	float m_hitTimer;
	float m_speed;
	float m_rotationSpeed;
	float m_collisionRadius;
	ALuint m_explosionSound;
	ALuint m_fireSound;
	D3DXVECTOR2 m_screenPosition;
	D3DXVECTOR3 m_position;
	D3DXVECTOR3 m_rotation;
	Model * m_spaceShip;
	Model * m_turret;
	FireEntity * m_fireEntities;
	Pattern ** m_patterns;
	CameraManager * m_cameraM;
	DeviceManager * m_deviceM;
	ProjectileManager * m_projectileM;
	BonusManager * m_bonusM;
	ParticleManager * m_particleM;

public:
	Enemy(D3DXVECTOR3 position, int patternSize, int actionsPerPattern, int waveId, int drop, int score, float collisionRadius, int fireEntityNumber);
	~Enemy();
	int getState();
	int getWaveId();
	bool isCollision(D3DXVECTOR2 position, float additionalRadius);
	void takeDamage(int damage);
	void refresh(float time);
	void draw();
};

#endif
