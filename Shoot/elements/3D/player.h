/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#ifndef __PLAYER_H
#define __PLAYER_H

#include <string>
#include "../../managers/optionManager.h"
#include "../../managers/projectileManager.h"
#include "../../managers/controlManager.h"
#include "../../managers/cameraManager.h"
#include "../../managers/deviceManager.h"
#include "../../managers/lightManager.h"
#include "../../managers/interfaceManager.h"
#include "../../managers/particleManager.h"
#include "../../managers/bonusManager.h"
#include "../../managers/messageManager.h"
#include "../../managers/soundManager.h"
#include "../../managers/logManager.h"
#include "../../elements/3D/model.h"

#define SPACESHIP_SPEED 20.0f
#define SPACESHIP_FIRE_DELAY 120
#define SPACESHIP_WIDTH 1.0f
#define SPACESHIP_HEIGHT 1.5f
#define SPACESHIP_FIRE_COLLISION_RADIUS 0.8f
#define SPACESHIP_OBJECT_COLLISION_RADIUS 8.0f
#define PLAYER_NUMBER_OF_CONTINUES 2
#define PLAYER_RESPAWN_TIME 2000
#define PLAYER_RESPAWN_INTERVAL 175
#define PLAYER_IMMUNITY_TIME 1750
#define PLAYER_BORDER_X_MODIFIER 6.0f
#define PLAYER_BORDER_Z_MODIFIER 3.0f
#define PLAYER_INTRO_Z_MODIFIER 20.0f
#define PLAYER_BORDER_Z_OFFSCREEN_MODIFIER 20.0f
#define PLAYER_PARTICLES_PER_SPRITE_CONTINUE 4
#define PLAYER_PARTICLES_CONTINUE_INTERVAL -8.0f
#define PLAYER_PARTICLES_CONTINUE_SPEED 60.0f
#define PLAYER_PARTICLES_CONTINUE_RADIUS 5f
#define PLAYER_PARTICLES_CONTINUE_CENTER_Z_MODIFIER -1.0f

enum playerPowerUp
{
	PLAYER_POWERUP_BIG,
	PLAYER_POWERUP_DOUBLE,
	PLAYER_POWERUP_CURVE,
	PLAYER_POWERUP_DIAGONAL,
	PLAYER_POWERUP_LATERAL,
	PLAYER_POWERUP_CURVE2
};

enum playerState
{
	PLAYER_STATE_ALIVE,
	PLAYER_STATE_DEAD,
	PLAYER_STATE_RESPAWNING,
	PLAYER_STATE_GAMEOVER,
	PLAYER_STATE_START
};

/**
* Player class
* Handle the player
*/
class Player
{
private:
	int m_lightId;
	int m_state;
	int m_powerUpState;
	int m_continues;
	int m_score;
	int m_bombs;
	float m_fireDelay;
	float m_respawnTimer;
	float m_cockpitRadius;
	float m_objectRadius;
	float m_continuesSpriteRotation;
	ALuint m_fireSound;
	ALuint m_explosionSound;
	D3DXVECTOR2 m_screenPositionContinues;
	D3DXVECTOR2 m_screenPosition;
	D3DXVECTOR3 m_position;
	D3DXVECTOR3 m_rotation;
	Sprite *** m_continuesSprites;
	Model * m_spaceShip;
	ProjectileManager * m_projectileM;
	OptionManager * m_optionM;
	ControlManager * m_controlM;
	CameraManager * m_cameraM;
	LightManager * m_lightM;
	InterfaceManager * m_interfaceM;
	ParticleManager * m_particleM;
	BonusManager * m_bonusM;
	MessageManager * m_messageM;
	DeviceManager * m_deviceM;

public:
	Player();
	~Player();
	bool isGameOver();
	bool isAlive();
	bool isCollision(D3DXVECTOR2 position, float additionalRadius);
	bool isCollisionBonus(D3DXVECTOR2 position, float additionalRadius, int bonusType);
	void refresh(float time);
	void draw();
};

#endif
