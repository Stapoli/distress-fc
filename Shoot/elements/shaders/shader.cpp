/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include <sstream>
#include "../../elements/shaders/shader.h"

/**
* Constructor
* @param name The shader filename
*/
Shader::Shader(std::string name)
{
	std::ostringstream stream;
	this->m_deviceM = DeviceManager::getInstance();
	this->m_logM = LogManager::getInstance();

	const D3DVERTEXELEMENT9 declaration[4] =
	{
		{0, 0,  D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION,0},
		{0, 12, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD,0},
		{0, 24, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL,  0},
		D3DDECL_END()
	};

	this->m_deviceM->getDevice()->CreateVertexDeclaration(declaration,&this->m_vertexShaderDeclaration);

	HRESULT hr;
    LPD3DXBUFFER pCode;
    DWORD dwShaderFlags = 0;
	LPD3DXBUFFER pBufferErrors = NULL;

	stream << SHADER_DIRECTORY << "/" << name << ".vsh";

	hr = D3DXCompileShaderFromFile(stream.str().c_str(), NULL, NULL, "main","vs_3_0", dwShaderFlags, &pCode,&pBufferErrors, &this->m_constantTableVS );
    if(FAILED(hr))
	{
		LPVOID pCompilErrors = pBufferErrors->GetBufferPointer();
		this->m_logM->getStream() << "Vertex Shader Compilation error: " << (const char*)pCompilErrors << "\n";
		this->m_logM->write();
	}
	this->m_deviceM->getDevice()->CreateVertexShader((DWORD*)pCode->GetBufferPointer(),&this->m_vertexShader);
    pCode->Release();

	stream.str("");
	stream << SHADER_DIRECTORY << "/" << name << ".psh";

	hr = D3DXCompileShaderFromFile(stream.str().c_str(), NULL, NULL, "main","ps_3_0", dwShaderFlags, &pCode,&pBufferErrors, &this->m_constantTablePS );
    if( FAILED(hr) )
	{
		LPVOID pCompilErrors = pBufferErrors->GetBufferPointer();
		this->m_logM->getStream() << "Pixel Shader Compilation error: " << (const char*)pCompilErrors << "\n";
		this->m_logM->write();
	}
    this->m_deviceM->getDevice()->CreatePixelShader((DWORD*)pCode->GetBufferPointer(),&this->m_pixelShader);
	pCode->Release();
}

/**
* Destructor
*/
Shader::~Shader()
{
	if(this->m_vertexShader != NULL)
		this->m_vertexShader->Release();
	if(this->m_vertexShaderDeclaration != NULL)
		this->m_vertexShaderDeclaration->Release();
	if(this->m_constantTableVS != NULL)
		this->m_constantTableVS->Release();
	if(this->m_pixelShader != NULL)
		this->m_pixelShader->Release();
	if(this->m_constantTablePS != NULL)
		this->m_constantTablePS->Release();
}

/**
* Get the Vertex Shader
* @return The Vertex Shader
*/
LPDIRECT3DVERTEXSHADER9 Shader::getVertexShader()
{
	return this->m_vertexShader;
}

/**
* Get the Pixel Shader
* @return The Pixel Shader
*/
LPDIRECT3DPIXELSHADER9 Shader::getPixelShader()
{
	return this->m_pixelShader;
}

/**
* Get the Vertex Shader Constant Table
* @return The Vertex Shader Constant Table
*/
LPD3DXCONSTANTTABLE Shader::getConstantTableVS()
{
	return this->m_constantTableVS;
}

/**
* Get the Pixel Shader Constant Table
* @return The Pixel Shader Constant Table
*/
LPD3DXCONSTANTTABLE Shader::getConstantTablePS()
{
	return this->m_constantTablePS;
}

/**
* Get the Vertex Shader Declaration
* @return The Vertex Shader Declaration
*/
LPDIRECT3DVERTEXDECLARATION9 Shader::getVertexShaderDeclaration()
{
	return this->m_vertexShaderDeclaration;
}

/**
* Get a Vertex Shader handle
* @param i The Vertex Shader handle number
* @return The Vertex Shader handle
*/
D3DXHANDLE Shader::getVertexHandle(int i)
{
	return this->m_vertexHandles[i];
}

/**
* Get a Pixel Shader handle
* @param i The Pixel Shader handle number
* @return The Pixel Shader handle
*/
D3DXHANDLE Shader::getPixelHandle(int i)
{
	return this->m_pixelHandles[i];
}
