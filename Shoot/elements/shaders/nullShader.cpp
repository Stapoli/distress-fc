/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include "../../elements/shaders/nullShader.h"

/**
* Constructor
* @param shaderType The shader type
*/
NullShader::NullShader(int shaderType) : Shader("null")
{
	this->m_vertexHandles = new D3DXHANDLE[NULLSHADER_HANDLE_VERTEX_SIZE];
	this->m_pixelHandles = new D3DXHANDLE[NULLSHADER_HANDLE_PIXEL_SIZE];

	this->m_vertexHandles[NULLSHADER_HANDLE_VERTEX_WORLD] = this->m_constantTableVS->GetConstantByName(NULL,"world");
	this->m_vertexHandles[NULLSHADER_HANDLE_VERTEX_WORLDVIEWPROJ] = this->m_constantTableVS->GetConstantByName(NULL,"worldViewProj");
	this->m_vertexHandles[NULLSHADER_HANDLE_VERTEX_CAMERAEYE] = this->m_constantTableVS->GetConstantByName(NULL,"cameraEye");

	this->m_pixelHandles[NULLSHADER_HANDLE_PIXEL_LIGHTRADIUS] = this->m_constantTablePS->GetConstantByName(NULL,"lightRadius");
	this->m_pixelHandles[NULLSHADER_HANDLE_PIXEL_LIGHTPOSITION] = this->m_constantTablePS->GetConstantByName(NULL,"lightPosition");
	this->m_pixelHandles[NULLSHADER_HANDLE_PIXEL_LIGHTCOLOR] = this->m_constantTablePS->GetConstantByName(NULL,"lightColor");
	this->m_pixelHandles[NULLSHADER_HANDLE_PIXEL_LIGHTENABLED] = this->m_constantTablePS->GetConstantByName(NULL,"lightActivated");
}

/**
* Destructor
*/
NullShader::~NullShader(){}