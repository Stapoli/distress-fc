/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#ifndef __PATTERN_H
#define __PATTERN_H

enum patternList
{
	PATTERN_ACTION_NOTHING,
	PATTERN_ACTION_TRANSLATE_X,
	PATTERN_ACTION_TRANSLATE_Z,
	PATTERN_ACTION_TRANSLATE_MINUS_X,
	PATTERN_ACTION_TRANSLATE_MINUS_Z,
	PATTERN_ACTION_TRANSLATE_X_MINUS_Z,
	PATTERN_ACTION_TRANSLATE_MINUS_X_Z,
	PATTERN_ACTION_TRANSLATE_X_Z,
	PATTERN_ACTION_TRANSLATE_MINUS_X_MINUS_Z,
	PATTERN_ACTION_MOVE_FORWARD_NORMAL,
	PATTERN_ACTION_MOVE_BACKWARD_NORMAL,
	PATTERN_ACTION_MOVE_FORWARD_SLOW,
	PATTERN_ACTION_MOVE_BACKWARD_SLOW,
	PATTERN_ACTION_MOVE_FORWARD_BOOST,
	PATTERN_ACTION_MOVE_BACKWARD_BOOST,
	PATTERN_ACTION_ROTATE,
	PATTERN_ACTION_MINUS_ROTATE,
	PATTERN_ACTION_ROTATE_AIM,
	PATTERN_ACTION_MINUS_ROTATE_AIM,
	PATTERN_ACTION_FIRE,
	PATTERN_ACTION_SLEEP,
	PATTERN_ACTION_LOOP
};

/**
* Pattern class
* Generic class to handle enemy movements
*/
class Pattern
{
public:
	int m_action;
	bool m_over;
	bool m_blocking;
	float m_unit;
	float m_unitSave;

	Pattern(int action, float unit, bool blocking = true);
	Pattern();
	~Pattern();
	void reset();
};

#endif
