/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include "../../elements/2D/projectile.h"

/**
* Constructor
*/
Projectile::Projectile(std::string texture, D3DXVECTOR3 position, D3DXVECTOR2 center, int projectileSource, float speed, float orientation, float deviation, int power, float radius) : Sprite(texture, D3DXVECTOR2(position.x, position.z), center)
{
	this->m_worldPosition = position;
	this->m_source = projectileSource;
	this->m_speed = speed;
	this->m_orientation = orientation;
	this->m_rot = orientation / 180.0f * PI;
	this->m_deviation = deviation;
	this->m_power = power;
	this->m_radius = radius;

	this->m_cameraM = CameraManager::getInstance();
	this->m_alive = true;
}

/**
* Constructor
*/
Projectile::Projectile() : Sprite()
{
	this->m_cameraM = CameraManager::getInstance();
	this->m_alive = false;
}

/**
* Destructor
*/
Projectile::~Projectile(){}

/**
* Get the projectile source
* @return The projectile source
*/
int Projectile::getSource()
{
	return this->m_source;
}

/**
* Get the projectile power
* @return The projectile power
*/
int Projectile::getPower()
{
	return this->m_power;
}

/**
* Get the projectile radius
* @return The projectile radius
*/
float Projectile::getRadius()
{
	return this->m_radius;
}

/**
* Get the projectile orientation
* @return The projectile orientation
*/
float Projectile::getOrientation()
{
	return this->m_orientation;
}

/**
* Test a collision
* @param position Coordinate of the target
* @return The collision state
*/
bool Projectile::isCollision(D3DXVECTOR3 position)
{
	return false;
}

/**
* Check if the projectile is alive
* @return The projectile state
*/
bool Projectile::isAlive()
{
	return this->m_alive;
}

/**
* Return the screen position
* @return The screen position
*/
D3DXVECTOR2 Projectile::getScreenPosition()
{
	return this->m_pos;
}

/**
* Return the world position
* @return The world position
*/
D3DXVECTOR3 Projectile::getWorldPosition()
{
	return this->m_worldPosition;
}

/**
* Change the projectile state
* @param b The projectile state
*/
void Projectile::setAlive(bool b)
{
	this->m_alive = b;
}

/**
* Set the projectile
* @param texture The texture path
* @param position The world position
* @param center The sprite center coordinate
* @param projectileSource The sourceof the projectile
* @param speed The particle speed
* @param orientation The particle orientation
* @param deviation The particle deviation
* @param power The particle power
* @param radius The particle radius
*/
void Projectile::setProjectile(std::string texture, D3DXVECTOR3 position, D3DXVECTOR2 center, int projectileSource, float speed, float orientation, float deviation, int power, float radius)
{
	this->m_center = center;
	this->m_texture = this->m_textureM->loadTexture(texture);
	this->m_worldPosition = position;
	this->m_source = projectileSource;
	this->m_speed = speed;
	this->m_orientation = orientation;
	this->m_rot = orientation / 180.0f * PI;
	this->m_deviation = deviation;
	this->m_power = power;

	setPosition(m_cameraM->coordinateToScreen(this->m_worldPosition));

	this->m_alive = true;
}

/**
* Refresh the projectile
* @param time Elapsed time
*/
void Projectile::refresh(float time)
{
	this->m_orientation += this->m_deviation * time / 1000.0f;
	this->m_rot = this->m_orientation / 180.0f * PI;
	this->m_worldPosition.x += sin(this->m_orientation / 180.0f * PI) * this->m_speed * time / 1000.0f;
	this->m_worldPosition.z += cos(this->m_orientation / 180.0f * PI) * this->m_speed * time / 1000.0f;

	setPosition(m_cameraM->coordinateToScreen(this->m_worldPosition));
}
