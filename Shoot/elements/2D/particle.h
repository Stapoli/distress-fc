/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#ifndef __PARTICLE_H
#define __PARTICLE_H

#include "../../misc/var.h"
#include "../../managers/cameraManager.h"
#include "../../elements/2D/sprite.h"

/**
* Particle class
* Handle particles
*/
class Particle : public Sprite
{
private:
	bool m_alive;
	float m_startAlpha;
	float m_maxLife;
	float m_life;
	float m_speed;
	float m_orientation;
	float m_grown;
	D3DXVECTOR3 m_worldPosition;
	CameraManager * m_cameraM;

public:
	Particle();
	~Particle();
	bool isAlive();
	D3DXVECTOR2 getScreenPosition();
	D3DXVECTOR3 getWorldPosition();
	void setAlive(bool b);
	void setParticle(std::string texture, D3DXVECTOR3 position, D3DXVECTOR2 center, float speed, float orientation, float scale, float lifeTime, float grown, float startAlpha);
	void refresh(float time);
};

#endif
