/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include "../../elements/2D/border.h"

/**
* Constructor
* @param worldPosition The world position
* @param placement The screen placement (BORDER_PLACEMENT_LEFT or BORDER_PLACEMENT_RIGHT)
*/
Border::Border(D3DXVECTOR3 worldPosition, int placement) : Sprite()
{
	int width, height;
	D3DXVECTOR2 tmp = CameraManager::getInstance()->coordinateToScreen(worldPosition);
	this->m_center = D3DXVECTOR2(0,0);
	switch(placement)
	{
	case BORDER_PLACEMENT_LEFT:
		width = (int)tmp.x;
		setPosition(D3DXVECTOR2(0,0));
		break;

	case BORDER_PLACEMENT_RIGHT:
		width = OptionManager::getInstance()->getWidth() - (int)tmp.x;
		setPosition(D3DXVECTOR2(tmp.x,0));
		break;
	}
	height = OptionManager::getInstance()->getHeight();
	this->m_texture = this->m_textureM->loadTexture("imgs/border.png");

	scaleSprite((float)width,(float)height);
}

/**
* Destructor
*/
Border::~Border(){}
