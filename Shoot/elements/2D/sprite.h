/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#ifndef __SPRITE_H
#define __SPRITE_H

#include <string>
#include <d3d9.h>
#include <d3dx9.h>
#include "../../managers/deviceManager.h"
#include "../../managers/textureManager.h"

/**
* Sprite class
* Generic sprite class that handle all the 2d objects
*/
class Sprite
{
protected:
	float m_rot;
	D3DXMATRIX m_matrix;
	D3DXVECTOR2 m_pos;
	D3DXVECTOR2 m_center;
	D3DXVECTOR2 m_scale;
	D3DXVECTOR2 m_finalTranslation;
	D3DXCOLOR m_color;
	LPD3DXSPRITE m_sprite;
	LPDIRECT3DTEXTURE9 m_texture;
	DeviceManager * m_deviceM;
	TextureManager * m_textureM;

public:
	Sprite(std::string texture, D3DXVECTOR2 position, D3DXVECTOR2 center);
	Sprite();
	~Sprite();
	LPD3DXSPRITE getSprite();
	D3DXVECTOR2 getPosition();
	void setColor(D3DXCOLOR color);
	void setPosition(D3DXVECTOR2 position);
	void scaleSprite(float sx, float sy);
	void draw();
};

#endif
