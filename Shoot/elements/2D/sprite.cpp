/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include "../../elements/2D/sprite.h"

/**
* Constructor
* @param texture The texture filename
* @param position The screen position
* @param center The center coordinate
*/
Sprite::Sprite(std::string texture, D3DXVECTOR2 position, D3DXVECTOR2 center)
{
	this->m_deviceM = DeviceManager::getInstance();
	this->m_center = center;
	setPosition(position);
	this->m_rot = 0;
	this->m_scale = D3DXVECTOR2(1,1);
	this->m_color = 0xFFFFFFFF;
	D3DXCreateSprite(this->m_deviceM->getDevice(),&this->m_sprite);
	this->m_textureM = TextureManager::getInstance();
	this->m_texture = this->m_textureM->loadTexture(texture);
}

/**
* Constructor
*/
Sprite::Sprite()
{
	this->m_deviceM = DeviceManager::getInstance();
	D3DXCreateSprite(this->m_deviceM->getDevice(),&this->m_sprite);
	this->m_textureM = TextureManager::getInstance();
	this->m_rot = 0;
	this->m_scale = D3DXVECTOR2(1,1);
	this->m_color = 0xFFFFFFFF;
}

/**
* Destructor
*/
Sprite::~Sprite()
{
	if(this->m_sprite != NULL)
	{
		this->m_sprite->Release();
	}
}

/**
* Get the sprite
* @return The sprite
*/
LPD3DXSPRITE Sprite::getSprite()
{
	return this->m_sprite;
}

/**
* Get the position
* @return The position
*/
D3DXVECTOR2 Sprite::getPosition()
{
	return this->m_pos;
}

/**
* Change the color
* @param color The color
*/
void Sprite::setColor(D3DXCOLOR color)
{
	this->m_color = color;
}

/**
* Change the position
* @param position The position
*/
void Sprite::setPosition(D3DXVECTOR2 position)
{
	this->m_pos = position;
	this->m_finalTranslation = this->m_pos - this->m_center;
}

/**
* Scale the sprite
* @param sx X scale factor
* @param sy Y scale factor
*/
void Sprite::scaleSprite(float sx, float sy)
{
	this->m_scale = D3DXVECTOR2(sx, sy);
}

/**
* Draw the sprite
*/
void Sprite::draw()
{
	D3DXMatrixTransformation2D(&this->m_matrix,NULL,0.0,&this->m_scale,&this->m_center,this->m_rot,&this->m_finalTranslation);
	this->m_sprite->SetTransform(&this->m_matrix);
	this->m_sprite->Begin(D3DXSPRITE_ALPHABLEND);
	this->m_sprite->Draw(this->m_texture,NULL,NULL,NULL,this->m_color);
	this->m_sprite->End();
}
