/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include "../../elements/2D/particle.h"

/**
* Constructor
*/
Particle::Particle() : Sprite()
{
	this->m_cameraM = CameraManager::getInstance();
	this->m_alive = false;
}

/**
* Destructor
*/
Particle::~Particle(){}

/**
* Check is the particle is alive
* @return The particle state
*/
bool Particle::isAlive()
{
	return this->m_alive;
}

/**
* Get the screen position
* @return The screen position
*/
D3DXVECTOR2 Particle::getScreenPosition()
{
	return this->m_pos;
}

/**
* Get the world position
* @return The world position
*/
D3DXVECTOR3 Particle::getWorldPosition()
{
	return this->m_worldPosition;
}

/**
* Change the particle state
* @param b The particle state
*/
void Particle::setAlive(bool b)
{
	this->m_alive = b;
}

/**
* Set the particle
* @param texture The texture path
* @param position The world position
* @param center The sprite center coordinate
* @param speed The speed
* @param orientation The orientation
* @param scale The sprite scale
* @param lifeTime The particle lifeTime
* @param grown The grown factor
* @param startAlpha The default alpha value
*/
void Particle::setParticle(std::string texture, D3DXVECTOR3 position, D3DXVECTOR2 center, float speed, float orientation, float scale, float lifeTime, float grown, float startAlpha)
{
	this->m_center = center;
	this->m_texture = this->m_textureM->loadTexture(texture);
	this->m_worldPosition = position;
	this->m_speed = speed;
	this->m_orientation = orientation;
	this->m_scale.x = scale;
	this->m_scale.y = scale;
	this->m_maxLife = lifeTime;
	this->m_life = 0;
	this->m_grown = grown;
	this->m_startAlpha = startAlpha;
	this->m_alive = true;
}

/**
* Refresh the particle
* @param time Elapsed time
*/
void Particle::refresh(float time)
{
	this->m_life += time;
	if(this->m_life < this->m_maxLife)
	{
		this->m_worldPosition.x += sin(this->m_orientation / 180.0f * PI) * this->m_speed * time / 1000.0f;
		this->m_worldPosition.z += cos(this->m_orientation / 180.0f * PI) * this->m_speed * time / 1000.0f;

		this->m_scale.x += this->m_grown * time / 1000.0f;
		this->m_scale.y += this->m_grown * time / 1000.0f;

		this->m_center.x += this->m_grown * time / 1000.0f;
		this->m_center.y += this->m_grown * time / 1000.0f;

		this->m_color.a = this->m_startAlpha - (this->m_life / this->m_maxLife) * this->m_startAlpha;

		setPosition(m_cameraM->coordinateToScreen(this->m_worldPosition));
	}
	else
		this->m_alive = false;
}
