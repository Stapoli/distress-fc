/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include "../../elements/2D/message.h"

/**
* Constructor
*/
Message::Message()
{
	this->m_fontM = FontManager::getInstance();
	this->m_alive = false;
}

/**
* Destructor
*/
Message::~Message(){}

/**
* Check if the message is alive
* @return The message state
*/
bool Message::isAlive()
{
	return this->m_alive;
}

/**
* Change the message state
* @param b State
*/
void Message::setAlive(bool b)
{
	this->m_alive = b;
}

/**
* Set the message
* @param fontDescName The font descriptor name
* @param fontName The font name
* @param text The text
* @param fontHeight The font heigh in pixel
* @param fontWidth The font width in pixel
* @param fontWeight The font weight
* @param italic Change the italic state of the message
* @param alignment Set the alignment
* @param startX The start x coordinate
* @param startY The start y coordinate
* @param endX The end x coordinate
* @param endY The end y coordinate
* @param color The message color
* @param life The message lifeTime
*/
void Message::setMessage(std::string fontDescName, std::string fontName, std::string text, int fontHeight, int fontWidth, int fontWeight, bool italic, int alignment, int startX, int startY, int endX, int endY, D3DXCOLOR color, float life)
{
	this->m_fontDescName = fontDescName;
	this->m_text = text;
	this->m_alignment = alignment;
	this->m_startX = startX;
	this->m_startY = startY;
	this->m_endX = endX;
	this->m_endY = endY;
	this->m_color = color;
	this->m_life = 0;
	this->m_lifeMax = life;
	this->m_alive = true;

	this->m_fontM->addFont(fontDescName,fontName,fontHeight,fontWidth,fontWeight,italic);
}

/**
* Refresh the message
* @param time Elapsed time
*/
void Message::refresh(float time)
{
	this->m_life += time;
	if(this->m_life < this->m_lifeMax)
	{
		if(this->m_life < MESSAGE_FADE_TIME)
		{
			this->m_color.a = this->m_life / (float)MESSAGE_FADE_TIME;
		}
		else
		{
			if(this->m_life < this->m_lifeMax - MESSAGE_FADE_TIME)
				this->m_color.a = 1;
			else
				this->m_color.a = (this->m_lifeMax - this->m_life) / (float)MESSAGE_FADE_TIME;
		}
	}
	else
		this->m_alive = false;
}

/**
* Draw the message
*/
void Message::draw()
{
	this->m_fontM->drawText(this->m_fontDescName,this->m_text,this->m_alignment,this->m_startX,this->m_startY,this->m_endX, this->m_endY,this->m_color);
}
