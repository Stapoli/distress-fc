/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include "../managers/particleManager.h"

/**
* Constructor
*/
ParticleManager::ParticleManager(){}

/**
* Destructor
*/
ParticleManager::~ParticleManager()
{
	delete this->m_particles;
}

/**
* Initialize the manager
*/
void ParticleManager::initialize()
{
	this->m_optionM = OptionManager::getInstance();
	this->m_particles = new Particle[MAX_PARTICLES];
	preload();
}

/**
* Preload the textures
*/
void ParticleManager::preload()
{
	//Preloading textures
	TextureManager * textureM = TextureManager::getInstance();
	textureM->loadTexture("imgs/particles/explosion1.png");
	textureM->loadTexture("imgs/particles/explosion2.png");
	textureM->loadTexture("imgs/particles/spark.png");
	textureM->loadTexture("imgs/particles/smoke.png");
}

/**
* Add a particle
* @param particleType The particle type
* @param position The particle starting position
* @param orientation The particle orientation
* @param numberOfInstance The number of particle instance to create
*/
void ParticleManager::addParticle(int particleType, D3DXVECTOR3 position, float orientation, int numberOfInstance)
{
	D3DXVECTOR3 randPosition;
	D3DXVECTOR2 center;
	float scale;

	for(int j = 0 ; j < numberOfInstance ; j++)
	{
		int index = -1;
		for(int i = 0 ; i < MAX_PARTICLES && index == -1 ; i++)
			if(!this->m_particles[i].isAlive())
				index = i;

		if(index > -1)
		{
			randPosition = position;

			switch(particleType)
			{
				case PARTICLE_EXPLOSION:
					randPosition.x += (rand() %4) - 2.0f;
					randPosition.z += (rand() %4) - 2.0f;
					scale = (((rand() % 15) + 10)) / 10.0f * this->m_optionM->getResizeFactor();
					center.x = 32 * scale;
					center.y = 32 * scale;
					this->m_stream.str("");
					this->m_stream << "imgs/particles/explosion" << ((rand() % 2) + 1) << ".png";
					this->m_particles[index].setParticle(this->m_stream.str(), randPosition, center, 0.0f, orientation, scale, (float)((rand() % 250) + 250), (rand() % 10) / 10.0f, 1.0f);
					break;

				case PARTICLE_SPARK:
					scale = this->m_optionM->getResizeFactor();
					center.x = 8 * scale;
					center.y = 8 * scale;
					this->m_stream.str("");
					this->m_stream << "imgs/particles/spark.png";
					this->m_particles[index].setParticle(this->m_stream.str(), randPosition, center, 40.0f, orientation + (rand() % 45) - 22, scale, (float)((rand() % 50) + 100), 0, 1.0f);
					break;

				case PARTICLE_SMOKE:
					randPosition.x += (rand() %10) / 20.0f - 0.25f;
					randPosition.z += (rand() %10) / 20.0f - 0.25f;
					scale = (((rand() % 25) + 10)) / 10.0f * this->m_optionM->getResizeFactor();
					center.x = 16 * scale;
					center.y = 16 * scale;
					this->m_stream.str("");
					this->m_stream << "imgs/particles/smoke.png";
					this->m_particles[index].setParticle(this->m_stream.str(), randPosition, center, 0.0f, orientation, scale, (float)((rand() % 500) + 500), 0, 0.3f);
					break;
			}
		}
	}
}

/**
* Refresh the particles
* @param time Elapsed time
*/
void ParticleManager::refresh(float time)
{
	for(int i = 0 ; i < MAX_PARTICLES ; i++)
		if(this->m_particles[i].isAlive())
			this->m_particles[i].refresh(time);
}

/**
* Draw the particle
*/
void ParticleManager::draw()
{
	for(int i = 0 ; i < MAX_PARTICLES ; i++)
		if(this->m_particles[i].isAlive())
			this->m_particles[i].draw();
}

/**
* Free the manager database
*/
void ParticleManager::freeDatabase()
{
	for(int i = 0 ; i < MAX_PARTICLES ; i++)
		if(this->m_particles[i].isAlive())
			this->m_particles[i].setAlive(false);
}
