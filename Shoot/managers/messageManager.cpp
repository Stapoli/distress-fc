/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include "../managers/messageManager.h"

/**
* Constructor
*/
MessageManager::MessageManager(){}

/**
* Destructor
*/
MessageManager::~MessageManager()
{
	freeDatabase();
}

/**
* Initialize the manager
*/
void MessageManager::initialize()
{
	this->m_optionM = OptionManager::getInstance();
	this->m_messages = new Message[MAX_MESSAGES];
}

/**
* Add a message in the message database
* @param messageType The message type
* @param life The life time of the message
*/
void MessageManager::addMessage(int messageType, float life)
{
	int fontSize;
	int index = -1;
		for(int i = 0 ; i < MAX_MESSAGES && index == -1 ; i++)
			if(!this->m_messages[i].isAlive())
				index = i;

	if(index > -1)
	{
		switch(messageType)
		{
		case MESSAGE_TYPE_IN_GAME_BEGIN:
			fontSize = (int)(10 * this->m_optionM->getResizeFactor());
			this->m_messages[index].setMessage("BEGIN_INGAME", "Courrier", "Op�rateur: S.P.E.S d�poy�, attaque imminente!", 0, fontSize, FW_NORMAL, false, FONTMANAGER_ALIGN_CENTER, 0, this->m_optionM->getHeight() / 8 - fontSize / 2, this->m_optionM->getWidth(), this->m_optionM->getHeight() / 8 + fontSize / 2, 0xFFFF5555, life);
			break;

		case MESSAGE_TYPE_IN_GAME_BOSS:
			fontSize = (int)(10 * this->m_optionM->getResizeFactor());
			this->m_messages[index].setMessage("BOSS_INGAME", "Courrier", "WARNING", 0, fontSize, FW_NORMAL, false, FONTMANAGER_ALIGN_CENTER, 0, this->m_optionM->getHeight() / 8 - fontSize / 2, this->m_optionM->getWidth(), this->m_optionM->getHeight() / 8 + fontSize / 2, 0xFFFFFFFF, life);
			break;

		case MESSAGE_TYPE_IN_GAME_GAME_OVER:
			fontSize = (int)(10 * this->m_optionM->getResizeFactor());
			this->m_messages[index].setMessage("GAMEOVER_INGAME", "Courrier", "Op�rateur: S.P.E.S d�truit, Animus r�cup�r�.", 0, fontSize, FW_NORMAL, false, FONTMANAGER_ALIGN_CENTER, 0, this->m_optionM->getHeight() / 8 - fontSize / 2, this->m_optionM->getWidth(), this->m_optionM->getHeight() / 8 + fontSize / 2, 0xFFFF5555, life);
			break;
		}
	}
}

/**
* Refresh the messages
* @param time Elapsed time
*/
void MessageManager::refresh(float time)
{
	for(int i = 0 ; i < MAX_MESSAGES ; i++)
		if(this->m_messages[i].isAlive())
			this->m_messages[i].refresh(time);
}

/**
* Draw the messages
*/
void MessageManager::draw()
{
	for(int i = 0 ; i < MAX_MESSAGES ; i++)
		if(this->m_messages[i].isAlive())
			this->m_messages[i].draw();
}

/**
* Free the manager database
*/
void MessageManager::freeDatabase()
{
	for(int i = 0 ; i < MAX_MESSAGES ; i++)
		if(this->m_messages[i].isAlive())
			this->m_messages[i].setAlive(false);
}
