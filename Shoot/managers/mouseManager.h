/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#ifndef __MOUSEMANAGER_H
#define __MOUSEMANAGER_H

#define CURSOR_FILE "imgs/cursor2.png"
#define KEYDOWN( buffer, key ) ( buffer[key] & 0x80 )

enum mouseButtonList
{
	MOUSE_BUTTON_ENTER_MENU,
	MOUSE_BUTTON_RETURN_MENU,
	MOUSE_BUTTON_SIZE
};

enum mouseButtonIndex
{
	MOUSE_BUTTON_INDEX_ASSIGNED,
	MOUSE_BUTTON_INDEX_STATE,
	MOUSE_BUTTON_INDEX_SIZE
};

enum mouseButtonState
{
	MOUSE_BUTTON_STATE_UP,
	MOUSE_BUTTON_STATE_DOWN,
	MOUSE_BUTTON_STATE_FROZEN
};

#include "../elements/2D/sprite.h"
#include "../managers/logManager.h"
#include "../managers/singletonManager.h"
#include "../managers/deviceManager.h"

/**
* MouseManager class
* Handle the mouse events
*/
class MouseManager : public SingletonManager<MouseManager>
{
friend class SingletonManager<MouseManager>;

private:
	bool m_hasMoved;
	int m_pos[2];
	int ** m_buttons;
	Sprite * m_cursor;
	DIMOUSESTATE m_state;
	LPDIRECTINPUTDEVICEA m_mouse;
	DeviceManager * m_deviceM;
	OptionManager * m_optionM;
	LogManager * m_logM;

public:
	~MouseManager();
	int getX();
	int getY();
	bool isButtonDown(int button);
	bool hasMoved();
	void initialize();
	void refresh();
	void restore();
	void release();
	void showCursor();
	void freezeKeys();
	void releaseResources();
	void restoreResources();
	
private:
	MouseManager();
};

#endif
