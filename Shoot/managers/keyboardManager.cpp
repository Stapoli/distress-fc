/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include "../misc/var.h"
#include "../managers/keyboardManager.h"

/**
* Constructor
*/
KeyboardManager::KeyboardManager(){}

/**
* Destructor
*/
KeyboardManager::~KeyboardManager()
{
	for(int i = 0 ; i < ACTION_KEY_SIZE ; i++)
		delete this->m_keys[i];
	delete this->m_keys;
	this->m_keys = NULL;

	release();
}

/**
* Check if a key is down
* @param key The key number
* @return If the key is down
*/
bool KeyboardManager::isKeyDown(int key)
{
	return (this->m_keys[key][KEYBOARD_KEY_INDEX_ASSIGNED] != ACTION_KEY_DISABLED && this->m_keys[key][KEYBOARD_KEY_INDEX_STATE] == KEYBOARD_KEY_STATE_DOWN);
}

/**
* Initialize the manager
*/
void KeyboardManager::initialize()
{
	HRESULT result;
	this->m_deviceM = DeviceManager::getInstance();
	this->m_logM = LogManager::getInstance();

	result = this->m_deviceM->getInput()->CreateDevice(GUID_SysKeyboard, &this->m_keyboard, NULL);
	if(FAILED(result))
		this->m_logM->getStream() << "Unable to create the keyboard device\n";

	result = this->m_keyboard->SetDataFormat(&c_dfDIKeyboard);
	if(FAILED(result))
		this->m_logM->getStream() << "Unable set the data format for the keyboard\n";

	result = this->m_keyboard->SetCooperativeLevel(this->m_deviceM->getHwnd(), DISCL_FOREGROUND | DISCL_NONEXCLUSIVE);
	if(FAILED(result))
		this->m_logM->getStream() << "Unable to set the cooperation level for the keyboard\n";

	this->m_keyboard->Acquire();

	this->m_keys = new int*[ACTION_KEY_SIZE];
	for(int i = 0 ; i < ACTION_KEY_SIZE ; i++)
		this->m_keys[i] = new int[KEYBOARD_KEY_INDEX_SIZE];

	//Key assignment
	this->m_keys[ACTION_KEY_UP_MENU][KEYBOARD_KEY_INDEX_ASSIGNED] = DIK_UPARROW;
	this->m_keys[ACTION_KEY_DOWN_MENU][KEYBOARD_KEY_INDEX_ASSIGNED] = DIK_DOWNARROW;
	this->m_keys[ACTION_KEY_LEFT_MENU][KEYBOARD_KEY_INDEX_ASSIGNED] = DIK_LEFTARROW;
	this->m_keys[ACTION_KEY_RIGHT_MENU][KEYBOARD_KEY_INDEX_ASSIGNED] = DIK_RIGHTARROW;
	this->m_keys[ACTION_KEY_ESCAPE_MENU][KEYBOARD_KEY_INDEX_ASSIGNED] = DIK_ESCAPE;
	this->m_keys[ACTION_KEY_ENTER_MENU][KEYBOARD_KEY_INDEX_ASSIGNED] = DIK_RETURN;
	this->m_keys[ACTION_KEY_UP_ARROW][KEYBOARD_KEY_INDEX_ASSIGNED] = GetPrivateProfileInt("keyboard", "up_arrow", DIK_UPARROW, INI_FILENAME);
	this->m_keys[ACTION_KEY_DOWN_ARROW][KEYBOARD_KEY_INDEX_ASSIGNED] = GetPrivateProfileInt("keyboard", "down_arrow", DIK_DOWNARROW, INI_FILENAME);
	this->m_keys[ACTION_KEY_LEFT_ARROW][KEYBOARD_KEY_INDEX_ASSIGNED] = GetPrivateProfileInt("keyboard", "left_arrow", DIK_LEFTARROW, INI_FILENAME);
	this->m_keys[ACTION_KEY_RIGHT_ARROW][KEYBOARD_KEY_INDEX_ASSIGNED] = GetPrivateProfileInt("keyboard", "right_arrow", DIK_RIGHTARROW, INI_FILENAME);
	this->m_keys[ACTION_KEY_FIRE1][KEYBOARD_KEY_INDEX_ASSIGNED] = GetPrivateProfileInt("keyboard", "fire", DIK_SPACE, INI_FILENAME);

	//Key assignment verification
	for(int i = 0 ; i < ACTION_KEY_SIZE ; i++)
		if(this->m_keys[i][KEYBOARD_KEY_INDEX_ASSIGNED] < 0 || this->m_keys[i][KEYBOARD_KEY_INDEX_ASSIGNED] >= sizeof(this->m_keysBuffer))
			this->m_keys[i][KEYBOARD_KEY_INDEX_ASSIGNED] = ACTION_KEY_DISABLED;

	//All the keys to the UP state
	for(int i = 0 ; i < ACTION_KEY_SIZE ; i++)
		this->m_keys[i][KEYBOARD_KEY_INDEX_STATE] = KEYBOARD_KEY_STATE_UP;

	//Initialization of keys that are linked to another
	this->m_keys[ACTION_KEY_UP_MENU][KEYBOARD_KEY_INDEX_LINKED_KEY] = ACTION_KEY_DOWN_MENU;
	this->m_keys[ACTION_KEY_DOWN_MENU][KEYBOARD_KEY_INDEX_LINKED_KEY] = ACTION_KEY_UP_MENU;
	this->m_keys[ACTION_KEY_LEFT_MENU][KEYBOARD_KEY_INDEX_LINKED_KEY] = ACTION_KEY_RIGHT_MENU;
	this->m_keys[ACTION_KEY_RIGHT_MENU][KEYBOARD_KEY_INDEX_LINKED_KEY] = ACTION_KEY_LEFT_MENU;
	this->m_keys[ACTION_KEY_ESCAPE_MENU][KEYBOARD_KEY_INDEX_LINKED_KEY] = -1;
	this->m_keys[ACTION_KEY_ENTER_MENU][KEYBOARD_KEY_INDEX_LINKED_KEY] = -1;
	this->m_keys[ACTION_KEY_UP_ARROW][KEYBOARD_KEY_INDEX_LINKED_KEY] = ACTION_KEY_DOWN_ARROW;
	this->m_keys[ACTION_KEY_DOWN_ARROW][KEYBOARD_KEY_INDEX_LINKED_KEY] = ACTION_KEY_UP_ARROW;
	this->m_keys[ACTION_KEY_LEFT_ARROW][KEYBOARD_KEY_INDEX_LINKED_KEY] = ACTION_KEY_RIGHT_ARROW;
	this->m_keys[ACTION_KEY_RIGHT_ARROW][KEYBOARD_KEY_INDEX_LINKED_KEY] = ACTION_KEY_LEFT_ARROW;
	this->m_keys[ACTION_KEY_FIRE1][KEYBOARD_KEY_INDEX_LINKED_KEY] = -1;

	this->m_logM->write();
}

/**
* Change a key state
* @param key The key number
* @param state The new state
*/
void KeyboardManager::setKeyState(int key, int state)
{
	this->m_keys[key][KEYBOARD_KEY_INDEX_STATE] = state;
}

/**
* Refresh the keyboard
*/
void KeyboardManager::refresh()
{
	HRESULT res = this->m_keyboard->GetDeviceState(sizeof(this->m_keysBuffer),(LPVOID)&this->m_keysBuffer);
	if(FAILED(res))
	{
		res = this->m_keyboard->Acquire();

		while(res == DIERR_INPUTLOST)    
			 res = this->m_keyboard->Acquire();

		if (SUCCEEDED(res))
			this->m_keyboard->GetDeviceState(sizeof(this->m_keysBuffer), (LPVOID)&this->m_keysBuffer);
	}

	//If the keyboard is acquired, we update the keys
	if(SUCCEEDED(res))
	{
		//Refresh the keys status
		for(int i = 0 ; i < ACTION_KEY_SIZE ; i++)
		{
			if(this->m_keys[i][KEYBOARD_KEY_INDEX_ASSIGNED] != ACTION_KEY_DISABLED)
			{
				//Key down => we update the key variable if necessary
				if(KEYDOWN(this->m_keysBuffer, this->m_keys[i][KEYBOARD_KEY_INDEX_ASSIGNED]))
				{
					if(this->m_keys[i][KEYBOARD_KEY_INDEX_STATE] == KEYBOARD_KEY_STATE_UP)
					{
						this->m_keys[i][KEYBOARD_KEY_INDEX_STATE] = KEYBOARD_KEY_STATE_DOWN;
						if(this->m_keys[i][KEYBOARD_KEY_INDEX_LINKED_KEY] > -1 && this->m_keys[this->m_keys[i][KEYBOARD_KEY_INDEX_LINKED_KEY]][KEYBOARD_KEY_INDEX_STATE] == KEYBOARD_KEY_STATE_DOWN)
							this->m_keys[this->m_keys[i][KEYBOARD_KEY_INDEX_LINKED_KEY]][KEYBOARD_KEY_INDEX_STATE] = KEYBOARD_KEY_STATE_DOWN_WAIT;
					}
				}
				else
				{
					//Key up => if KEYBOARD_KEY_STATE_DOWN, change to KEYBOARD_KEY_STATE_UP and change it's linked key to KEYBOARD_KEY_STATE_DOWN if necessary
					if(this->m_keys[i][KEYBOARD_KEY_INDEX_STATE] == KEYBOARD_KEY_STATE_DOWN)
					{
						this->m_keys[i][KEYBOARD_KEY_INDEX_STATE] = KEYBOARD_KEY_STATE_UP;
						if(this->m_keys[i][KEYBOARD_KEY_INDEX_LINKED_KEY] > -1 && this->m_keys[this->m_keys[i][KEYBOARD_KEY_INDEX_LINKED_KEY]][KEYBOARD_KEY_INDEX_STATE] == KEYBOARD_KEY_STATE_DOWN_WAIT)
							this->m_keys[this->m_keys[i][KEYBOARD_KEY_INDEX_LINKED_KEY]][KEYBOARD_KEY_INDEX_STATE] = KEYBOARD_KEY_STATE_DOWN;
					}
					else
					{
						//If a frozen key is not down, we set it to up
						if(!KEYDOWN(this->m_keysBuffer, this->m_keys[i][KEYBOARD_KEY_INDEX_ASSIGNED]) && this->m_keys[i][KEYBOARD_KEY_INDEX_STATE] == KEYBOARD_KEY_STATE_FROZEN)
							this->m_keys[i][KEYBOARD_KEY_INDEX_STATE] = KEYBOARD_KEY_STATE_UP;
						else
							//Key up => if KEYBOARD_KEY_STATE_DOWN_WAIT, change to KEYBOARD_KEY_STATE_UP
							if(this->m_keys[i][KEYBOARD_KEY_INDEX_STATE] == KEYBOARD_KEY_STATE_DOWN_WAIT)
								this->m_keys[i][KEYBOARD_KEY_INDEX_STATE] = KEYBOARD_KEY_STATE_UP;
					}
				}
			}
		}
	}
}

/**
* Restore the keyboard
*/
void KeyboardManager::restore()
{
	for(int i = 0 ; i < ACTION_KEY_SIZE ; i++)
		this->m_keys[i][KEYBOARD_KEY_INDEX_STATE] = KEYBOARD_KEY_STATE_UP;
}

/**
* Freeze the keyboard
*/
void KeyboardManager::freezeKeys()
{
	for(int i = 0 ; i < ACTION_KEY_SIZE ; i++)
		this->m_keys[i][KEYBOARD_KEY_INDEX_STATE] = KEYBOARD_KEY_STATE_FROZEN;
}

/**
* Release the keyboard
*/
void KeyboardManager::release()
{
	if(this->m_keyboard) 
	{ 
		this->m_keyboard->Unacquire(); 
		this->m_keyboard->Release();
		this->m_keyboard = NULL; 
	}
}