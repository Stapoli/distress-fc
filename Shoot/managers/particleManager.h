/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#ifndef __PARTICLEMANAGER_H
#define __PARTICLEMANAGER_H

#include <sstream>
#include "../elements/2D/particle.h"
#include "../managers/textureManager.h"
#include "../managers/optionManager.h"
#include "../managers/singletonManager.h"

#define MAX_PARTICLES 200

enum 
{
	PARTICLE_EXPLOSION,
	PARTICLE_SPARK,
	PARTICLE_SMOKE
};

/**
* ParticleManager class
* Handle the particles called during the game
*/
class ParticleManager : public SingletonManager<ParticleManager>
{
friend class SingletonManager<ParticleManager>;

private:
	std::ostringstream m_stream;
	Particle * m_particles;
	OptionManager * m_optionM;

public:
	~ParticleManager();
	void initialize();
	void preload();
	void addParticle(int particleType, D3DXVECTOR3 position, float orientation, int numberOfInstance);
	void refresh(float time);
	void draw();
	void freeDatabase();
	
private:
	ParticleManager();
};

#endif
