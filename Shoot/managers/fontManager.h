/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#ifndef __POLICEMANAGER_H
#define __POLICEMANAGER_H

#include <string>
#include <map>
#include <d3d9.h>
#include <d3dx9.h>
#include "../managers/optionManager.h"
#include "../managers/deviceManager.h"
#include "../managers/singletonManager.h"

enum
{
	FONTMANAGER_ALIGN_LEFT,
	FONTMANAGER_ALIGN_CENTER,
	FONTMANAGER_ALIGN_RIGHT
};

/**
* FontManager class
* Handle the fonts
*/
class FontManager : public SingletonManager<FontManager>
{
friend class SingletonManager<FontManager>;

private:
	std::map<std::string,LPD3DXFONT>m_fonts;
	DeviceManager * m_deviceM;
	LogManager * m_logM;

public:
	~FontManager();
	bool fontExists(std::string fontDescName);
	void initialize();
	void freeDatabase();
	void addFont(std::string fontDescName, std::string fontName, int height, int width, int weight, bool italic);
	void drawText(std::string fontDescName, std::string text, int format, int posX, int posY, int endX, int endY, D3DCOLOR color);

private:
	FontManager();
};

#endif
