/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include "../managers/fontManager.h"

/**
* Constructor
*/
FontManager::FontManager(){}

/**
* Destructor
*/
FontManager::~FontManager()
{
	freeDatabase();
}

/**
* Check if a font exists in the database
* @param fontDescName Font description name
* @return The text result
*/
bool FontManager::fontExists(std::string fontDescName)
{
	bool ret = false;
	for(std::map<std::string, LPD3DXFONT>::iterator it = this->m_fonts.begin() ; it != this->m_fonts.end() ; it++)
		if(it->first == fontDescName)
			ret = true;
	return ret;
}

/**
* Initialize the manager
*/
void FontManager::initialize()
{
	this->m_deviceM = DeviceManager::getInstance();
	this->m_logM = LogManager::getInstance();
}

/**
* Free the manager database
*/
void FontManager::freeDatabase()
{
	for(std::map<std::string, LPD3DXFONT>::iterator it = this->m_fonts.begin() ; it != this->m_fonts.end() ; it++)
	{
		it->second->Release();
		it->second = NULL;
	}
	this->m_fonts.clear();
}

/**
* Add a font to the manager
* @param fontDescName The Font description name
* @param fontName The font name
* @param height The choosen font height
* @param width The choosen font width
* @param weight The choosen font weight
* @param italic If the font is set to italic
*/
void FontManager::addFont(std::string fontDescName, std::string fontName, int height, int width, int weight, bool italic)
{
	LPD3DXFONT font;
	if(!fontExists(fontDescName))
	{
		HRESULT res = D3DXCreateFont(this->m_deviceM->getDevice(), height, width, weight, 1, italic, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, ANTIALIASED_QUALITY, DEFAULT_PITCH|FF_DONTCARE, fontName.c_str(), &font);
		if(FAILED(res))
		{
			this->m_logM->getStream() << "Unable to load the font: " << fontDescName << "\n";
			this->m_logM->write();
		}
		else this->m_fonts[fontDescName] = font;
	}
}

/**
* Draw the font
* @param fontDescName The Font description name
* @param text The text to draw
* @param format The alignment
* @param posX The x starting coordinate
* @param posY The y starting coordinate
* @param endX The x ending coordinate
* @param endY The y ending coordinate
* @param color The color
*/
void FontManager::drawText(std::string fontDescName, std::string text, int format, int posX, int posY, int endX, int endY, D3DCOLOR color)
{
	if(fontExists(fontDescName))
	{
		RECT font_rect;
		DWORD dformat;
		switch(format)
		{
			case FONTMANAGER_ALIGN_LEFT:
			dformat = DT_LEFT|DT_NOCLIP;
			break;

			case FONTMANAGER_ALIGN_CENTER:
				dformat = DT_CENTER|DT_NOCLIP;
			break;

			case FONTMANAGER_ALIGN_RIGHT:
				dformat = DT_RIGHT|DT_NOCLIP;
			break;
		}
		SetRect(&font_rect,posX,posY,endX,endY);
		this->m_fonts[fontDescName]->DrawText(NULL, text.c_str(), strlen(text.c_str()), &font_rect, dformat, color);
	}
}
