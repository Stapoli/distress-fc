/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#ifndef __KEYBOARDMANAGER_H
#define __KEYBOARDMANAGER_H

enum keyboardKeyIndex
{
	KEYBOARD_KEY_INDEX_ASSIGNED,
	KEYBOARD_KEY_INDEX_STATE,
	KEYBOARD_KEY_INDEX_LINKED_KEY,
	KEYBOARD_KEY_INDEX_SIZE
};

enum keyboardKeyState
{
	KEYBOARD_KEY_STATE_UP,
	KEYBOARD_KEY_STATE_DOWN,
	KEYBOARD_KEY_STATE_DOWN_WAIT,
	KEYBOARD_KEY_STATE_FROZEN
};


#include "../managers/logManager.h"
#include "../managers/deviceManager.h"
#include "../managers/singletonManager.h"

/**
* KeyboardManager class
* Handle the keyboard events
*/
class KeyboardManager : public SingletonManager<KeyboardManager>
{
friend class SingletonManager<KeyboardManager>;

private:
	char m_keysBuffer[256];
	int ** m_keys;
	LPDIRECTINPUTDEVICEA m_keyboard;
	DeviceManager * m_deviceM;
	LogManager * m_logM;

public:
	~KeyboardManager();
	bool isKeyDown(int key);
	void initialize();
	void setKeyState(int key, int state);
	void refresh();
	void restore();
	void freezeKeys();
	void release();
	
private:
	KeyboardManager();
};

#endif
