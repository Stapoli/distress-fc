/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include "../managers/deviceManager.h"

/**
* Constructor
*/
DeviceManager::DeviceManager(){}

/**
* Destructor
*/
DeviceManager::~DeviceManager()
{
	if(this->m_input != NULL)
	{
		this->m_input->Release();
		this->m_input = NULL; 
	}

	if(this->m_zbuffer != NULL)
	{
		delete this->m_zbuffer;
		this->m_zbuffer = NULL;
	}

	if(this->m_device != NULL)
	{
		this->m_device->Release();
		this->m_device = NULL;
	}

	if(this->m_d3d != NULL)
	{
		this->m_d3d->Release();
		this->m_d3d = NULL;
	}
}

/**
* Get te device
* @return The device
*/
LPDIRECT3DDEVICE9 DeviceManager::getDevice()
{
	return this->m_device;
}

/**
* Get the input device
* @return The input device
*/
LPDIRECTINPUT DeviceManager::getInput()
{
	return this->m_input;
}

/**
* Get the window handle
* @return The window handle
*/
HWND DeviceManager::getHwnd()
{
	return this->m_hwnd;
}

/**
* Initialize the manager
* @param hInst Instance
* @param hWnd Window
*/
void DeviceManager::initialize(HINSTANCE hInst, HWND hWnd)
{
	this->m_optM = OptionManager::getInstance();
	this->m_logM = LogManager::getInstance();
	this->m_hwnd = hWnd;

	this->m_d3d = Direct3DCreate9(D3D_SDK_VERSION);
    ZeroMemory(&this->m_d3dpp, sizeof(this->m_d3dpp));

	if(this->m_optM->isFullScreen())
		this->m_d3dpp.Windowed = FALSE;
	else
		this->m_d3dpp.Windowed = TRUE;

    this->m_d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
    this->m_d3dpp.hDeviceWindow = this->m_hwnd;
    this->m_d3dpp.BackBufferFormat = D3DFMT_X8R8G8B8;
	this->m_d3dpp.BackBufferWidth = this->m_optM->getWidth();
    this->m_d3dpp.BackBufferHeight = this->m_optM->getHeight();
    this->m_d3dpp.EnableAutoDepthStencil = TRUE;	
    this->m_d3dpp.AutoDepthStencilFormat = D3DFMT_D16;

    this->m_d3d->CreateDevice(D3DADAPTER_DEFAULT,D3DDEVTYPE_HAL, this->m_hwnd,D3DCREATE_HARDWARE_VERTEXPROCESSING,&this->m_d3dpp, &this->m_device);

	this->m_device->CreateDepthStencilSurface(this->m_optM->getWidth(), this->m_optM->getHeight(), D3DFMT_D16, D3DMULTISAMPLE_NONE, 0, TRUE, &this->m_zbuffer, NULL);

    this->m_device->SetRenderState(D3DRS_LIGHTING, FALSE);
    this->m_device->SetRenderState(D3DRS_ZENABLE, TRUE);
    this->m_device->SetRenderState(D3DRS_AMBIENT, D3DCOLOR_XRGB(50, 50, 50));

    this->m_device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
    this->m_device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
    this->m_device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
    this->m_device->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);

	//Init inputs
	DirectInput8Create(hInst,DIRECTINPUT_VERSION,IID_IDirectInput8,(void**)&m_input,NULL);
}

/**
* Reset the device
*/
void DeviceManager::reset()
{
	HRESULT res = this->m_device->Reset(&this->m_d3dpp);
	if(FAILED(res))
	{
		this->m_logM->getStream() << "Unable to reset the device\n";
		this->m_logM->write();
	}
	else
	{
		this->m_logM->getStream() << "Reset device => OK\n";
		this->m_logM->write();
	}
}
