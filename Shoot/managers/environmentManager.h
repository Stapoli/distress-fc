/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#ifndef __ENVIRONMENTMANAGER_H
#define __ENVIRONMENTMANAGER_H

#include "../managers/cameraManager.h"
#include "../managers/soundManager.h"
#include "../managers/lightManager.h"
#include "../managers/logManager.h"
#include "../managers/singletonManager.h"

#define ENVIRONMENT_EXPLOSION_TIME 6000.0f
#define ENVIRONMENT_EXPLOSION_DELAY_MIN 12000.0f
#define ENVIRONMENT_EXPLOSION_DELAY_MAX 22000.0f
#define ENVIRONMENT_EXPLOSION_LIGHT_MODIFIER 5
#define ENVIRONMENT_EXPLOSION_LIGHT_Y 20
#define ENVIRONMENT_EXPLOSION_LIGHT_RADIUS 50

enum environmentExplosionLocation
{
	ENVIRONMENT_EXPLOSION_LEFT_TOP,
	ENVIRONMENT_EXPLOSION_LEFT_BOTTOM,
	ENVIRONMENT_EXPLOSION_RIGHT_TOP,
	ENVIRONMENT_EXPLOSION_RIGHT_BOTTOM,
	ENVIRONMENT_EXPLOSION_SIZE
};

/**
* EnvironmentManager class
* Handle the environment events
*/
class EnvironmentManager : public SingletonManager<EnvironmentManager>
{
friend class SingletonManager<EnvironmentManager>;

private:
	int m_lightId;
	int m_lightLocation;
	bool m_explosionActivated;
	float m_explosionLightTimer;
	float m_explosionTimer;
	float m_explosionNextDelay;
	ALuint m_explosionSound1;
	ALuint m_explosionSound2;
	LightManager * m_lightM;

public:
	~EnvironmentManager();
	void initialize();
	void reset();
	void refresh(float time);

private:
	EnvironmentManager();
};

#endif
