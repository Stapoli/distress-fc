/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include "../managers/mouseManager.h"

/**
* Constructor
*/
MouseManager::MouseManager(){}

/**
* Destructor
*/
MouseManager::~MouseManager()
{
	for(int i = 0 ; i < MOUSE_BUTTON_SIZE ; i++)
	{
		delete this->m_buttons[i];
		this->m_buttons[i] = NULL;
	}
	delete this->m_buttons;
	this->m_buttons = NULL;

	release();
}

/**
* Get the x coordinate
* @return The x coordinate
*/
int MouseManager::getX()
{
	return this->m_pos[0];
}

/**
* Get the y coordinate
* @return The y coordinate
*/
int MouseManager::getY()
{
	return this->m_pos[1];
}

/**
* Test if a button is down
* @param button The button number
* @return The test result
*/
bool MouseManager::isButtonDown(int button)
{
	return this->m_buttons[button][MOUSE_BUTTON_INDEX_STATE] == MOUSE_BUTTON_STATE_DOWN;
}

/**
* Test if the mouse has moved since its previous known coordinate
* @return The test result
*/
bool MouseManager::hasMoved()
{
	return this->m_hasMoved;
}

/**
* Initialize the manager
*/
void MouseManager::initialize()
{
	HRESULT result;
	this->m_deviceM = DeviceManager::getInstance();
	this->m_optionM = OptionManager::getInstance();
	this->m_logM = LogManager::getInstance();

	result = this->m_deviceM->getInput()->CreateDevice(GUID_SysMouse, &this->m_mouse, NULL);
	if(FAILED(result))
		this->m_logM->getStream() << "Unable to create the mouse device\n";

	result = this->m_mouse->SetDataFormat(&c_dfDIMouse);
	if(FAILED(result))
		this->m_logM->getStream() << "Unable set the data format for the mouse\n";

	this->m_mouse->SetCooperativeLevel(this->m_deviceM->getHwnd(), DISCL_FOREGROUND | DISCL_EXCLUSIVE );
	if(FAILED(result))
		this->m_logM->getStream() << "Unable to set the cooperation level for the mouse\n";

	this->m_mouse->Acquire();
	if(FAILED(result))
		this->m_logM->getStream() << "Unable to acquire the mouse\n";

	this->m_pos[0] = this->m_optionM->getWidth() / 2;
	this->m_pos[1] = this->m_optionM->getHeight() / 2;

	this->m_buttons = new int*[MOUSE_BUTTON_SIZE];
	for(int i = 0 ; i < MOUSE_BUTTON_INDEX_SIZE ; i++)
		this->m_buttons[i] = new int[MOUSE_BUTTON_INDEX_SIZE];

	//Button assignment
	this->m_buttons[MOUSE_BUTTON_ENTER_MENU][MOUSE_BUTTON_INDEX_ASSIGNED] = GetPrivateProfileInt("mouse", "enter_menu", 0, INI_FILENAME);
	this->m_buttons[MOUSE_BUTTON_RETURN_MENU][MOUSE_BUTTON_INDEX_ASSIGNED] = GetPrivateProfileInt("mouse", "escape_menu", 1, INI_FILENAME);

	//Button to UP state
	this->m_buttons[MOUSE_BUTTON_ENTER_MENU][MOUSE_BUTTON_INDEX_STATE] = MOUSE_BUTTON_STATE_UP;
	this->m_buttons[MOUSE_BUTTON_RETURN_MENU][MOUSE_BUTTON_INDEX_STATE] = MOUSE_BUTTON_STATE_UP;

	this->m_cursor = new Sprite(CURSOR_FILE,D3DXVECTOR2(this->m_optionM->getWidth() / 2.0f, this->m_optionM->getHeight() / 2.0f),D3DXVECTOR2(0.0f,0.0f));

	this->m_logM->write();
}

/**
* Refresh the mouse and the cursor
*/
void MouseManager::refresh()
{
	HRESULT res;
	this->m_hasMoved = false;
	res = this->m_mouse->GetDeviceState(sizeof(this->m_state),(LPVOID)&this->m_state);
	if(FAILED(res))
	{
		res = this->m_mouse->Acquire();

		while(res == DIERR_INPUTLOST)    
			 res = this->m_mouse->Acquire();

		if (SUCCEEDED(res))
		{
			SetCursorPos(0,0);
			this->m_mouse->GetDeviceState(sizeof(this->m_state), (LPVOID)&this->m_state);
		}
	}
	else
		SetCursorPos(0,0);

	this->m_pos[0] += (int)this->m_state.lX;
	this->m_pos[1] += (int)this->m_state.lY;

	if(this->m_state.lX != 0 || this->m_state.lY != 0)
		this->m_hasMoved = true;

	if(this->m_pos[0] < 0)
		this->m_pos[0] = 0;
	if(this->m_pos[1] < 0)
		this->m_pos[1] = 0;

	if(this->m_pos[0] > this->m_optionM->getWidth())
		this->m_pos[0] = this->m_optionM->getWidth();
	if(this->m_pos[1] > this->m_optionM->getHeight())
		this->m_pos[1] = this->m_optionM->getHeight();

	this->m_cursor->setPosition(D3DXVECTOR2((float)this->m_pos[0], (float)this->m_pos[1]));
	
	for(int i = 0 ; i < MOUSE_BUTTON_SIZE ; i++)
	{
		if(KEYDOWN(this->m_state.rgbButtons,this->m_buttons[i][MOUSE_BUTTON_INDEX_ASSIGNED]))
		{
			if(this->m_buttons[i][MOUSE_BUTTON_INDEX_STATE] == MOUSE_BUTTON_STATE_UP)
				this->m_buttons[i][MOUSE_BUTTON_INDEX_STATE] = MOUSE_BUTTON_STATE_DOWN;
		}
		else
		{
			if(this->m_buttons[i][MOUSE_BUTTON_INDEX_STATE] == MOUSE_BUTTON_STATE_DOWN || this->m_buttons[i][MOUSE_BUTTON_INDEX_STATE] == MOUSE_BUTTON_STATE_FROZEN)
				this->m_buttons[i][MOUSE_BUTTON_INDEX_STATE] = MOUSE_BUTTON_STATE_UP;
		}
	}
}

/**
* Restore the mouse device
*/
void MouseManager::restore()
{
	for(int i = 0 ; i < MOUSE_BUTTON_SIZE ; i++)
		this->m_buttons[i][MOUSE_BUTTON_INDEX_STATE] = MOUSE_BUTTON_STATE_UP;
}

/**
* Release the mouse device
*/
void MouseManager::release()
{
	if(this->m_mouse) 
	{ 
		this->m_mouse->Unacquire(); 
		this->m_mouse->Release();
		this->m_mouse = NULL; 
	}
}

/**
* Draw the cursor
*/
void MouseManager::showCursor()
{
	this->m_cursor->draw();
}

/**
* Freeze the mouse buttons
*/
void MouseManager::freezeKeys()
{
	for(int i = 0 ; i < MOUSE_BUTTON_SIZE ; i++)
		this->m_buttons[i][MOUSE_BUTTON_INDEX_STATE] = MOUSE_BUTTON_STATE_FROZEN;
}

/**
* Release the ressources
*/
void MouseManager::releaseResources()
{
	this->m_cursor->getSprite()->OnLostDevice();
}

/**
* Restore the ressources
*/
void MouseManager::restoreResources()
{
	this->m_cursor->getSprite()->OnResetDevice();
	this->m_pos[0] = this->m_optionM->getWidth() / 2;
	this->m_pos[1] = this->m_optionM->getHeight() / 2;
}