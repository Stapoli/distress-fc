/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#ifndef __GAMEMANAGER_H
#define __GAMEMANAGER_H

#include "../managers/deviceManager.h"
#include "../managers/mouseManager.h"
#include "../managers/keyboardManager.h"
#include "../managers/joypadManager.h"
#include "../managers/controlManager.h"
#include "../managers/projectileManager.h"
#include "../managers/lightManager.h"
#include "../managers/logManager.h"
#include "../managers/cameraManager.h"
#include "../managers/enemyManager.h"
#include "../managers/bonusManager.h"
#include "../managers/interfaceManager.h"
#include "../managers/particleManager.h"
#include "../managers/messageManager.h"
#include "../managers/soundManager.h"
#include "../managers/environmentManager.h"
#include "../managers/singletonManager.h"
#include "../menus/mainMenu.h"
#include "../menus/gameMenu.h"
#include "../menus/gameOverMenu.h"
#include "../elements/2D/border.h"
#include "../elements/3D/player.h"
#include "../elements/3D/building.h"
#include "../elements/3D/enemies/slider1.h"
#include "../elements/3D/enemies/slider2.h"
#include "../elements/3D/enemies/slider3.h"
#include "../elements/3D/enemies/slider4.h"
#include "../elements/3D/enemies/slider5.h"
#include "../elements/3D/enemies/slider6.h"
#include "../elements/3D/enemies/cycler1.h"
#include "../elements/3D/enemies/cycler2.h"
#include "../elements/3D/enemies/cycler3.h"

#define LEVEL_WIDTH 5
#define LEVEL_INTRO_SIZE 10
#define LEVEL_SPEED 1.5f
#define LEVEL_SPEED_INTRO 60.0f
#define LEVEL_ELEMENTS_Y 40.0f
#define LEVEL_BUILDING_X_DECAL -8.0f
#define LEVEL_BUILDING_Z_DECAL 12.0f
#define LEVEL_ENEMY_START_DELAY 6000
#define ENEMY_START_Z 20.0f
#define GAMEOVER_DELAY 4000

enum gameState
{
	GAME_STATE_2D,
	GAME_STATE_3D,
	GAME_STATE_EXIT
};

/**
* GameManager class
* Handle the game state
*/
class GameManager : public SingletonManager<GameManager>
{
friend class SingletonManager<GameManager>;

private:
	bool m_enemyStarted;
	int m_gameState;
	int m_levelHeight;
	float m_enemyStartTimer;
	float m_levelMovement;
	double m_gameOverTimer;
	Player * m_player;
	Building *** m_introBuildings;
	Building *** m_buildings;
	Menu * m_menu;
	Border * m_leftBorder;
	Border * m_rightBorder;
	DeviceManager * m_deviceM;
	MouseManager * m_mouseM;
	ProjectileManager * m_projectileM;
	LightManager * m_lightM;
	KeyboardManager * m_keyM;
	JoypadManager * m_joypadM;
	ControlManager * m_controlM;
	EnemyManager * m_enemyM;
	BonusManager * m_bonusM;
	MessageManager * m_messageM;
	InterfaceManager * m_interfaceM;
	ParticleManager * m_particleM;
	SoundManager * m_soundM;
	EnvironmentManager * m_environmentM;

public:
	~GameManager();
	int getGameState();
	void initialize();
	void refresh(float time);
	void draw();
	void loadLevel(int number);
	void unloadLevel();

private:
	GameManager();
};

#endif
