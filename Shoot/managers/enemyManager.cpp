/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include "../managers/enemyManager.h"

/**
* Constructor
*/
EnemyManager::EnemyManager(){}

/**
* Destructor
*/
EnemyManager::~EnemyManager()
{
	freeDatabase();
}

/**
* Test a collision beteween the enemies and a projectile
* @param position The projectile position
* @param additionalRadius The projectile radius
* @param power The projectile power
* @return The test result
*/
bool EnemyManager::collisionTest(D3DXVECTOR2 position, float additionalRadius, int power)
{
	bool ret = false;
	for(unsigned int i = 0 ; i < this->m_buffedEnemies.size() && !ret ; i++)
	{
		if(this->m_buffedEnemies[i]->isCollision(position,additionalRadius))
		{
			this->m_buffedEnemies[i]->takeDamage(power);
			ret = true;
		}
	}
	return ret;
}

/**
* Add an enemy
* @param enemy An enemy pointer
*/
void EnemyManager::addEnemy(Enemy * enemy)
{
	this->m_standbyEnemies.push_back(enemy);
}

/**
* Refresh the enemies
* @param time Elapsed time
*/
void EnemyManager::refresh(float time)
{
	//Update buffered enemies and move killed enemies to the killed vector
	for(unsigned int i = 0 ; i < this->m_buffedEnemies.size() ; i++)
	{
		this->m_buffedEnemies[i]->refresh(time);
		if(this->m_buffedEnemies[i]->getState() == ENEMY_STATE_DEAD)
			this->m_killedEnemies.push_back(this->m_buffedEnemies[i]);
	}

	//Erase moved enemies from the buffered vector
	for(unsigned int i = 0 ; i < this->m_buffedEnemies.size() ; i++)
	{
		if(this->m_buffedEnemies[i]->getState() == ENEMY_STATE_DEAD)
		{
			m_buffedEnemies.erase(this->m_buffedEnemies.begin() + i);
			i--;
		}
	}

	//If the current enemy wave is over, load the next one if available
	if(this->m_buffedEnemies.size() == 0 && this->m_standbyEnemies.size() > 0)
	{
		int currentWaveId = this->m_standbyEnemies[0]->getWaveId();
		for(unsigned int i = 0 ; i < this->m_standbyEnemies.size() ; i++)
		{
			if(this->m_standbyEnemies[i]->getWaveId() == currentWaveId)
			{
				this->m_buffedEnemies.push_back(this->m_standbyEnemies[i]);
				this->m_standbyEnemies.erase(this->m_standbyEnemies.begin() + i);
				i--;
			}
		}
	}
}

/**
* Draw the enemies
*/
void EnemyManager::draw()
{
	for(unsigned int i = 0 ; i < this->m_buffedEnemies.size() ; i++)
		this->m_buffedEnemies[i]->draw();
}

/**
* Free the manager database
*/
void EnemyManager::freeDatabase()
{
	for(unsigned int i = 0 ; i < this->m_standbyEnemies.size() ; i++)
		delete this->m_standbyEnemies[i];
	this->m_standbyEnemies.clear();

	for(unsigned int i = 0 ; i < this->m_buffedEnemies.size() ; i++)
		delete this->m_buffedEnemies[i];
	this->m_buffedEnemies.clear();

	for(unsigned int i = 0 ; i < this->m_killedEnemies.size() ; i++)
		delete this->m_killedEnemies[i];
	this->m_killedEnemies.clear();
}