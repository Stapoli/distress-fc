/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include "../managers/controlManager.h"

/**
* Constructor
*/
ControlManager::ControlManager(){}

/**
* Destructor
*/
ControlManager::~ControlManager(){}

/**
* Check if a key is down
* @param key The key number
* @return The text result
*/
bool ControlManager::isKeyDown(int key)
{
	return this->m_keyM->isKeyDown(key) || this->m_joypadM->isButtonDown(key);
}

/**
* Change the key state
* @param key The key number
* @param state The key state
*/
void ControlManager::setKey(int key, int state)
{
	this->m_keyM->setKeyState(key, state);
	if(this->m_joypadM->isJoypadDetected())
		this->m_joypadM->setButtonState(key, state);
}

/**
* Allow the use of the controls
*/
void ControlManager::restore()
{
	this->m_keyM->restore();
	if(this->m_joypadM->isJoypadDetected())
		this->m_joypadM->restore();
}

/**
* Freeze the controls
*/
void ControlManager::freezeInputs()
{
	this->m_keyM->freezeKeys();
	if(this->m_joypadM->isJoypadDetected())
		this->m_joypadM->freezeButtons();
}

/**
* Initialize the manager
*/
void ControlManager::initialize()
{
	this->m_keyM = KeyboardManager::getInstance();
	this->m_joypadM = JoypadManager::getInstance();
}
