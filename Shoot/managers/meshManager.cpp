/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include "../managers/meshManager.h"

/**
* Constructor
*/
MeshManager::MeshManager(){}

/**
* Destructor
*/
MeshManager::~MeshManager()
{
	freeDatabase();
}

/**
* Load and return a X mesh
* If the mesh already exists in the mesh database, return the mesh
* @param filename The mesh filename
* @return The mesh
*/
LPD3DXMESH MeshManager::loadMesh(std::string filename)
{
	if(!meshExists(filename))
	{
		LPD3DXBUFFER bufMaterial;

		D3DXLoadMeshFromX(filename.c_str(),D3DXMESH_SYSTEMMEM,this->m_deviceM->getDevice(),NULL,&bufMaterial,NULL,&this->m_numMaterialDatabase[filename],&this->m_meshDatabase[filename]);
		D3DXMATERIAL * tmpMat = (D3DXMATERIAL *)bufMaterial->GetBufferPointer();

		this->m_materialDatabase[filename] = new D3DMATERIAL9[this->m_numMaterialDatabase[filename]];
		this->m_textureDatabase[filename] = new LPDIRECT3DTEXTURE9[this->m_numMaterialDatabase[filename]];

		for(DWORD i = 0 ; i < this->m_numMaterialDatabase[filename] ; i++)
		{
			this->m_materialDatabase[filename][i] = tmpMat[i].MatD3D;
			this->m_materialDatabase[filename][i].Ambient = tmpMat[i].MatD3D.Diffuse;
			this->m_textureDatabase[filename][i] = NULL;
			D3DXCreateTextureFromFileEx(this->m_deviceM->getDevice(),tmpMat[i].pTextureFilename,D3DX_DEFAULT,D3DX_DEFAULT,D3DX_DEFAULT,D3DUSAGE_RENDERTARGET,D3DFMT_UNKNOWN,D3DPOOL_DEFAULT,D3DX_FILTER_TRIANGLE | D3DX_FILTER_DITHER,D3DX_FILTER_BOX,0xFFFF00FF,NULL,NULL,&this->m_textureDatabase[filename][i]);	

			//Adjacency optimization
			DWORD * adjacency = new DWORD[this->m_meshDatabase[filename]->GetNumFaces() * 3];
			this->m_meshDatabase[filename]->GenerateAdjacency(0.01f,adjacency);
			this->m_meshDatabase[filename]->OptimizeInplace(D3DXMESHOPT_VERTEXCACHE,adjacency,NULL,NULL,NULL);
		}
	}
	return this->m_meshDatabase[filename];
}

/**
* Test if a mesh exists in the mesh database
* @param filename The mesh filename
* @return True if the mesh is found
*/
bool MeshManager::meshExists(std::string filename)
{
	bool ret = false;
	for(std::map<std::string, LPD3DXMESH>::iterator it = this->m_meshDatabase.begin() ; it != this->m_meshDatabase.end() ; it++)
		if(it->first == filename)
			ret = true;
	return ret;
}

/**
* Draw a mesh
* @param filename The mesh to draw
*/
void MeshManager::draw(std::string filename)
{
	if(meshExists(filename))
	{
		for(DWORD i = 0 ; i < this->m_numMaterialDatabase[filename] ; i++)
		{
			this->m_deviceM->getDevice()->SetMaterial(&this->m_materialDatabase[filename][i]);
			if(this->m_textureDatabase[filename] != NULL)
			{
				this->m_deviceM->getDevice()->SetTexture(0,this->m_textureDatabase[filename][i]);
				this->m_deviceM->getDevice()->SetSamplerState(0,D3DSAMP_MIPFILTER,D3DTEXF_LINEAR);
			}
			this->m_meshDatabase[filename]->DrawSubset(i);
		}
	}
}

/**
* Initialize the manager
*/
void MeshManager::initialize()
{
	this->m_deviceM = DeviceManager::getInstance();
}

/**
* Free the manager database
*/
void MeshManager::freeDatabase()
{
	for(std::map<std::string, LPD3DXMESH>::iterator it = this->m_meshDatabase.begin() ; it != this->m_meshDatabase.end() ; it++)
	{
		it->second->Release();
		it->second = NULL;
	}
	this->m_meshDatabase.clear();

	for(std::map<std::string, LPDIRECT3DTEXTURE9 *>::iterator it = this->m_textureDatabase.begin() ; it != this->m_textureDatabase.end() ; it++)
	{
		for(DWORD i = 0 ; i < this->m_numMaterialDatabase[it->first] ; i++)
			it->second[i]->Release();
		it->second = NULL;
	}
	this->m_textureDatabase.clear();
}
