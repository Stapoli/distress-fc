/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include "../managers/gameManager.h"

/**
* Constructor
*/
GameManager::GameManager(){}

/**
* Destructor
*/
GameManager::~GameManager(){}

/**
* Return the game state
* @return The game state
*/
int GameManager::getGameState()
{
	return this->m_gameState;
}

/**
* Initialize the manager
*/
void GameManager::initialize()
{
	this->m_deviceM = DeviceManager::getInstance();
	this->m_mouseM = MouseManager::getInstance();
	this->m_projectileM = ProjectileManager::getInstance();
	this->m_lightM = LightManager::getInstance();
	this->m_keyM = KeyboardManager::getInstance();
	this->m_joypadM = JoypadManager::getInstance();
	this->m_controlM = ControlManager::getInstance();
	this->m_enemyM = EnemyManager::getInstance();
	this->m_bonusM = BonusManager::getInstance();
	this->m_interfaceM = InterfaceManager::getInstance();
	this->m_particleM = ParticleManager::getInstance();
	this->m_messageM = MessageManager::getInstance();
	this->m_soundM = SoundManager::getInstance();
	this->m_environmentM = EnvironmentManager::getInstance();
	this->m_gameState = GAME_STATE_2D;
	this->m_gameOverTimer = 0;
	this->m_menu = new MainMenu();
	this->m_soundM->loadMusic("sounds/intro.wav");
}

/**
* Refresh the game
* @param time Elapsed time
*/
void GameManager::refresh(float time)
{
	int internalState;
	this->m_mouseM->refresh();
	this->m_keyM->refresh();
	if(this->m_joypadM->isJoypadDetected())
		this->m_joypadM->refresh();

	this->m_soundM->refresh();

	switch(this->m_gameState)
	{
	case GAME_STATE_2D:
		this->m_menu->refresh(time);
		internalState = this->m_menu->getSelectedMenu();
		if(internalState != this->m_menu->getCodeMenu())
		{
			switch(internalState)
			{
			case ID_EXITGAME:
				this->m_gameState = GAME_STATE_EXIT;
				break;

			case ID_PLAYGAME:
				delete this->m_menu;
				this->m_gameState = GAME_STATE_3D;
				this->m_menu = NULL;
				this->m_controlM->freezeInputs();
				this->m_mouseM->freezeKeys();
				loadLevel(1);
				this->m_soundM->playMusic();
				this->m_environmentM->reset();
				break;
			}
		}
		break;

	case GAME_STATE_3D:
		if(this->m_menu != NULL)
		{
			this->m_menu->refresh(time);
			internalState = this->m_menu->getSelectedMenu();
			if(internalState != this->m_menu->getCodeMenu())
			{
				switch(internalState)
				{
				case ID_RESUMEGAME:
					delete this->m_menu;
					this->m_menu = NULL;
					this->m_controlM->freezeInputs();
					this->m_mouseM->freezeKeys();
					this->m_soundM->resumeSounds();
					break;

				case ID_MAINMENU:
					delete this->m_menu;
					this->m_gameState = GAME_STATE_2D;
					this->m_menu = new MainMenu();
					this->m_controlM->freezeInputs();
					this->m_mouseM->freezeKeys();
					unloadLevel();
					break;

				case ID_RETRYGAME:
					delete this->m_menu;
					this->m_menu = NULL;
					this->m_controlM->freezeInputs();
					this->m_mouseM->freezeKeys();
					unloadLevel();
					loadLevel(1);
					this->m_soundM->playMusic();
					this->m_environmentM->reset();
					break;
				}
			}
		}
		else
		{
			if(this->m_controlM->isKeyDown(ACTION_KEY_ESCAPE_MENU) && this->m_gameOverTimer == 0)
			{
				this->m_menu = new GameMenu();
				this->m_controlM->freezeInputs();
				this->m_mouseM->freezeKeys();
				this->m_soundM->pauseSounds();
			}
			else
			{
				if(!this->m_enemyStarted)
				{
					this->m_levelMovement = LEVEL_SPEED_INTRO * time / 1000.0f;
					this->m_enemyStartTimer += time;
					if(this->m_enemyStartTimer >= LEVEL_ENEMY_START_DELAY)
					{
						this->m_enemyStarted = true;
						this->m_levelMovement = 0;
					}

					for(int i = 0 ; i < LEVEL_INTRO_SIZE ; i++)
					{
						for(int j = 0 ; j < LEVEL_WIDTH ; j++)
							if(this->m_introBuildings[i][j] != NULL)
								this->m_introBuildings[i][j]->refresh(time, this->m_levelMovement);
					}
				}
				else
				{
					this->m_levelMovement = LEVEL_SPEED * time / 1000.0f;
					for(int i = 0 ; i < this->m_levelHeight ; i++)
					{
						for(int j = 0 ; j < LEVEL_WIDTH ; j++)
							if(this->m_buildings[i][j] != NULL)
								this->m_buildings[i][j]->refresh(time, this->m_levelMovement);
					}
				}

				this->m_player->refresh(time);
				this->m_projectileM->refresh(time);
				if(this->m_enemyStarted)
				{
					this->m_enemyM->refresh(time);
					this->m_environmentM->refresh(time);
				}
				this->m_particleM->refresh(time);
				this->m_messageM->refresh(time);
				this->m_bonusM->refresh(time);

				for(int i = 0 ; i < MAX_PROJECTILES ; i++)
				{
					//Projectile - Enemies detection
					if(this->m_projectileM->getProjectile(i)->getSource() == PROJECTILE_SOURCE_PLAYER)
					{
						if(this->m_projectileM->getProjectile(i)->isAlive() && this->m_enemyM->collisionTest(this->m_projectileM->getProjectile(i)->getScreenPosition(),this->m_projectileM->getProjectile(i)->getRadius(),this->m_projectileM->getProjectile(i)->getPower()))
						{
							this->m_projectileM->getProjectile(i)->setAlive(false);
							this->m_particleM->addParticle(PARTICLE_SPARK,this->m_projectileM->getProjectile(i)->getWorldPosition(),this->m_projectileM->getProjectile(i)->getOrientation() + 180.0f,10);
						}
					}
					else
					{
						//Projectile - Player detection
						if(this->m_projectileM->getProjectile(i)->isAlive() && this->m_player->isAlive() && this->m_player->isCollision(this->m_projectileM->getProjectile(i)->getScreenPosition(),this->m_projectileM->getProjectile(i)->getRadius()))
							this->m_projectileM->getProjectile(i)->setAlive(false);
					}
				}

				for(int i = 0 ; i < MAX_BONUS ; i++)
				{
					if(this->m_bonusM->getBonus(i)->isAlive() && this->m_player->isAlive() && this->m_player->isCollisionBonus(this->m_bonusM->getBonus(i)->getScreenPosition(), this->m_bonusM->getBonus(i)->getRadius(), this->m_bonusM->getBonus(i)->getType()))
						this->m_bonusM->getBonus(i)->setAlive(false);
				}

				if(this->m_player->isGameOver())
				{
					this->m_gameOverTimer += time;
					if(this->m_gameOverTimer >= GAMEOVER_DELAY)
					{
						this->m_soundM->stopSounds();
						this->m_menu = new GameOverMenu();
					}
				}
			}
		}
		break;
	}
}

/**
* Draw the game
*/
void GameManager::draw()
{
	switch(this->m_gameState)
	{
	case GAME_STATE_2D:
		this->m_menu->draw();
		this->m_mouseM->showCursor();
		break;

	case GAME_STATE_3D:
		if(this->m_enemyStarted)
		{
			for(int i = 0 ; i < this->m_levelHeight ; i++)
			{
				for(int j = 0 ; j < LEVEL_WIDTH ; j++)
					if(this->m_buildings[i][j] != NULL && this->m_buildings[i][j]->getPosition().z <= WINDOW_BORDER_Z_FAR + BUILDING_WIDTH / 2 + BUILDING_BORDER_Z_MODIFIER_FAR && this->m_buildings[i][j]->getPosition().z >= WINDOW_BORDER_Z_NEAR - BUILDING_WIDTH / 2 - BUILDING_BORDER_Z_MODIFIER_NEAR)
						this->m_buildings[i][j]->draw();
			}
		}
		else
		{
			for(int i = 0 ; i < LEVEL_INTRO_SIZE ; i++)
			{
				for(int j = 0 ; j < LEVEL_WIDTH ; j++)
					if(this->m_introBuildings[i][j] != NULL && this->m_introBuildings[i][j]->getPosition().z <= WINDOW_BORDER_Z_FAR + BUILDING_WIDTH / 2 + BUILDING_BORDER_Z_MODIFIER_FAR && this->m_introBuildings[i][j]->getPosition().z >= WINDOW_BORDER_Z_NEAR - BUILDING_WIDTH / 2 - BUILDING_BORDER_Z_MODIFIER_NEAR)
						this->m_introBuildings[i][j]->draw();
			}
		}
		
		if(this->m_enemyStarted)
			this->m_enemyM->draw();
		this->m_bonusM->draw();
		this->m_player->draw();
		this->m_projectileM->draw();
		this->m_particleM->draw();
		this->m_messageM->draw();
		this->m_leftBorder->draw();
		this->m_rightBorder->draw();
		//this->m_interfaceM->draw();
		if(this->m_menu != NULL)
		{
			this->m_menu->draw();
			this->m_mouseM->showCursor();
		}
		break;
	}
}

/**
* Load a level
* @param number The level number
*/
void GameManager::loadLevel(int number)
{
	int lineNumber = 1;
	int tmp;
	// Open level file
	std::ostringstream stream;
	stream << "levels/" << number << ".dat";

	std::ifstream file;

	file.open(stream.str().c_str(), std::ios::in);

	// Read file
	if(file)
	{
		int cmpt = 0;
		int enemyId, enemyOrientation, enemyX, enemyWaveId, enemyDrop, enemySleep;

		file >> this->m_levelHeight;
		this->m_buildings = new Building**[this->m_levelHeight];
		for(int i = 0 ; i < this->m_levelHeight ; i++)
		{
			this->m_buildings[i] = new Building*[LEVEL_WIDTH];
			for(int j = 0 ; j < LEVEL_WIDTH ; j++)
				this->m_buildings[i][j] = NULL;
		}

		// Level creation
		while( cmpt < (this->m_levelHeight * LEVEL_WIDTH))
		{
			file >> tmp;
			if(tmp > 0)
				this->m_buildings[cmpt / LEVEL_WIDTH][cmpt % LEVEL_WIDTH] = new Building((cmpt % LEVEL_WIDTH) * (BUILDING_WIDTH + 1 / 2) + LEVEL_BUILDING_X_DECAL, (cmpt / LEVEL_WIDTH) * (BUILDING_WIDTH + 1 / 2) + LEVEL_BUILDING_Z_DECAL ,tmp / 10, tmp - ((tmp / 10) * 10));
			cmpt++;
		}

		//Enemy allocation
		while(!file.eof())
		{
			file >> enemyId >> enemyOrientation >> enemyX  >> enemyWaveId >> enemyDrop >> enemySleep;

			switch(enemyId)
			{
			case 1:
				this->m_enemyM->addEnemy(new Slider1(D3DXVECTOR3((float)enemyX,LEVEL_ELEMENTS_Y,ENEMY_START_Z),enemyOrientation,enemySleep,enemyWaveId,enemyDrop));
				break;

			case 2:
				this->m_enemyM->addEnemy(new Slider2(D3DXVECTOR3((float)enemyX,LEVEL_ELEMENTS_Y,ENEMY_START_Z),enemyOrientation,enemySleep,enemyWaveId,enemyDrop));
				break;

			case 3:
				this->m_enemyM->addEnemy(new Slider3(D3DXVECTOR3((float)enemyX,LEVEL_ELEMENTS_Y,ENEMY_START_Z),enemyOrientation,enemySleep,enemyWaveId,enemyDrop));
				break;

			case 4:
				this->m_enemyM->addEnemy(new Slider4(D3DXVECTOR3((float)enemyX,LEVEL_ELEMENTS_Y,ENEMY_START_Z),enemyOrientation,enemySleep,enemyWaveId,enemyDrop));
				break;

			case 5:
				this->m_enemyM->addEnemy(new Slider5(D3DXVECTOR3((float)enemyX,LEVEL_ELEMENTS_Y,ENEMY_START_Z),enemyOrientation,enemySleep,enemyWaveId,enemyDrop));
				break;

			case 6:
				this->m_enemyM->addEnemy(new Slider6(D3DXVECTOR3((float)enemyX,LEVEL_ELEMENTS_Y,ENEMY_START_Z),enemyOrientation,enemySleep,enemyWaveId,enemyDrop));
				break;

			case 7:
				this->m_enemyM->addEnemy(new Cycler1(D3DXVECTOR3((float)enemyX,LEVEL_ELEMENTS_Y,ENEMY_START_Z),enemyOrientation,enemySleep,enemyWaveId,enemyDrop));
				break;

			case 8:
				this->m_enemyM->addEnemy(new Cycler2(D3DXVECTOR3((float)enemyX,LEVEL_ELEMENTS_Y,ENEMY_START_Z),enemyOrientation,enemySleep,enemyWaveId,enemyDrop));
				break;

			case 9:
				this->m_enemyM->addEnemy(new Cycler3(D3DXVECTOR3((float)enemyX,LEVEL_ELEMENTS_Y,ENEMY_START_Z),enemyOrientation,enemySleep,enemyWaveId,enemyDrop));
				break;
			}
			enemyId = -1;
		}
		file.close();
	}

	//Intro level creation
	this->m_introBuildings = new Building**[LEVEL_INTRO_SIZE];
	for(int i = 0 ; i < LEVEL_INTRO_SIZE ; i++)
	{
		this->m_introBuildings[i] = new Building*[LEVEL_WIDTH];
		for(int j = 0 ; j < LEVEL_WIDTH ; j++)
			this->m_introBuildings[i][j] = NULL;
	}

	for(int i = 0 ; i < LEVEL_INTRO_SIZE ; i++)
	{
		for(int j = 0 ; j < LEVEL_WIDTH ; j++)
		{
			tmp = (rand() % 4 * 10);
			if(tmp > 0)
				tmp += (rand() % 3 + 1);
			if(tmp > 0 && tmp <= 33)
				this->m_introBuildings[i][j] = new Building(j * (BUILDING_WIDTH + 1 / 2) + LEVEL_BUILDING_X_DECAL, i * (BUILDING_WIDTH + 1 / 2) + LEVEL_BUILDING_Z_DECAL ,tmp / 10, tmp - ((tmp / 10) * 10));
		}
	}

	this->m_levelMovement = 0;
	this->m_enemyStartTimer = 0;
	this->m_enemyStarted = false;

	this->m_player = new Player();
	this->m_leftBorder = new Border(D3DXVECTOR3(-WINDOW_BORDER_X,LEVEL_ELEMENTS_Y,BLACK_BORDER_Z_COORDINATE),BORDER_PLACEMENT_LEFT);
	this->m_rightBorder = new Border(D3DXVECTOR3(WINDOW_BORDER_X,LEVEL_ELEMENTS_Y,BLACK_BORDER_Z_COORDINATE),BORDER_PLACEMENT_RIGHT);
}

/**
* Unload a level
*/
void GameManager::unloadLevel()
{
	this->m_projectileM->freeDatabase();
	this->m_particleM->freeDatabase();
	this->m_bonusM->freeDatabase();
	this->m_enemyM->freeDatabase();
	this->m_lightM->freeDatabase();

	delete this->m_player;
	for(int i = 0 ; i < this->m_levelHeight ; i++)
	{
		for(int j = 0 ; j < LEVEL_WIDTH ; j++)
			if(this->m_buildings[i][j] != NULL)
				delete this->m_buildings[i][j];
		delete this->m_buildings[i];
	}
	delete this->m_buildings;
	this->m_buildings = NULL;

	for(int i = 0 ; i < LEVEL_INTRO_SIZE ; i++)
	{
		for(int j = 0 ; j < LEVEL_WIDTH ; j++)
			if(this->m_introBuildings[i][j] != NULL)
				delete this->m_introBuildings[i][j];
		delete this->m_introBuildings[i];
	}
	delete this->m_introBuildings;
	this->m_introBuildings = NULL;

	delete this->m_leftBorder;
	delete this->m_rightBorder;
	this->m_gameOverTimer = 0;
}
