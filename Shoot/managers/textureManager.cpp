/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include "../managers/textureManager.h"

/**
* Constructor
*/
TextureManager::TextureManager()
{
	this->m_deviceM = DeviceManager::getInstance();
}

/**
* Destructor
*/
TextureManager::~TextureManager()
{
	freeDatabase();
}

/**
* Load a texture and return a pointer of it. If the texture already exists in the database, simply return the pointer.
* @param filename The texture filename
* @return A pointer of the texture
*/
LPDIRECT3DTEXTURE9 TextureManager::loadTexture(std::string filename)
{
	if(!textureExists(filename))
		D3DXCreateTextureFromFile(this->m_deviceM->getDevice(), filename.c_str(), &this->m_textureDatabase[filename]);
	return this->m_textureDatabase[filename];
}

/**
* Check if the texture already exists in the database
* @param filename The texture filename
* @return The test result
*/
bool TextureManager::textureExists(std::string filename)
{
	bool ret = false;
	for(std::map<std::string, LPDIRECT3DTEXTURE9>::iterator it = this->m_textureDatabase.begin() ; it != this->m_textureDatabase.end() ; it++)
		if(it->first == filename)
			ret = true;
	return ret;
}

/**
* Free the manager database
*/
void TextureManager::freeDatabase()
{
	for(std::map<std::string, LPDIRECT3DTEXTURE9>::iterator it = this->m_textureDatabase.begin() ; it != this->m_textureDatabase.end() ; it++)
	{
		it->second->Release();
		it->second = NULL;
	}
	this->m_textureDatabase.clear();
}
