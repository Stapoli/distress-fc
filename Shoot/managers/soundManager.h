/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#ifndef __SOUNDMANAGER_H
#define __SOUNDMANAGER_H

#define MUSIC_BUFFER_SIZE 4

#include <string>
#include <map>
#include "../openAL/ALBase.h"
#include "../openAL/ALwaveloader.h"
#include "../managers/optionManager.h"
#include "../managers/singletonManager.h"

struct ALBuff
{
	ALchar  *Dat;
	ALenum	Fmt;
	ALsizei	Siz, Frq;	
};

class SoundManager : public SingletonManager<SoundManager>
{
friend class SingletonManager<SoundManager>;

private:
	bool m_soundEnabled;
	bool m_playingMusic;
	ALint m_musicIBuff;
	ALint m_musicIState;
	ALuint m_musicRead;
	ALuint m_musicSource;
	ALuint m_musicBuffId;
	ALuint m_musicBuffers[MUSIC_BUFFER_SIZE];
	ALBuff m_musicCBuffer;
	WAVEID m_musicWID;
	WAVEFORMATEX m_musicWFtex;
	CWaves * m_waveLoader;

	std::map<std::string, ALuint> m_sourceDatabase;
	std::map<std::string, ALuint> m_bufferDatabase;

	ALCdevice * m_soundDevice; 
	ALCcontext * m_context;
	OptionManager * m_optionM;

public:
	~SoundManager();
	bool soundExists(std::string soundPath);
	bool isSoundEnabled();
	unsigned int musicPushBuffer(ALuint buff);
	ALuint addSound(std::string soundPath);
	void loadMusic(std::string musicPath);
	void pauseMusic();
	void playMusic();
	void stopMusic();
	void pauseSounds();
	void resumeSounds();
	void stopSounds();
	void initialize();
	void refresh();
	void freeDatabase();

private:
	SoundManager();
};

#endif
