/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include "../misc/var.h"
#include "../managers/joypadManager.h"

#define JOYPAD_ANALOG_DEAD_ZONE 10.0f

/**
* Constructor
*/
JoypadManager::JoypadManager(){}

/**
* Destructor
*/
JoypadManager::~JoypadManager()
{
	if(this->m_joypad != NULL)
	{
		this->m_joypad->Unacquire();
		this->m_joypad->Release();
		this->m_joypad = NULL;
	}
}

/**
* Initialize the manager
*/
void JoypadManager::initialize()
{
	this->m_joypadDetected = false;
	this->m_joypad = NULL;
	this->m_deviceM = DeviceManager::getInstance();
	this->m_logM = LogManager::getInstance();
	HRESULT result;
	result = this->m_deviceM->getInput()->EnumDevices(DI8DEVCLASS_GAMECTRL, createDeviceCallback, NULL, DIEDFL_ATTACHEDONLY);

	if(this->m_joypadDetected)
	{
		result = this->m_joypad->SetDataFormat(&c_dfDIJoystick);
		if(FAILED(result))
			this->m_logM->getStream() << "Unable to set the format for the gamepad\n";

		this->m_buttons = new int*[ACTION_KEY_SIZE];
		for(int i = 0 ; i < ACTION_KEY_SIZE ; i++)
		{
			this->m_buttons[i] = new int[JOYPAD_KEY_INDEX_SIZE];
			this->m_buttons[i][JOYPAD_KEY_INDEX_ASSIGNED] = ACTION_KEY_LOCKED;
		}

		//Customizable button assignment
		this->m_buttons[ACTION_KEY_ENTER_MENU][JOYPAD_KEY_INDEX_ASSIGNED] = GetPrivateProfileInt("joypad", "enter_joypad", 0, INI_FILENAME);
		this->m_buttons[ACTION_KEY_ESCAPE_MENU][JOYPAD_KEY_INDEX_ASSIGNED] = GetPrivateProfileInt("joypad", "escape_joypad", 1, INI_FILENAME);
		this->m_buttons[ACTION_KEY_FIRE1][JOYPAD_KEY_INDEX_ASSIGNED] = GetPrivateProfileInt("joypad", "fire1_joypad", 0, INI_FILENAME);

		//Button assignment verification
		if(this->m_buttons[ACTION_KEY_ENTER_MENU][JOYPAD_KEY_INDEX_ASSIGNED] < 0 || this->m_buttons[ACTION_KEY_ENTER_MENU][JOYPAD_KEY_INDEX_ASSIGNED] >= sizeof(this->m_state.rgbButtons))
			this->m_buttons[ACTION_KEY_ENTER_MENU][JOYPAD_KEY_INDEX_ASSIGNED] = ACTION_KEY_DISABLED;
		if(this->m_buttons[ACTION_KEY_ESCAPE_MENU][JOYPAD_KEY_INDEX_ASSIGNED] < 0 || this->m_buttons[ACTION_KEY_ENTER_MENU][JOYPAD_KEY_INDEX_ASSIGNED] >= sizeof(this->m_state.rgbButtons))
			this->m_buttons[ACTION_KEY_ESCAPE_MENU][JOYPAD_KEY_INDEX_ASSIGNED] = ACTION_KEY_DISABLED;
	}
	else
		this->m_logM->getStream() << "No gamepad detected\n";
	this->m_logM->write();
}

/**
* Refresh the joypad
*/
void JoypadManager::refresh()
{
	HRESULT res = this->m_joypad->GetDeviceState(sizeof(this->m_state),(LPVOID)&this->m_state);
	if(FAILED(res))
	{
		res = this->m_joypad->Acquire();

		while(res == DIERR_INPUTLOST)    
			 res = this->m_joypad->Acquire();

		if(SUCCEEDED(res))
			this->m_joypad->GetDeviceState(sizeof(this->m_state), (LPVOID)&this->m_state);
	}

	for(int i = 0 ; i < ACTION_KEY_SIZE ; i++)
	{
		if(this->m_buttons[i][JOYPAD_KEY_INDEX_ASSIGNED] != ACTION_KEY_LOCKED)
		{
			if(KEYDOWN(this->m_state.rgbButtons,this->m_buttons[i][JOYPAD_KEY_INDEX_ASSIGNED]))
			{
				if(this->m_buttons[i][JOYPAD_KEY_INDEX_STATE] == JOYPAD_KEY_STATE_UP)
					this->m_buttons[i][JOYPAD_KEY_INDEX_STATE] = JOYPAD_KEY_STATE_DOWN;
			}
			else
				this->m_buttons[i][JOYPAD_KEY_INDEX_STATE] = JOYPAD_KEY_STATE_UP;
		}
	}

	if(this->m_state.rgdwPOV[0] == 0 || this->m_state.rgdwPOV[0] == 4500 || this->m_state.rgdwPOV[0] == 31500)
		this->m_buttons[ACTION_KEY_UP_MENU][JOYPAD_KEY_INDEX_STATE] = JOYPAD_KEY_STATE_DOWN;
	else
		this->m_buttons[ACTION_KEY_UP_MENU][JOYPAD_KEY_INDEX_STATE] = JOYPAD_KEY_STATE_UP;

	if(this->m_state.rgdwPOV[0] == 18000 || this->m_state.rgdwPOV[0] == 22500 || this->m_state.rgdwPOV[0] == 13500)
		this->m_buttons[ACTION_KEY_DOWN_MENU][JOYPAD_KEY_INDEX_STATE] = JOYPAD_KEY_STATE_DOWN;
	else
		this->m_buttons[ACTION_KEY_DOWN_MENU][JOYPAD_KEY_INDEX_STATE] = JOYPAD_KEY_STATE_UP;

	if(this->m_state.rgdwPOV[0] == 27000 || this->m_state.rgdwPOV[0] == 22500 || this->m_state.rgdwPOV[0] == 31500)
		this->m_buttons[ACTION_KEY_LEFT_MENU][JOYPAD_KEY_INDEX_STATE] = JOYPAD_KEY_STATE_DOWN;
	else
		this->m_buttons[ACTION_KEY_LEFT_MENU][JOYPAD_KEY_INDEX_STATE] = JOYPAD_KEY_STATE_UP;

	if(this->m_state.rgdwPOV[0] == 9000 || this->m_state.rgdwPOV[0] == 4500 || this->m_state.rgdwPOV[0] == 13500)
		this->m_buttons[ACTION_KEY_RIGHT_MENU][JOYPAD_KEY_INDEX_STATE] = JOYPAD_KEY_STATE_DOWN;
	else
		this->m_buttons[ACTION_KEY_RIGHT_MENU][JOYPAD_KEY_INDEX_STATE] = JOYPAD_KEY_STATE_UP;

	this->m_buttons[ACTION_KEY_UP_ARROW][JOYPAD_KEY_INDEX_STATE] = this->m_buttons[ACTION_KEY_UP_MENU][JOYPAD_KEY_INDEX_STATE];
	this->m_buttons[ACTION_KEY_DOWN_ARROW][JOYPAD_KEY_INDEX_STATE] = this->m_buttons[ACTION_KEY_DOWN_MENU][JOYPAD_KEY_INDEX_STATE];
	this->m_buttons[ACTION_KEY_LEFT_ARROW][JOYPAD_KEY_INDEX_STATE] = this->m_buttons[ACTION_KEY_LEFT_MENU][JOYPAD_KEY_INDEX_STATE];
	this->m_buttons[ACTION_KEY_RIGHT_ARROW][JOYPAD_KEY_INDEX_STATE] = this->m_buttons[ACTION_KEY_RIGHT_MENU][JOYPAD_KEY_INDEX_STATE];
}

/**
* Check the joypad
* @return If the joypad is detected
*/
bool JoypadManager::isJoypadDetected()
{
	return this->m_joypadDetected;
}

/**
* Get the joypad device
* @return The joypad device
*/
LPDIRECTINPUTDEVICEA * JoypadManager::getJoypad()
{
	return &this->m_joypad;
}

/**
* Check if a button is down
* @param button The button number
* @return The test result
*/
bool JoypadManager::isButtonDown(int button)
{
	return this->m_joypadDetected && this->m_buttons[button][JOYPAD_KEY_INDEX_STATE] == JOYPAD_KEY_STATE_DOWN;
}

/**
* Change a button state
* @param button The button number
* @param state The new state
*/
void JoypadManager::setButtonState(int button, int state)
{
	this->m_buttons[button][JOYPAD_KEY_INDEX_STATE] = state;
}

/**
* Release the joypad
*/
void JoypadManager::release()
{
	if(this->m_joypad)
	{
		this->m_joypad->Unacquire();
		this->m_joypad->Release();
		this->m_joypad = NULL;
	}
}

/**
* Restore the joypad
*/
void JoypadManager::restore()
{
	for(int i = 0 ; i < ACTION_KEY_SIZE ; i++)
		this->m_buttons[ACTION_KEY_UP_MENU][JOYPAD_KEY_INDEX_STATE] = JOYPAD_KEY_STATE_UP;
}

/**
* Set the joypad detection to true
*/
void JoypadManager::setJoypadDetected()
{
	this->m_joypadDetected = true;
}

/**
* Freeze the joypad
*/
void JoypadManager::freezeButtons()
{
	for(int i = 0 ; i < ACTION_KEY_SIZE ; i++)
	{
		if(this->m_buttons[i][JOYPAD_KEY_INDEX_ASSIGNED] != ACTION_KEY_LOCKED)
			this->m_buttons[i][JOYPAD_KEY_INDEX_STATE] = JOYPAD_KEY_STATE_FROZEN;
	}
}

/**
* Create the joypad device
* @return The enumeration state
*/
BOOL CALLBACK createDeviceCallback(LPCDIDEVICEINSTANCE instance, void * reference)
{
	JoypadManager * joypadM = JoypadManager::getInstance();
	DeviceManager * deviceM = DeviceManager::getInstance();

	HRESULT result = deviceM->getInput()->CreateDevice( instance->guidInstance, joypadM->getJoypad(), NULL);
	
	if(SUCCEEDED(result))
	{
		joypadM->setJoypadDetected();
		return DIENUM_STOP;
	}
	return DIENUM_CONTINUE;
}
