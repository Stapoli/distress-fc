/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include "../managers/lightManager.h"

/**
* Constructor
*/
LightManager::LightManager(){}

/**
* Destructor
*/
LightManager::~LightManager(){}

/**
* Get the light radius array
* @return The light radius array
*/
float * LightManager::getRadius()
{
	return this->m_radius;
}

/**
* Get the light activated array
* @return The light activated array
*/
int * LightManager::getActivated()
{
	return this->m_activated;
}

/**
* Get the light position array
* @return The light position array
*/
D3DXVECTOR4 * LightManager::getPositions()
{
	return this->m_position;
}

/**
* Get the light color array
* @return The light color array
*/
D3DXVECTOR4 * LightManager::getColors()
{
	return this->m_color;
}

/**
* Get the first available index
* @return An available index
*/
int LightManager::getLightIndexAvailable()
{
	int ret = -1;
	for(int i = 0 ; i < LIGHT_RESERVATION_SIZE && ret == -1 ; i++)
		if(!this->m_activated[i])
			ret = i;
	return ret;
}

/**
* Change a light state
* @param i The light number
* @param b The light state
*/
void LightManager::setEnabled(int i, int b)
{
	this->m_activated[i] = b;
}

/**
* Change the light position
* @param i The light number
* @param pos The light position
*/
void LightManager::setPosition(int i, D3DXVECTOR3 pos)
{
	this->m_position[i] = D3DXVECTOR4(pos,1);
}

/**
* Change the light color
* @param i The light number
* @param color The light color
*/
void LightManager::setColor(int i, D3DXVECTOR3 color)
{
	this->m_color[i] = D3DXVECTOR4(color,1);
}

/**
* Change the light radius
* @param i The light number
* @param radius The light radius
*/
void LightManager::setRadius(int i, float radius)
{
	this->m_radius[i] = radius;
}

/**
* Free the manager database
*/
void LightManager::freeDatabase()
{
	for(int i = 0 ; i < LIGHT_RESERVATION_SIZE ; i++)
		this->m_activated[i] = 0;
}

/**
* Initialize the manager
*/
void LightManager::initialize()
{
	for(int i = 0 ; i < LIGHT_RESERVATION_SIZE ; i++)
	{
		this->m_activated[i] = 0;
		this->m_radius[i] = 0;
		this->m_color[i] = D3DXVECTOR4(1,1,1,1);
	}
}