/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include <windows.h>
#include "../managers/optionManager.h"

/**
* Constructor
*/
OptionManager::OptionManager()
{
	this->m_width = GetPrivateProfileInt("general", "resolution_width", INI_DEFAULT_RESOLUTION_WIDTH, INI_FILENAME);
	this->m_height = GetPrivateProfileInt("general", "resolution_height", INI_DEFAULT_RESOLUTION_HEIGHT, INI_FILENAME);
	this->m_soundVolume = GetPrivateProfileInt("general", "sound_volume", INI_DEFAULT_SOUND_VOLUME, INI_FILENAME) / 10.0f;
	this->m_musicVolume = GetPrivateProfileInt("general", "music_volume", INI_DEFAULT_MUSIC_VOLUME, INI_FILENAME) / 10.0f;
	this->m_ratio = this->m_width / (float)this->m_height;
	this->m_fullscreen = GetPrivateProfileInt("general", "fullscreen", 0, INI_FILENAME);
	this->m_resizeFactor = this->m_width / (float)GLOBAL_DEFAULT_SIZE_FACTOR;
}

/**
* Destructor
*/
OptionManager::~OptionManager(){}

/**
* Get the y screen resolution
* @return The y screen resolution
*/
int OptionManager::getHeight()
{
	return this->m_height;
}

/**
* Get the x screen resolution
* @return The x screen resolution
*/
int OptionManager::getWidth()
{
	return this->m_width;
}

/**
* Get the sound volume
* @return The sound volume
*/
float OptionManager::getSoundVolume()
{
	return this->m_soundVolume;
}

/**
* Get the music volume
* @return The music volume
*/
float OptionManager::getMusicVolume()
{
	return this->m_musicVolume;
}

/**
* Get the resolution ratio
* @return The resolution ratio
*/
float OptionManager::getRatio()
{
	return this->m_ratio;
}

/**
* Get the resize factor
* @return The resize factor
*/
float OptionManager::getResizeFactor()
{
	return this->m_resizeFactor;
}

/**
* Get the fullscreen state
* @return If the fullscreen is activated
*/
bool OptionManager::isFullScreen()
{
	return this->m_fullscreen == 1;
}
