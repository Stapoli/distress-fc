/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include "../managers/shaderManager.h"

/**
* Constructor
*/
ShaderManager::ShaderManager(){}

/**
* Destructor
*/
ShaderManager::~ShaderManager()
{
	freeDatabase();
}

/**
* Load, compile and return a shader. If the shader already exists in the shader database, simply return the shader.
* @param shaderType The shader type
* @return A pointer to the shader
*/
Shader * ShaderManager::loadShader(int shaderType)
{
	if(!shaderExists(shaderType))
	{
		switch(shaderType)
		{
		case SHADER_TYPE_NULL:
			this->m_shaderDatabase[shaderType] = new NullShader(shaderType);
			break;

		case SHADER_TYPE_HIT:
			this->m_shaderDatabase[shaderType] = new HitShader(shaderType);
			break;
		}
	}
	return this->m_shaderDatabase[shaderType];
}

/**
* Verify if a shader exists in the shader database
* @param shaderType The shader type
* @return The test result
*/
bool ShaderManager::shaderExists(int shaderType)
{
	bool ret = false;
	for(std::map<int, Shader *>::iterator it = this->m_shaderDatabase.begin() ; it != this->m_shaderDatabase.end() ; it++)
		if(it->first == shaderType)
			ret = true;
	return ret;
}

/**
* Free the manager database
*/
void ShaderManager::freeDatabase()
{
	for(std::map<int, Shader *>::iterator it = this->m_shaderDatabase.begin() ; it != this->m_shaderDatabase.end() ; it++)
	{
		delete it->second;
		it->second = NULL;
	}
	this->m_shaderDatabase.clear();
}