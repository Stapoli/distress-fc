/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include "../managers/cameraManager.h"

/**
* Constructor
*/
CameraManager::CameraManager(){}

/**
* Destructor
*/
CameraManager::~CameraManager(){}

/**
* Get the View Matrix
* @return The view matrix
*/
D3DXMATRIX CameraManager::getViewMatrix()
{
	return this->m_viewMatrix;
}

/**
* Get the Projection Matrix
* @return The projection matrix
*/
D3DXMATRIX CameraManager::getProjMatrix()
{
	return this->m_projMatrix;
}

/**
* Get the View Proj Matrix
* @return the view prof matrix
*/
D3DXMATRIX CameraManager::getViewProjMatrix()
{
	return this->m_viewProjMatrix;
}

/**
* Convert a world position to its screen position
* @param coordinate The world position
* @return The screen position
*/
D3DXVECTOR2 CameraManager::coordinateToScreen(D3DXVECTOR3 coordinate)
{
	D3DXVECTOR2 result;
	D3DXVECTOR4 screenPoint;
	D3DXVec3Transform(&screenPoint, &coordinate, &this->m_viewProjMatrix);

	result.x = screenPoint.x * (this->m_optionM->getWidth() / 2.0f) / screenPoint.z + this->m_optionM->getWidth() / 2.0f;
	result.y = -screenPoint.y * (this->m_optionM->getHeight() / 2.0f) / screenPoint.z + this->m_optionM->getHeight() / 2.0f;
	return result;
}

/**
* Get the Eye position
* @return The eye position
*/
D3DXVECTOR3 CameraManager::getEye()
{
	return this->m_eye;
}

/**
* Refresh the martixes
* @param pEye The eye position
* @param pAt The look at position
*/
void CameraManager::generateMatrixes(D3DXVECTOR3 pEye, D3DXVECTOR3 pAt)
{
	this->m_eye = pEye;
	D3DXMatrixLookAtLH(&this->m_viewMatrix,&pEye,&pAt,&this->m_up);
	D3DXMatrixPerspectiveFovLH(&this->m_projMatrix,  D3DXToRadian(45), this->m_optionM->getWidth() / (float)this->m_optionM->getHeight(), 1.0f, 200.0f);
	D3DXMatrixMultiply(&this->m_viewProjMatrix, &this->m_viewMatrix, &this->m_projMatrix);
}

/**
* Initialize the manager
*/
void CameraManager::initialize()
{
	this->m_optionM = OptionManager::getInstance();
	this->m_up = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
}