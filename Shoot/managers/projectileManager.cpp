/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include "../managers/projectileManager.h"

/**
* Constructor
*/
ProjectileManager::ProjectileManager(){}

/**
* Destructor
*/
ProjectileManager::~ProjectileManager()
{
	delete this->m_projectiles;
}

/**
* Return a projectile
* @param i The projectile number
* @return A pointer to the projectile
*/
Projectile * ProjectileManager::getProjectile(int i)
{
	return &this->m_projectiles[i];
}

/**
* Initialize the manager
*/
void ProjectileManager::initialize()
{
	this->m_optionM = OptionManager::getInstance();
	this->m_projectiles = new Projectile[MAX_PROJECTILES];
	preload();
}

/**
* Preload the textures
*/
void ProjectileManager::preload()
{
	//Preloading textures
	TextureManager * textureM = TextureManager::getInstance();
	textureM->loadTexture("imgs/laser_big.png");
	textureM->loadTexture("imgs/laser_small.png");
	textureM->loadTexture("imgs/laser2.png");
}

/**
* Add a projectile
* @param projectileType The projectile type
* @param projectileSource The projectile source
* @param position The projectile starting position
* @param orientation The projectile orientation
*/
void ProjectileManager::addProjectile(int projectileType, int projectileSource, D3DXVECTOR3 position, float orientation)
{
	D3DXVECTOR2 center;

	int index = -1;
	for(int i = 0 ; i < MAX_PROJECTILES && index == -1 ; i++)
		if(!this->m_projectiles[i].isAlive())
			index = i;

	if(index > -1)
	{
		switch(projectileType)
		{
			case PROJECTILE_TYPE_PLAYER_BIG:
				center.x = 16 * this->m_optionM->getResizeFactor();
				center.y = 16 * this->m_optionM->getResizeFactor();
				this->m_projectiles[index].setProjectile("imgs/laser_big.png",position,center,projectileSource,50.0f,orientation,0.0f,20,10);
				break;

			case PROJECTILE_TYPE_PLAYER_BIG_SLOW:
				center.x = 16 * this->m_optionM->getResizeFactor();
				center.y = 16 * this->m_optionM->getResizeFactor();
				this->m_projectiles[index].setProjectile("imgs/laser_big.png",position,center,projectileSource,30.0f,orientation,0.0f,20,10);
				break;

			case PROJECTILE_TYPE_PLAYER_SMALL:
				center.x = 8 * this->m_optionM->getResizeFactor();
				center.y = 8 * this->m_optionM->getResizeFactor();
				this->m_projectiles[index].setProjectile("imgs/laser_small.png",position,center,projectileSource,50.0f,orientation,0.0f,20,10);
				break;

			case PROJECTILE_TYPE_PLAYER_SMALL_SLOW:
				center.x = 8 * this->m_optionM->getResizeFactor();
				center.y = 8 * this->m_optionM->getResizeFactor();
				this->m_projectiles[index].setProjectile("imgs/laser_small.png",position,center,projectileSource,30.0f,orientation,0.0f,20,10);
				break;

			case PROJECTILE_TYPE_PLAYER_CURVE_LEFT:
				center.x = 8 * this->m_optionM->getResizeFactor();
				center.y = 8 * this->m_optionM->getResizeFactor();
				this->m_projectiles[index].setProjectile("imgs/laser_small.png",position,center,projectileSource,50.0f,orientation,-60.0f,20,10);
				break;

			case PROJECTILE_TYPE_PLAYER_CURVE_RIGHT:
				center.x = 8 * this->m_optionM->getResizeFactor();
				center.y = 8 * this->m_optionM->getResizeFactor();
				this->m_projectiles[index].setProjectile("imgs/laser_small.png",position,center,projectileSource,50.0f,orientation,60.0f,20,10);
				break;

			case PROJECTILE_TYPE_ENEMY1:
				center.x = 16 * this->m_optionM->getResizeFactor();
				center.y = 16 * this->m_optionM->getResizeFactor();
				this->m_projectiles[index].setProjectile("imgs/laser2.png",position,center,projectileSource,20.0f,orientation,0.0f,1,7);
				break;
		}
		this->m_projectiles[index].scaleSprite(this->m_optionM->getResizeFactor(),this->m_optionM->getResizeFactor());
	}
}

/**
* Refresh the projectiles
* @param time Elapsed time
*/
void ProjectileManager::refresh(float time)
{
	for(int i = 0 ; i < MAX_PROJECTILES ; i++)
	{
		if(this->m_projectiles[i].isAlive())
		{
			this->m_projectiles[i].refresh(time);
			if(this->m_projectiles[i].getWorldPosition().x <= -WINDOW_BORDER_X - PROJECTILES_BORDER_X_MODIFIER || this->m_projectiles[i].getWorldPosition().x >= WINDOW_BORDER_X + PROJECTILES_BORDER_X_MODIFIER || this->m_projectiles[i].getWorldPosition().z >= WINDOW_BORDER_Z_FAR + PROJECTILES_BORDER_Z_MODIFIER || this->m_projectiles[i].getWorldPosition().z <= WINDOW_BORDER_Z_NEAR - PROJECTILES_BORDER_Z_MODIFIER)
				this->m_projectiles[i].setAlive(false);
		}
	}
}

/**
* Draw the projectiles
*/
void ProjectileManager::draw()
{
	for(int i = 0 ; i < MAX_PROJECTILES ; i++)
	{
		if(this->m_projectiles[i].isAlive())
			this->m_projectiles[i].draw();
	}
}

/**
* Free the manager database
*/
void ProjectileManager::freeDatabase()
{
	for(int i = 0 ; i < MAX_PROJECTILES ; i++)
	{
		if(this->m_projectiles[i].isAlive())
			this->m_projectiles[i].setAlive(false);
	}
}
