/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include "../managers/soundManager.h"

/**
* Constructor
*/
SoundManager::SoundManager(){}

/**
* Destructor
*/
SoundManager::~SoundManager()
{
	freeDatabase();
	alcDestroyContext(this->m_context);
	ALCboolean v = alcCloseDevice(this->m_soundDevice);
}

/**
* Verify if a sound exists in the database
* @param soundPath The sound path
* @return The test result
*/
bool SoundManager::soundExists(std::string soundPath)
{
	bool ret = false;
	for(std::map<std::string, ALuint>::iterator it = this->m_sourceDatabase.begin() ; it != this->m_sourceDatabase.end() ; it++)
		if(it->first == soundPath)
			ret = true;
	return ret;
}

/**
* Check if the sound is enabled
* @return If the sound is enabled
*/
bool SoundManager::isSoundEnabled()
{
	return this->m_soundEnabled;
}

/**
* Push the next music buffer to memory
* @param buff Buffer
* @return The number of data read
*/
unsigned int SoundManager::musicPushBuffer(ALuint buff)
{
	unsigned long int read = 0;
	WAVERESULT h;
	h = this->m_waveLoader->ReadWaveData(this->m_musicWID, this->m_musicCBuffer.Dat, this->m_musicCBuffer.Siz, &read);
	if SUCCEEDED(h) 
	{
		alBufferData(buff, this->m_musicCBuffer.Fmt, this->m_musicCBuffer.Dat, read, this->m_musicCBuffer.Frq);			
		alSourceQueueBuffers(this->m_musicSource, 1, &buff);
	}
	return read;
}

/**
* Add a sound and return its id. If the sound already exists in the database, simply return its id.
* @param soundPath The sound path
* @return The sound id
*/
ALuint SoundManager::addSound(std::string soundPath)
{
	if(!soundExists(soundPath))
	{
		alGenBuffers(1,&this->m_bufferDatabase[soundPath]);
		alGenSources(1,&this->m_sourceDatabase[soundPath]);
		{
			ALvoid * alData;
			ALenum alFormat;
			ALsizei alSize, alFreq;
			WaveLoader waveloader;
			WAVEID wid = waveloader.Open(soundPath.c_str(),&alFormat,&alData,&alSize,&alFreq);
			if(wid >= 0)
			{
				waveloader.Info(wid);
				alBufferData(this->m_bufferDatabase[soundPath],alFormat,alData,alSize,alFreq);
				waveloader.Close(wid);
				alSourcei(this->m_sourceDatabase[soundPath], AL_BUFFER,this->m_bufferDatabase[soundPath]);
			}
		}
		alSourcef(this->m_sourceDatabase[soundPath], AL_GAIN, this->m_optionM->getSoundVolume());
	}
	return this->m_sourceDatabase[soundPath];
}

/**
* Initialize the manager
*/
void SoundManager::initialize()
{
	this->m_optionM = OptionManager::getInstance();
	this->m_musicCBuffer.Dat = NULL;
	this->m_soundDevice = alcOpenDevice(NULL);
	this->m_context = NULL;
	if(this->m_soundDevice) 
	{
		this->m_context = alcCreateContext(this->m_soundDevice,NULL);
		alcMakeContextCurrent(this->m_context);
		this->m_soundEnabled = true;
		this->m_waveLoader = new CWaves();
	}
	else
		this->m_soundEnabled = false;
	this->m_playingMusic = false;
}

/**
* Load a music
* @param musicPath The music path
*/
void SoundManager::loadMusic(std::string musicPath)
{
	if(this->m_soundEnabled)
	{
		alDeleteSources(1,&this->m_musicSource);
		alDeleteBuffers(MUSIC_BUFFER_SIZE,this->m_musicBuffers);
		if(this->m_musicCBuffer.Dat != NULL)
			delete this->m_musicCBuffer.Dat;

		this->m_waveLoader->OpenWaveFile(musicPath.c_str(),&this->m_musicWID);
		GetWaveInfo(this->m_waveLoader,this->m_musicWID,&this->m_musicCBuffer.Fmt,&this->m_musicCBuffer.Siz,&this->m_musicCBuffer.Frq);
		this->m_waveLoader->GetWaveFormatExHeader(this->m_musicWID, &this->m_musicWFtex);
		this->m_musicCBuffer.Siz = this->m_musicWFtex.nAvgBytesPerSec / 4;
		this->m_musicCBuffer.Siz -= this->m_musicCBuffer.Siz % this->m_musicWFtex.nBlockAlign;
		this->m_musicCBuffer.Dat = new ALchar[this->m_musicCBuffer.Siz];
		alGenBuffers(MUSIC_BUFFER_SIZE,this->m_musicBuffers);
		alGenSources(1,&this->m_musicSource);
		this->m_waveLoader->SetWaveDataOffset(this->m_musicWID, 0);

		for(int i = 0 ; i < MUSIC_BUFFER_SIZE ; i++)
			this->m_musicRead = musicPushBuffer(this->m_musicBuffers[i]);

		alSourcef(this->m_musicSource, AL_GAIN, this->m_optionM->getMusicVolume());
	}
}

/**
* Pause the music
*/
void SoundManager::pauseMusic()
{
	if(this->m_soundEnabled)
		alSourcePause(this->m_musicSource);
}

/**
* Play the music
*/
void SoundManager::playMusic()
{
	if(this->m_soundEnabled)
	{
		alSourcePlay(this->m_musicSource);
		this->m_playingMusic = true;
	}
}

/**
* Stop the music
*/
void SoundManager::stopMusic()
{
	if(this->m_playingMusic)
	{
		int queued;
		alSourceStop(this->m_musicSource);
		this->m_playingMusic = false;
		this->m_waveLoader->SetWaveDataOffset(this->m_musicWID, 0);
		alGetSourcei(this->m_musicSource, AL_BUFFERS_QUEUED, &queued);
	    
		while(queued--)
		{
			ALuint buffer;
			alSourceUnqueueBuffers(this->m_musicSource, 1, &buffer);
		}
		for(int i = 0 ; i < MUSIC_BUFFER_SIZE ; i++)
			this->m_musicRead = musicPushBuffer(this->m_musicBuffers[i]);
	}
}

/**
* Pause all sounds
*/
void SoundManager::pauseSounds()
{
	ALint status;
	for(std::map<std::string, ALuint>::iterator it = this->m_sourceDatabase.begin() ; it != this->m_sourceDatabase.end() ; it++)
	{
		alGetSourcei(it->second, AL_SOURCE_STATE, &status);
		if(status == AL_PLAYING)
			alSourcePause(it->second);
	}
	alSourcePause(this->m_musicSource);
}

/**
* Resume all the sounds
*/
void SoundManager::resumeSounds()
{
	ALint status;
	for(std::map<std::string, ALuint>::iterator it = this->m_sourceDatabase.begin() ; it != this->m_sourceDatabase.end() ; it++)
	{
		alGetSourcei(it->second, AL_SOURCE_STATE, &status);
		if(status == AL_PAUSED)
			alSourcePlay(it->second);
	}
	alSourcePlay(this->m_musicSource);
}

/**
* Stop all the sounds
*/
void SoundManager::stopSounds()
{
	ALint status;
	for(std::map<std::string, ALuint>::iterator it = this->m_sourceDatabase.begin() ; it != this->m_sourceDatabase.end() ; it++)
	{
		alGetSourcei(it->second, AL_SOURCE_STATE, &status);
		if(status == AL_PAUSED || AL_PLAYING)
			alSourceStop(it->second);
	}
	stopMusic();
}

/**
* Refresh the music
*/
void SoundManager::refresh()
{
	if(this->m_playingMusic)
	{
		this->m_musicIBuff = 0;
		alGetSourcei(this->m_musicSource, AL_BUFFERS_PROCESSED, &this->m_musicIBuff);			
		for(int i = 0 ; i < this->m_musicIBuff ; i++) 
		{
			alSourceUnqueueBuffers(this->m_musicSource, 1, &this->m_musicBuffId);
			this->m_musicRead = musicPushBuffer(this->m_musicBuffId);
		}
		alGetSourcei(this->m_musicSource, AL_SOURCE_STATE, &this->m_musicIState);
		if(this->m_musicRead == 0)
			this->m_waveLoader->SetWaveDataOffset(this->m_musicWID, 0);
	}
}

/**
* Free the manager database
*/
void SoundManager::freeDatabase()
{
	for(std::map<std::string, ALuint>::iterator it = this->m_sourceDatabase.begin() ; it != this->m_sourceDatabase.end() ; it++)
		alDeleteSources(1,&it->second);
	for(std::map<std::string, ALuint>::iterator it = this->m_bufferDatabase.begin() ; it != this->m_bufferDatabase.end() ; it++)
		alDeleteBuffers(1,&it->second);
	this->m_sourceDatabase.clear();
	this->m_bufferDatabase.clear();
	if(this->m_musicCBuffer.Dat != NULL)
		delete this->m_musicCBuffer.Dat;
	alDeleteBuffers(1,&this->m_musicSource);
}
