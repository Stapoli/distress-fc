/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#ifndef __CAMERAMANAGER_H
#define __CAMERAMANAGER_H

#include <d3d9.h>
#include <d3dx9.h>
#include "../managers/optionManager.h"
#include "../managers/singletonManager.h"

#define BLACK_BORDER_Z_COORDINATE 12.0f

#define WINDOW_BORDER_X 21.0f
#define WINDOW_BORDER_Z_FAR 13.0f
#define WINDOW_BORDER_Z_NEAR -33.0f

/**
* CameraManager class
* Handle the camera and the matrixes
*/
class CameraManager : public SingletonManager<CameraManager>
{
friend class SingletonManager<CameraManager>;

private:
	D3DXMATRIX m_viewMatrix;
	D3DXMATRIX m_projMatrix;
	D3DXMATRIX m_viewProjMatrix;
	D3DXVECTOR3 m_eye;
	D3DXVECTOR3 m_up;
	OptionManager * m_optionM;

public:
	~CameraManager();
	D3DXMATRIX getViewMatrix();
	D3DXMATRIX getProjMatrix();
	D3DXMATRIX getViewProjMatrix();
	D3DXVECTOR3 getEye();
	D3DXVECTOR2 coordinateToScreen(D3DXVECTOR3 coordinate);
	void generateMatrixes(D3DXVECTOR3 pEye, D3DXVECTOR3 pAt);
	void initialize();

private:
	CameraManager();
};

#endif
