/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#ifndef __MESHMANAGER_H
#define __MESHMANAGER_H

#include <d3d9.h>
#include <d3dx9.h>
#include <map>
#include <string>
#include "../managers/deviceManager.h"
#include "../managers/logManager.h"
#include "../managers/singletonManager.h"

/**
* MeshManager class
* Handle the meshs
*/
class MeshManager : public SingletonManager<MeshManager>
{
friend class SingletonManager<MeshManager>;

private:
	std::map<std::string,LPD3DXMESH>m_meshDatabase;
	std::map<std::string,DWORD>m_numMaterialDatabase;
	std::map<std::string,D3DMATERIAL9 *>m_materialDatabase;
	std::map<std::string,LPDIRECT3DTEXTURE9 *>m_textureDatabase;
	DeviceManager * m_deviceM;

public:
	~MeshManager();
	LPD3DXMESH loadMesh(std::string filename);
	bool meshExists(std::string filename);
	void draw(std::string filename);
	void initialize();
	void freeDatabase();

private:
	MeshManager();
};

#endif
