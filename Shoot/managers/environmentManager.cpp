/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include "../managers/environmentManager.h"

/**
* Constructor
*/
EnvironmentManager::EnvironmentManager(){}

/**
* Destructor
*/
EnvironmentManager::~EnvironmentManager(){}

/**
* Initialize the manager
*/
void EnvironmentManager::initialize()
{
	this->m_lightM = LightManager::getInstance();
	this->m_lightId = LIGHT_RESERVATION_ENVIRONMENT;

	reset();

	this->m_explosionSound1 = SoundManager::getInstance()->addSound("sounds/nuclear_blast1.wav");
	this->m_explosionSound2 = SoundManager::getInstance()->addSound("sounds/nuclear_blast2.wav");
}

/**
* Reset the manager
*/
void EnvironmentManager::reset()
{
	this->m_explosionActivated = false;
	this->m_explosionLightTimer = 0;
	this->m_explosionTimer = 0;
	this->m_explosionNextDelay = (rand() % (int)ENVIRONMENT_EXPLOSION_DELAY_MAX) + ENVIRONMENT_EXPLOSION_DELAY_MIN;

	this->m_lightM->setRadius(this->m_lightId, ENVIRONMENT_EXPLOSION_LIGHT_RADIUS);
	this->m_lightM->setColor(this->m_lightId, D3DXVECTOR3(0, 0, 0));
	this->m_lightM->setEnabled(this->m_lightId, false);
}

/**
* Refresh the manager
* @param time Elapsed time
*/
void EnvironmentManager::refresh(float time)
{
	if(this->m_explosionActivated)
	{
		this->m_explosionLightTimer += time;
		if(this->m_explosionLightTimer < ENVIRONMENT_EXPLOSION_TIME / 3.0f)
		{
			this->m_lightM->setColor(this->m_lightId, D3DXVECTOR3(this->m_explosionLightTimer / (ENVIRONMENT_EXPLOSION_TIME / 3.0f), 0, 0));
		}
		else
		{
			if(this->m_explosionLightTimer < (ENVIRONMENT_EXPLOSION_TIME / 3.0f) * 2.0f)
			{
				this->m_lightM->setColor(this->m_lightId,D3DXVECTOR3(1,0,0));
			}
			else
			{
				if(this->m_explosionLightTimer < ENVIRONMENT_EXPLOSION_TIME)
					this->m_lightM->setColor(this->m_lightId, D3DXVECTOR3(1 - (((this->m_explosionLightTimer - (ENVIRONMENT_EXPLOSION_TIME / 3.0f ) * 2) / (ENVIRONMENT_EXPLOSION_TIME / 3.0f))), 0, 0));
				else
				{
					this->m_explosionLightTimer = 0;
					this->m_explosionActivated = false;
					this->m_lightM->setEnabled(this->m_lightId, false);
				}
			}
		}
	}
	else
	{
		this->m_explosionTimer += time;
		if(this->m_explosionTimer >= this->m_explosionNextDelay)
		{
			this->m_lightLocation = rand() % ENVIRONMENT_EXPLOSION_SIZE;
			this->m_explosionActivated = true;
			this->m_explosionTimer = 0;
			this->m_explosionNextDelay = (rand() % (int)ENVIRONMENT_EXPLOSION_DELAY_MAX) + ENVIRONMENT_EXPLOSION_DELAY_MIN;
			this->m_lightM->setEnabled(this->m_lightId, true);
			
			switch(this->m_lightLocation)
			{
			case ENVIRONMENT_EXPLOSION_LEFT_TOP:
				this->m_lightM->setPosition(this->m_lightId, D3DXVECTOR3(-(WINDOW_BORDER_X + ENVIRONMENT_EXPLOSION_LIGHT_MODIFIER), ENVIRONMENT_EXPLOSION_LIGHT_Y, WINDOW_BORDER_Z_FAR + ENVIRONMENT_EXPLOSION_LIGHT_MODIFIER));
				break;

			case ENVIRONMENT_EXPLOSION_LEFT_BOTTOM:
				this->m_lightM->setPosition(this->m_lightId, D3DXVECTOR3(-(WINDOW_BORDER_X + ENVIRONMENT_EXPLOSION_LIGHT_MODIFIER), ENVIRONMENT_EXPLOSION_LIGHT_Y, WINDOW_BORDER_Z_NEAR - ENVIRONMENT_EXPLOSION_LIGHT_MODIFIER));
				break;

			case ENVIRONMENT_EXPLOSION_RIGHT_TOP:
				this->m_lightM->setPosition(this->m_lightId, D3DXVECTOR3(WINDOW_BORDER_X + ENVIRONMENT_EXPLOSION_LIGHT_MODIFIER, ENVIRONMENT_EXPLOSION_LIGHT_Y, WINDOW_BORDER_Z_FAR + ENVIRONMENT_EXPLOSION_LIGHT_MODIFIER));
				break;

			case ENVIRONMENT_EXPLOSION_RIGHT_BOTTOM:
				this->m_lightM->setPosition(this->m_lightId, D3DXVECTOR3(WINDOW_BORDER_X + ENVIRONMENT_EXPLOSION_LIGHT_MODIFIER, ENVIRONMENT_EXPLOSION_LIGHT_Y, WINDOW_BORDER_Z_NEAR - ENVIRONMENT_EXPLOSION_LIGHT_MODIFIER));
				break;
			}
			if(rand() % 2 == 0)
				alSourcePlay(this->m_explosionSound1);
			else
				alSourcePlay(this->m_explosionSound2);
		}
	}
}
