/*
==============================================================================================================================================================

Author: Baudoux St�phane
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include "../managers/interfaceManager.h"

/**
* Constructor
*/
InterfaceManager::InterfaceManager(){}

/**
* Destructor
*/
InterfaceManager::~InterfaceManager()
{
	delete this->m_scoreFont;
}

/**
* Initialize the manager
*/
void InterfaceManager::initialize()
{
	this->m_scoreFont = new FontMenu("SCORE_INTERFACE","Courrier","SCORE: 0",25,0,FW_NORMAL,false,FONTMANAGER_ALIGN_LEFT,0,0,200,40,0xFFFFFF00,0xFFFF0000);
}

/**
* Set the score
* @param score The score value
*/
void InterfaceManager::setScore(int score)
{
	int scoreSize;
	this->m_stream.str("");
	this->m_stream << score;
	scoreSize = this->m_stream.str().length();
	this->m_stream.str("");
	this->m_stream << "Score: ";
	for (int i = 0 ; i < SCORE_SIZE - scoreSize ; i++)
		this->m_stream << "0";
	this->m_stream << score;
	this->m_scoreFont->setText(this->m_stream.str());
}

/**
* Set the continues
* @param continues The continues value
*/
void InterfaceManager::setContinues(int continues)
{
	this->m_continues = continues;
}

/**
* Set the bombs
* @param bombs The boms value
*/
void InterfaceManager::setBombs(int bombs)
{
	this->m_bombs = bombs;
}

/**
* Draw the interface
*/
void InterfaceManager::draw()
{
	this->m_scoreFont->draw();
}
