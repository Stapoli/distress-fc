uniform float4x4 worldViewProj;
uniform float4x4 world;

struct TVertex
{
	float4 position  	: POSITION;
	float2 texcoord0 	: TEXCOORD0;
	float3 normal    	: NORMAL;
};

struct TVertexOut
{
	float4 position  : POSITION;
	float2 texcoord0 : TEXCOORD0;
	float3 pos3  	 : TEXCOORD1;
	float3 normal    : TEXCOORD2;
};

TVertexOut main( TVertex IN )
{
	TVertexOut OUT;

	float4 v = float4( IN.position.x,
		               IN.position.y,
					   IN.position.z,
					   1.0f );

	OUT.normal = normalize(mul(-IN.normal,world));
    OUT.position = mul( v, worldViewProj );
	OUT.pos3 = mul(IN.position,world);
    OUT.texcoord0 = IN.texcoord0;

    return OUT;
}
